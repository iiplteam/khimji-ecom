
var userData = {};
var totalItems = 0;
$('.user-in').hide();
$('.user-out').hide();
$('.sendotp-error').hide();
$('.verify-error').hide();
$('.registration-error').hide();
$('#verifyForm').hide();
$('#registrationForm').hide();

$('.name-error').hide();
$('.email-error').hide();
$('.street-error').hide();
$('.area-error').hide();
$('.city-error').hide();
$('.zip-error').hide();


var duringRegistration = false;
var baseUrl = '/';
// var newBaseUrl = 'https://dev.khimjionline.in/';
var newBaseUrl = '/';

$(document).ready(function () {


    $('.no-loading').hide();
    $('.loading').show();
    if (localStorage.getItem('userData') == 'undefined') {
        localStorage.removeItem('userData');
    }

    $('.cart-img').attr('src', '/img/cart.png');
    if (localStorage.getItem('userData')) {
        var userData = JSON.parse(localStorage.getItem('userData'));
        refresh_cart();
        setInterval(() => {
            refresh_cart();
        }, 60000);

        getDetails();
        fetchWish();

        $('.user-name').html(userData.name);
        $('.user-in').show();
        $('.user-out').hide();
        if (localStorage.getItem("tobesaved")) {
            console.log("to be added");

            itemAdd(localStorage.getItem("tobesaved"));

            localStorage.removeItem("tobesaved");
        } else {
            console.log("Noo need be added");
        }
    } else {
        localStorage.removeItem('userToken');
        localStorage.removeItem('userData');
        localStorage.removeItem("phonepeResponse");
        $('.user-in').hide();
        $('.user-out').show();
        $('.orders-empty').hide();
        $('.orders-block').hide();
        $('#wish-table').hide();
        $('.wish-empty').hide();
        if (window.location.pathname.split('/')[1] == "cart"
            || window.location.pathname.split('/')[1] == "my-wishlist"
            || window.location.pathname.split('/')[1] == "my-orders"
            || window.location.pathname.split('/')[1] == "my-account-update") {
            location.replace('/');
        }
    }
});



function fetchWish() {

    $.ajax(newBaseUrl + 'api/wishlist', {
        type: 'GET',
        headers: {
            'x-auth': localStorage.getItem('userToken')
        },
        dataType: 'json', // type of response data
        contentType: 'application/json',
        success: function (data, status, xhr) { // success callback function
            // console.log("WishPPPPPP", data);
            if (data.length == 0) {
                $('#wish-table').hide();
                $('.wish-empty').show();
                $('.no-loading').show();
                $('.loading').hide();
                $('.wish-img').attr('src', '/home-img/wishlist.png');
            } else {
                $('.no-loading').show();
                $('.loading').hide();
                $('.wish-empty').hide();


                $('#cart-row-new').html('');
                totalItems = data.length;
                for (let i = 0; i < data.length; i++) {
                    const params = data[i];
                    if (data.length == 0) {

                        $('.wish-img').attr('src', '/home-img/wishlist.png');
                    } else if (data.length <= 9) {
                        $('.wish-img').attr('src', '/home-img/wishlist' + data.length + '.png');
                    } else {
                        $('.wish-img').attr('src', '/home-img/wishlist+.png');
                    }
                    $('#cart-row-new').append('');
                    $('#cart-row-new').append(`
                            <div class="col-md-6 wish-b-each">
                              

                                <div class="col-md-3 col-xs-4 img-b"	>
                                    <img src="https://khimji.s3.amazonaws.com/khimji/${params.pictures[0]}.jpg">
                                    </div>
                                    <div class="col-md-9 col-xs-8  details-prod-b"	>
                                        <div class="name">
                                            <span>${params.name}</span>

                                            </div>
                                            <div class="price-btn">
                                                <span style="display: grid;text-align: center;">
                                                <span class="mrp-stike" style="padding-left:0 !important"> &#8377 ${params.mrp}</span>
                                            <span class="price-v"style="color: #606060;"> &#8377 ${params.payPrice}</span>
                                            </span>
                                            <span>

                                            <span style="    margin-right: 1rem;    cursor: pointer;"><i class="fa fa-trash"  onclick="checkToDel('${params.id}')" aria-hidden="true" style="cursor:pointer;"></i></span>
                                            <span> <a class="add-item" href="javascript:itemAdd('${params.id}')" class="more phonepe" style="display:none;">@(Add To Cart)</a></span></span>
                                        </div>
                                    </div>
                                    
                                    
                            </div>
                        `);



                }
                $('#wish-table').show();
            }

        },
        error: function (jqXhr, textStatus, errorMessage) { // error callback 
            console.log("errror");
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Please try again',
                showConfirmButton: false,
                timer: 1500
            }).then(() => {

                fetchWish();
            });
        }
    });
}

function fetchUser() {

    userData = JSON.parse(localStorage.getItem('userData'));

    $(".name-append").html(`${userData.name}`);
    $(".email-append").html(`${userData.email}`);
    $(".phone-append").html(`${userData.phone}`);
}

function checkToDel(id) {
    if (totalItems <= 1) {

        Swal.fire({
            Swaltitle: "Are you sure?",
            text: "Your cart will be empty",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }).then((result) => {
            if (result.value) {
                delWish(id);
            }
        });
    } else {
        delWish(id);
    }

}
function delWish(id) {

    var tempobj = {
        "product_id": id.toString()
    };
    $.ajax(newBaseUrl + 'api/wishlist', {
        type: 'DELETE',
        headers: {
            'x-auth': localStorage.getItem('userToken')
        },
        data: JSON.stringify(tempobj),
        dataType: 'json', // type of response data
        contentType: 'application/json',
        success: function (data, status, xhr) { // success callback function
            $('#wish-table').hide();
            if (data.state) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: ' Removed from Wishlist',
                    showConfirmButton: false,
                    timer: 1500
                }).then(() => {
                    wishListData = [];
                    fetchWish();
                });
            }
        },
        error: function (jqXhr, textStatus, errorMessage) { // error callback 
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Please try again',
                showConfirmButton: false,
                timer: 1500
            }).then(() => {

                fetchWish();
            });
        }
    });
}

function isEmail(email) {
    var regex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    return regex.test(email);
}

function isName(id) {
    // var regex = /^[a-zA-Z ]{2,30}$/;
    var regex = /^[a-zA-Z\s ]{2,30}$/;
    if (regex.test(id)) {
        return true;
    }
    else {
        return false;
    }
}

function itemAdd(i) {
    console.log("DDDDDDD", localStorage.getItem('userData'));

    $.ajax(`${newBaseUrl}api/product/${i}`, {
        type: 'GET',
        dataType: 'json', // type of response data
        contentType: 'application/json',
        success: function (data, status, xhr) {
            dataLayer.push({
                'event': 'addToCart',
                'ecommerce': {
                    'currencyCode': 'INR',
                    'add': {
                        'products': [{
                            'name': data.name,
                            'id': data.id,
                            'price': data.payPrice,
                            'brand': data.manufacturer,
                            'category': data.linker_category,
                            'quantity': 1
                        }]
                    }
                }
            });
        }
    });

    if (localStorage.getItem('userData') == null || localStorage.getItem('userData') == undefined) {

        localStorage.setItem("tobesaved", i);
        login();
        return;

    } else {
        var tempobj = {
            "productId": i.toString(),
            "quantity": 1
        };

        $.ajax(`${newBaseUrl}api/cart?t=` + new Date().getTime(), {
            type: 'POST',
            headers: {
                'x-auth': localStorage.getItem('userToken')
            },
            data: JSON.stringify(tempobj),
            dataType: 'json', // type of response data
            contentType: 'application/json',
            success: function (data, status, xhr) { // success callback function

                if (data["status"]) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Added Successfully to cart',
                        showConfirmButton: false,
                        timer: 1500
                    }).then(() => {

                        refresh_cart();
                    });
                }
            },
            error: function (jqXhr, textStatus, errorMessage) {
                if (jqXhr.status == 401) {
                    console.log("401 Err");
                    localStorage.removeItem('userToken');
                    localStorage.removeItem('userData');
                    localStorage.removeItem("phonepeResponse");
                    location.reload();
                }

            }
        });

    }



}

function getDetails() {
    $.ajax(newBaseUrl + 'api/user?t=' + new Date().getTime(), {
        type: 'GET',
        headers: {
            'x-auth': localStorage.getItem('userToken')
        },
        dataType: 'json', // type of response data
        contentType: 'application/json',
        success: function (data, status, xhr) { // success callback function
            localStorage.setItem('userData', JSON.stringify(data));
        },
        error: function (jqXhr, textStatus, errorMessage) { // error callback 
            if (jqXhr.status == 401) {
                console.log("DDDDDDDDDDDDDDDDDDDDDDDD");
                localStorage.removeItem('userToken');
                localStorage.removeItem('userData');
                localStorage.removeItem("phonepeResponse");
                location.reload();
            }

        }
    });

}

function refresh_cart() {


    $.ajax(newBaseUrl + 'api/cart?t=' + new Date().getTime(), {
        type: 'GET',
        headers: {
            'x-auth': localStorage.getItem('userToken')
        },
        dataType: 'json', // type of response data
        contentType: 'application/json',
        success: function (data, status, xhr) { // success callback function
            // console.log("Wish", data);

            if (data["status"]) {
                // if (data.data.products.length == 0) {
                $('.cart-img').attr('src', '/img/cart.png');
                // }
                //  else if (data.data.products.length <= 9) {
                // 	$('.cart-img').attr('src', '/img/carticons/cart' + data.data.products.length + '.png');
                // } else {
                // 	$('.cart-img').attr('src', '/img/carticons/cart9+.png');
                // }
            }
        },
        error: function (jqXhr, textStatus, errorMessage) { // error callback 
            if (jqXhr.status == 401) {
                console.log("DDDDDDDDDDDDDDDDDDDDDDDD");
                localStorage.removeItem('userToken');
                localStorage.removeItem('userData');
                localStorage.removeItem("phonepeResponse");
                location.reload()
            }

        }
    });

}

function sendOtpSubmit() {

    var sendOtpObj = {
        phoneNo: $('#phoneNo').val(),

    };

    $.ajax(`${newBaseUrl}api/user-login/`, {
        type: 'POST',
        data: JSON.stringify(sendOtpObj),
        dataType: 'json', // type of response data
        contentType: 'application/json',
        success: function (response, status, xhr) { // success callback function



            if (response['status']) {
                $('.sendotp-error').hide();
                $('#sendOtpForm').hide();
                $('#verifyForm').show();
            }
            else {
                $('.sendotp-error').show();
                $('.sendotp-error').html(response["message"]);

            }
        },
        error: function (jqXhr, textStatus, errorMessage) { // error callback 
            console.log("errror");

        }
    });
}

function addToWishlist(id) {
    if (localStorage.getItem('userData') == null || localStorage.getItem('userData') == undefined) {
        $(".open-modal").click();
        return;

    } else {
        var tempobj = {
            "product_id": id.toString()
        };

        $.ajax(newBaseUrl + 'api/wishlist', {
            type: 'POST',
            headers: {
                'x-auth': localStorage.getItem('userToken')
            },
            data: JSON.stringify(tempobj),
            dataType: 'json', // type of response data
            contentType: 'application/json',
            success: function (data, status, xhr) { // success callback function

                if (data["state"]) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Added Successfully to wishlist',
                        showConfirmButton: false,
                        timer: 1500
                    }).then(() => {
                        fetchWish();

                    });
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        title: data["message"],
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
            },
            error: function (jqXhr, textStatus, errorMessage) { // error callback 
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Please try again',
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        });
    }
}

function verifyOtpSubmit() {

    var verifyOtpObj = {
        phoneNo: $('#phoneNo').val(),
        otp: $('#otp').val(),

    };

    $.ajax(`${newBaseUrl}api/user-verify`, {
        type: 'POST',
        data: JSON.stringify(verifyOtpObj),
        dataType: 'json', // type of response data
        contentType: 'application/json',
        success: function (response, status, xhr) { // success callback function

            if (response['status']) {
                $('.verify-error').hide();
                $('#verifyForm').hide();
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: response["message"],
                    showConfirmButton: false,
                    timer: 2000
                }).then(() => {
                    localStorage.setItem('userToken', response["token"]);
                    if (response["userState"]) {
                        localStorage.setItem('userData', JSON.stringify(response["data"]));
                        location.reload();
                    } else {
                        $('#dontClose').hide();
                        $('#registrationForm').show();
                        $('#phone').val($('#phoneNo').val());
                    }
                });

            }
            else {

                $('.verify-error').show();
                $('.verify-error').html(response["message"]);

            }
        },
        error: function (jqXhr, textStatus, errorMessage) { // error callback 
            console.log("errror");

        }
    });
}



function registrationOtpSubmit() {
    let nameError = false;
    let emailError = false;
    let streetError = false;
    let cityError = false;
    let areaError = false;
    let zipError = false;


    if (!isName($('#name').val())) {
        $('.name-error').show();
        nameError = true;
    } else {
        $('.name-error').hide();
        nameError = false;
    }


    if (!isEmail($('#email').val())) {
        $('.email-error').show();
        emailError = true;
    } else {
        $('.email-error').hide();
        emailError = false;
    }

    if ($('#street').val() == '') {
        $('.street-error').show();
        streetError = true;
    } else {
        $('.street-error').hide();
        streetError = false;
    }

    if ($('#area').val() == '') {
        $('.area-error').show();
        areaError = true;
    } else {
        $('.area-error').hide();
        areaError = false;
    }

    if ($('#city').val() == '') {
        $('.city-error').show();
        cityError = true;
    } else {
        cityError = false;
        $('.city-error').hide();
    }

    if ($('#zip').val() == '') {
        zipError = true;
        $('.zip-error').show();
    } else {
        zipError = false;
        $('.zip-error').hide();
    }

    if (nameError || emailError || streetError || cityError || areaError || zipError) {
        return;
    } else {
        var registrationOtpObj = {
            name: $('#name').val(),

            phone: $('#phone').val(),
            email: $('#email').val(),

            addresses: [
                {
                    name: $('#name').val(),
                    phone: $('#phone').val(),
                    street: $('#street').val(),
                    area: $('#area').val(),
                    city: $('#city').val(),
                    zip: $('#zip').val(),
                }
            ]
        };

        $.ajax(`${newBaseUrl}api/user`, {
            type: 'POST',
            data: JSON.stringify(registrationOtpObj),
            headers: {
                'x-auth': localStorage.getItem('userToken')
            },
            dataType: 'json', // type of response data
            contentType: 'application/json',
            success: function (response, status, xhr) { // success callback function

                if (response['status']) {
                    $('.registration-error').hide();
                    $('#registrationForm').hide();
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: response["message"],
                        showConfirmButton: false,
                        timer: 3000
                    }).then(() => {

                    });

                    setTimeout(() => {
                        localStorage.setItem('userData', JSON.stringify(response['data'][0]));
                        location.reload();
                    }, 2500);

                }
                else {

                    $('.registration-error').show();
                    $('.registration-error').html(response["message"]);

                }
            },
            error: function (jqXhr, textStatus, errorMessage) { // error callback 
                console.log("errror");

            }
        });


    }
}

function phonepyVerifyOtpSubmit(a, b) {
    // alert("jph");
    var verifyOtpObj = {
        phoneNo: a,
        otp: b,

    };

    $.ajax(`/api/user-verify`, {
        type: 'POST',
        data: JSON.stringify(verifyOtpObj),
        dataType: 'json', // type of response data
        contentType: 'application/json',
        success: function (response, status, xhr) { // success callback function
            // alert("jph ----"+JSON.stringify(response));
            if (response['status']) {


                if (response["userState"]) {
                    localStorage.setItem('userToken', response["token"]);
                    localStorage.setItem('userData', JSON.stringify(response['data']));
                    location.reload();
                } else {
                    var phData = localStorage.getItem("phonepeResponse");

                    $('#email').val(phData["data"]["primaryEmail"]);
                    $('#name').val(phData["data"]["name"]);
                    $('#dontClose').hide();
                    $('#registrationForm').show();
                    $('#phone').val($('#phoneNo').val());
                }
            }

        },
        error: function (jqXhr, textStatus, errorMessage) { // error callback 
            console.log("errror");
            // alert("error"+JSON.stringify(errorMessage));

        }
    });
}

