var MONGO_DB_CONNECTION = process.env.MONGO_DB_CONNECTION || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);
var jwt = require('jsonwebtoken');
var pincodeVerify = MODULE('pincodetest');

// jwt secret key 
var JWT_SECRET_KEY = process.env.JWT_SECRET_KEY || 'happi_jwt_secret';

NEWSCHEMA('Users').make(function (schema) {

	schema.define('id', 'UID');
	schema.define('name', 'String(100)');
	schema.define('firstname', 'Capitalize(50)');
	schema.define('lastname', 'Capitalize(50)');
	schema.define('phone', String);
	schema.define('email', 'Email');
	schema.define('gender', ['male', 'female']);
	schema.define('addresses', "[Object]");
	schema.define('datecreated', Date);
	schema.define('dateupdated', Date);

	// saves user into database
	schema.setSave(function ($) {
		var model = $.model;
		var mnosql = new Agent();
		var token = $.controller.headers['x-auth'];
		// token verify
		console.log("TRIGGERED USER SAVE");
		if (token != null) {
			try {
				decoded = jwt.verify(token, JWT_SECRET_KEY);
				console.log("decoded", decoded);
				if (decoded != null) {
					//console.log("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy")
					if (decoded.phone != model.phone) {
						$.callback({
							status: false,
							message: "You are not allowed to access this data"
						});
						return;
					};

					mnosql.select('users', 'Users').make(function (builder) {
						builder.where('phone', decoded.phone);
						builder.first()
					});

					mnosql.exec(async function (err, response) {
						console.log("MongoErr", err);
						//console.log("Mongo RES", response.users);
						//return;
						var mongoClient = new Agent();
						// add new user
						if (response.users == null) {
							model.id = UID();
							model.datecreated = new Date();
							model.dateupdated = new Date();
							if (model.addresses.length > 0) {
								for (let i = 0; i < model.addresses.length; i++) {
									const element = model.addresses[i];
									//console.log("element",element);
									var pincodedata = await pincodeVerify.pincodeVerify(element.zip);
									//console.log("pincode data ????????", pincodedata);
									//if (pincodedata != "Invalid Pincode") {
										mongoClient.insert('addUser', 'Users').make(function (builder) {
											builder.set(model);
										});
									/*} else {
										return $.callback({
											status: false,
											message: "Invalid Pincode"
										});
									}*/
								}
							} else {
								mongoClient.insert('addUser', 'Users').make(function (builder) {
									builder.set(model);
								});
							}

						} else { // update user
							model.dateupdated = new Date();
							if (model.addresses.length > 0) {
								for (let i = 0; i < model.addresses.length; i++) {
									const element = model.addresses[i];
									//console.log("element",element);
									//var pincodedata = await pincodeVerify.pincodeVerify(element.zip);
									//console.log("pincode data ????????", pincodedata);
									//if (pincodedata != "Invalid Pincode") {
										mongoClient.update('updateUser', 'Users').make(function (builder) {
											builder.set(model);
											builder.where('phone', decoded.phone);
										});
									/*} else {
										return $.callback({
											status: false,
											message: "Invalid Pincode"
										});
									}*/
								}
								//console.log("pincode data", pincodedata);

							} else {
								mongoClient.update('updateUser', 'Users').make(function (builder) {
									builder.set(model);
									builder.where('phone', decoded.phone);
								});
							}

						}
						mongoClient.select('getUser', 'Users').make(function (builder) {
							builder.where('phone', decoded.phone);
						});
						mongoClient.exec(function (err, res) {
							if (err) {
								console.log("mongoerr", err);
								return $.invalid(err);
							}
							//console.log("response", res.getUser);
							$.callback({
								status: true,
								data: res.getUser
							});
						});
					})
				}
			} catch (err) {
				// err
				console.log("err", err);
				$.controller.throw401("Invalid Token");
			}
		} else {
			$.controller.throw401("Please provide token");
		}

		if (model.uid == '') {
			$.invalid('invalid-user');
			return;
		}

	});


	// Gets a specific user
	schema.setGet(function ($) {
		var mnosql = new Agent();
		var token = $.controller.headers['x-auth'];
		if (token != null) {
			try {
				decoded = jwt.verify(token, JWT_SECRET_KEY);
				console.log("decoded", decoded);
				if (decoded != null) {
					mnosql.select('users', 'Users').make(function (builder) {
						builder.where('phone', decoded.phone);
						builder.first()
					})

					mnosql.exec(function (err, response) {
						console.log("MongoErr", err);
						//console.log("Mongo RES", response.users)
						if (response.users == null) {
							$.callback({ status: false, message: "User not registered" });
						}
						addressVerfiction(response.users);
						$.callback(response.users);
					})
				}
			} catch (err) {
				// err
				$.controller.throw401("Invalid Token");
			}
		} else {
			$.controller.throw401("Please provide token");
		}

	});


});

function addressVerfiction(user){
	var pincodes = [
		500038,
		515001,
		500062,
		500039,
		500060,
		500050,
		515671,
		500016,
		500072,
		515001,
		505001,
		500009,
		500072,
		518001,
		500008,
		500081,
		509001,
		500047,
		500034,
		500013,
		500020,
		501218,
		500039,
		500070,
		500072,
		500034,
		508002,
		506002,
		500048,
		506001,
		505209,
		518001,
		515591,
		502001,
		503001,
		534201,
		504001,
		500055,
		522002,
		533101,
		502103,
		500059,
		500070,
		506001,
		534101,
		522001,
		500084,
		522201,
		508213,
		506132,
		507001,
		500062,
		520010,
		520010,
		520002,
		520002,
		515411,
		530006,
		530001,
		532001,
		530020,
		523002,
		500035,
		500079,
		500004,
		500007,
		500043,
		500101,
		501102,
		500010,
		500044,
		501301,
		500015,
		500091,
		500040,
		500058,
		500064,
		500027,
		500037,
		500013,
		500003,
		500075,
		500078,
		500011,
		500092,
		500005,
		500022,
		500046,
		501401,
		500024,
		500086,
		500033,
		500018,
		500053,
		500001,
		500065,
		500096,
		500080,
		500097,
		500068,
		500014,
		500042,
		500066,
		500025,
		500051,
		500054,
		500028,
		500002,
		500076,
		500082,
		500031,
		500087,
		500006,
		500088,
		500077,
		500030,
		500100,
		500074,
		500017,
		500063,
		500036,
		500089,
		500032,
		500098,
		500049,
		500083,
		500026,
		500090,
		500095,
		500069,
		500041,
		500059,
		500094,
		500035,
		500061,
		500073,
		500067,
		500052,
		500079,
		500057,
		501101,
		500093,
		500023,
		520003,
		520012,
		520008,
		520007,
		520015,
		520001,
		520004,
		520011,
		521528,
		520013,
		521104,
		521108,
		530016,
		530003,
		530001,
		530002,
		530045,
		530040,
		530022,
		530024,
		530017,
		530013,
		530041,
		530043,
		530035,
		530012,
		531163,
		531162,
		530004,
		530005,
		530007,
		530008,
		530011,
		530018,
		530009,
		530014,
		530044,
		530029,
		531219,
		530028,
		530032,
		530046,
		530047,
		530031,
		530015];

	if(user && user.addresses){
		for(var i =0; i < user.addresses.length ;i++ ){
			if( pincodes.indexOf(parseInt(user.addresses[i].zip)) != -1){
				user.addresses[i].istwohrs = true;
			}else{
				user.addresses[i].istwohrs = false;
			} 
		}
	}	
		
}

