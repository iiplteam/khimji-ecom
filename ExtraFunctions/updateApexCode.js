var MONGO_DB_CONNECTION = process.env.MONGO_DB_CONNECTION || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);
var fs = require('fs');

async function updateApexItemCode() {
    var nosql = new Agent();
    var res = JSON.parse(fs.readFileSync(__dirname + '/apexitemcode.json'));
    //console.log("res", res);
    res.forEach(element => {
        if (element.APXitemCode != "N/A") {
            console.log("apx", element.APXitemCode, "id", element.id);
            nosql.update('updateApx', 'product').make(function (builder) {
                builder.set('APXitemCode', element.APXitemCode);
                builder.where('id', element.id)
            });
            nosql.exec(function (err, response) {
                if (err) {
                    console.log("mongoerr", err);
                }
                console.log("response",response.updateApx); 
                
            });
           
        }
    });

}

updateApexItemCode();