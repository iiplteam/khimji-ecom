const CouponJS = require('couponjs');
var MONGO_DB_CONNECTION = process.env.MONGO_DB_CONNECTION || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);
var as = require('async');
const coupon = new CouponJS({
    verbose: true,
    logPerformance: true
});

const myCoupons = coupon.generate({
    length: 6,
    numberOfCoupons: 50,
    prefix: 'TT',
    characterSet: {
        builtIn: ['CHARSET_ALPHA', 'CHARSET_DIGIT']
    }
});

console.log(myCoupons);
var counter = 0;

as.eachOfLimit(myCoupons.coupons, 25, loop_func, end);

function loop_func(coupon, index, callback) {
    var mongoClient = new Agent();
    var obj = {
        coupon: coupon,
        type: "amount",
        amount: 200,
        use: "one",
        minimum_amount: 10000,
        isActive: true
    };

    mongoClient.insert('one_time_coupon', 'one_time_coupon').make(function (builder) {
        builder.set(obj);
    });

    mongoClient.exec(function (err, result) {
        console.log("KEY", index);
        callback();
    });

}

function end() {
    console.log("THE END");
}

