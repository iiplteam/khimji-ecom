var MONGO_DB_CONNECTION =
    process.env.MONGO_DB_CONNECTION ||
    'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev'
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION)
var axios = require('axios')
var argv = require('minimist')(process.argv.slice(2))
var logger = require('logger').createLogger('stockupdate.log')
var as = require('async');
const FS = require('fs');
var nodemailer = require('nodemailer');
var path = require('path');
console.log(argv)
var stores = [
    'CHANDANAGAR',
    'HYDERNAGAR',
    'KPHB',
    'MADHAPUR',
    'GREENLANDS',
    'AMEERPET',
    'SDROAD',
    'SRNAGAR',
    'UPPAL',
    'BODUPPAL',
    'MALKAJGIRI',
    'RAMANTHAPUR',
    'LANGERHOUSE',
    'SHAMSHABAD',
    'KARMANGHAT',
    'DILSUKHNAGAR',
    'CHAITANYAPURI',
    'SAROORNAGAR',
    'SANTOSH NAGAR',
    'KARKHANA',
    'ASRAONAGAR',
    'ECIL',
    'ATTAPUR',
    'RTC X ROAD',
    'SANGAREDDY',
    'CHINTAL',
    'HIMAYATNAGAR',
    'SHARATHCITYMALL',
    'BANJARAHILLS',
    'VANASTALIPURAM',
    'WAREHOUSE TG',
    'ECOMMERCE - TS',
    'HIMAYATHNAGAR',
    'KHARKHANA',
    'KHARMANGHAT',
    'KUKATPALLY',
    'LANGARHOUSE',
    'S.D.ROAD'
];

async function getAPXitemCode() {
    var nosql = new Agent();
    // clear logger file
    // const filepath = path.join(__dirname + '/../stockupdate.log');
    // try {
    //     FS.unlinkSync(filepath)
    //     //file removed
    // } catch (err) {
    //     console.error(err)
    // }

    nosql.select('getApex', 'product').make(function (builder) {
        if (argv.id) {
            builder.where('id', argv.id)
        }
        builder.where('ispublished', true)
        builder.fields('name', 'id', 'APXitemCode');
        builder.sort('dateupdated', 'desc')
        //builder.page(1,5);
    })
    var data = await nosql.promise('getApex')
    // 180429002li60b -- pid
    run(data);
    // console.log("END",data);
    //process.exit(1)
}

function iteratee(item, i, callback) {
    const element = item;
    if (element.APXitemCode == undefined) {
        logger.info(`${i} - ${element.id} - ${element.name} - No Apex Code `)
        callback()
        return
    }
    if (element.APXitemCode == '' || element.APXitemCode == null) {
        logger.info(`${i} - ${element.id} - ${element.name} - No Apex Code `)
        callback()
        return
    }
    var options = {
        method: 'GET',
        url:
            'http://183.82.44.213:8085/api/apxapi/GetStockInfo?CompanyCode=HM&ItemCode=' +
            element.APXitemCode +
            '&AsonDate=0&Brand=0&BranchCode=0',
        headers: {
            UserId: 'WEBSITE',
            SecurityCode: '9519-9919-1463-3397'
        }
    }
    axios(options)
        .then(function (response) {
            // console.log("hari",response.data)
            var raw = response.data.Data
            var stock = 0
            var totalStock = 0
            var mongoClient = new Agent()
            mongoClient
                .remove('deletestock', 'product_stock')
                .make(function (builder) {
                    builder.where('productId', element.id)
                })
            //await mongoClient.promise('deletestock');
            for (let j = 0; j < raw.length; j++) {
                const element1 = raw[j]
                var obj = {
                    productId: element.id,
                    itemCode: element1.ITEM_CODE,
                    branchName: element1.BRANCH_NAME,
                    stock: element1.SALEABLE_STOCK
                };
                mongoClient
                    .insert('addstock' + j, 'product_stock')
                    .make(function (builder) {
                        builder.set(obj)
                    })
                //await mongoClient.promise('addstock');
                totalStock = totalStock + element1.SALEABLE_STOCK
                if (stores.indexOf(element1.BRANCH_NAME) != -1) {
                    stock = stock + element1.SALEABLE_STOCK
                }
                //console.log("productStockAdd", JSON.stringify(obj));
            }
            console.log("productStockInserted", element.id, "TOTAL STOCK", totalStock, "STOCK", stock);
            mongoClient.update('updateStock', 'product').make(function (builder) {
                builder.where('id', element.id)
                builder.set('stock', parseInt(stock))
                builder.set('datesync', new Date());
            })
            mongoClient.exec(function (err, response) {
                if (err) {
                    logger.info(
                        `${i} - ${element.id} - ${element.name} - DB Error `
                    )
                } else {
                    logger.info(
                        `${i} - ${element.id} - ${element.name} - ${totalStock} -  ${stock} `
                    )
                }
                //console.log(`${i} - ${element.id} - ${element.name}`);
                callback()
            });
            //await mongoClient.promise('updateStock');
        })
        .catch(function (error) {
            //console.log(error)
            logger.info(
                `${i} - ${element.id} - ${element.name} - APX fetch Error `
            )
            callback();
        })
        .then(function () {
            // always executed
        })
    //console.log("body", body.data);
}
function callbackopt(err) {
    console.log("err",err);
    var attachments = [{
        filename: 'stockupdate.log',
        path: path.join(__dirname + '/../stockupdate.log'), // stream this file
        encoding: 'base64'
    }]
    var email = {
        to: ["bhagat@redmattertech.com", "srikant@redmattertech.com"],
        subject: "Stock Update Details",
        body: 'Stock Update Details',
        attachments: attachments
    };
    send_mail(email.to, email.subject, email.body, email.attachments, function (err, result1) {
        console.log("Email Res", err, result1);
    })
    console.log("DONE")
}
function run(data) {
    console.log("INSIDE RUN");
    as.eachOfLimit(data, 15, iteratee, callbackopt)
}

getAPXitemCode()



// send mail 
var options = {
    host: "smtp.mailgun.org",
    port: 465,
    ignoreTLS: true,
    secure: true, // use TLS
    auth: {
        user: "orders@sitemail.happimobiles.com",
        pass: "88fc724923ef26a8520590c28ba254fa-f8faf5ef-75baaefa"
    },
    timeout: 10000
}
let smtpTransport = nodemailer.createTransport(options);
var fromAddress = "orders@sitemail.happimobiles.com";

function send_mail(to, subject, body, attachments, cb) {
    var obj = {
        from: fromAddress, // sender address
        to: to, // list of receivers
        subject: subject, // Subject line
        text: body, // plain text body
        //html: html, // html body
        attachments: attachments
    };
    smtpTransport.sendMail(
        obj
        , function (err, data) {

            cb(err, data);
        });
};


