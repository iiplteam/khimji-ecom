var MONGO_DB_CONNECTION_QA = process.env.MONGO_DB_CONNECTION_QA || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION_QA);


async function OrderConfirm(orderId) {
   // console.log("orderIDDDDDDD", orderId);
    
    var nosql = new Agent();

    nosql.select('fetchOrder', 'orders').make(function (builder) {
        builder.where('id', orderId);
        builder.first();
    });

    try {
        //var order = fetchOrder
        var order = await nosql.promise('fetchOrder');
        // console.log("cart", cart);
        if(order != null) {
            for(var i =0 ; i < order.items.length ; i++){
                await StockUpdate(order.items[i].id, (-1 * order.items[i].quantity) +"" );
            }
        } else {
            console.log("Invalid orderId")
        }
    } catch (err) {
        console.log("Invalid order Err", err);
    }
}

async function StockUpdate(productId, value){
    var nosql = new Agent();
    nosql.update('updateProduct', 'product').make(function (builder) {
        builder.where('id', productId);
        builder.inc('stock', value);
    });
    try {
        //var order = fetchOrder
        var updateProduct = await nosql.promise('updateProduct');
        // console.log("cart", cart);
       console.log("updateProduct", updateProduct);
    } catch (err) {
        console.log("Invalid order Err", err);
    }
}

async function OrderCancel(orderId){

    var nosql = new Agent();

    nosql.select('fetchOrder', 'orders').make(function (builder) {
        builder.where('id', orderId);
        builder.first();
    });

    try {
        //var order = fetchOrder
        var order = await nosql.promise('fetchOrder');
        // console.log("cart", cart);
        if(order != null) {
            for(var i =0 ; i < order.items.length ; i++){
                await StockUpdate(order.items[i].id, 1 * order.items[i].quantity);
            }
        } else {
            console.log("Invalid orderId")
        }
    } catch (err) {
        console.log("Invalid order Err", err);
    }
}

module.exports.OrderConfirm = OrderConfirm;
module.exports.OrderCancel = OrderCancel;