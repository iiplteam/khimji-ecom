var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
const assert = require('assert');
var jwt = require('jsonwebtoken');
var moment = require('moment');

// mongo db long url
const MONGO_URL = process.env.MONGO_URL || 'mongodb://sowmya:iNNrxOhVfEdvsUaI@cluster0-shard-00-00-cnw2n.mongodb.net:27017,cluster0-shard-00-01-cnw2n.mongodb.net:27017,cluster0-shard-00-02-cnw2n.mongodb.net:27017/khimji_dev?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority';

// Database Name
const DB_NAME = process.env.DB_NAME || 'khimji_dev';

// jwt secret key 
var JWT_SECRET_KEY = process.env.JWT_SECRET_KEY || 'happi_jwt_secret';

exports.install = function () {
    ROUTE('#admin/api/order/count', ordersCount, ['post', 'cors']);
    ROUTE('#admin/api/store-order/count', storeOrdersCount, ['post', 'cors']);
    ROUTE('#admin/api/order/ytd-count', ytdordersCount, ['post', 'cors']);
    ROUTE('#admin/api/store-order/ytd-count', ytdStoredOrdersCount, ['post', 'cors']);
}

async function ytdordersCount() {
    var self = this;
    var body = self.body
    var token = self.headers['x-auth'];
    if (token != null) {
        try {
            decoded = jwt.verify(token, JWT_SECRET_KEY);
            //console.log("decoded", decoded);
            if (decoded != null) {
                const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });
                client.connect(function (err, C) {
                    assert.equal(null, err);
                    //const start = moment(new Date()).startOf('month').format();
                    const current = moment(new Date()).format();
                    var date = new Date();
                    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);


                    firstDay.setHours(firstDay.getHours() + 5);
                    firstDay.setMinutes(firstDay.getMinutes() + 30);
                    console.log("firstDay", firstDay);
                    const startOfMonth = new Date(new Date(firstDay).setUTCHours(0, 0, 0, 0))
                    const currentDay = new Date(new Date(current).setUTCHours(23, 59, 59, 999))

                    // console.log("startOfMonth", startOfMonth);
                    // console.log("currentDay", currentDay);

                    //  console.log("firstDayOfMonth",new Date(new Date(startOfMonth).setUTCHours(0, 0, 0, 0)),"currentDay" ,new Date(new Date(currentDay).setUTCHours(23, 59, 59, 999)));

                    // current date
                    var fromDt = new Date();
                    fromDt.setUTCHours(0, 0, 0, 0);

                    var toDt = new Date();
                    toDt.setUTCHours(23, 59, 59, 999);

                    // console.log("fromDt", fromDt);
                    // console.log("toDt", toDt);
                    const db = C.db(DB_NAME);

                    yearToDayOrders(db, function (ytd) {
                        //C.close();
                        currentDayOrders(db, fromDt, toDt, function (ctd) {
                            // C.close();
                            monthToDayOrders(db, startOfMonth, currentDay, function (mtd) {
                                C.close();
                                ytd.push(ctd);
                                ytd.push(mtd);
                                self.json({
                                    status: true,
                                    data: ytd
                                })
                            });
                        });
                    });
                });
            }
        } catch (err) {
            // err
            console.log("err", err);
            self.throw401("Invalid Token");
        }
    } else {
        self.throw401("Please provide token");
    }

}

async function ytdStoredOrdersCount() {
    var self = this;
    var body = self.body
    var token = self.headers['x-auth'];
    if (token != null) {
        try {
            decoded = jwt.verify(token, JWT_SECRET_KEY);
            //console.log("decoded", decoded);
            if (decoded != null) {
                const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });
                client.connect(function (err, C) {
                    assert.equal(null, err);
                    const start = moment(new Date()).startOf('month').format();
                    const current = moment(new Date()).format();

                    const startOfMonth = new Date(new Date(start).setUTCHours(0, 0, 0, 0))
                    const currentDay = new Date(new Date(current).setUTCHours(23, 59, 59, 999))

                    console.log("firstDayOfMonth", new Date(new Date(startOfMonth).setUTCHours(0, 0, 0, 0)), "currentDay", new Date(new Date(currentDay).setUTCHours(23, 59, 59, 999)));

                    // current date
                    var fromDt = new Date();
                    fromDt.setUTCHours(0, 0, 0, 0);

                    var toDt = new Date();
                    toDt.setUTCHours(23, 59, 59, 999);

                    // console.log("fromDt", fromDt);
                    // console.log("toDt", toDt);
                    var referalId;
                    if (body.referalId) {
                        referalId = body.referalId;
                    } else {
                        referalId = ""
                    }
                    console.log("referalId", referalId)
                    const db = C.db(DB_NAME);

                    yearToDayStoreOrders(db, referalId, function (ytd) {
                        //C.close();
                        currentDayStoreOrders(db, fromDt, toDt, referalId, function (ctd) {
                            // C.close();
                            monthToDayStoreOrders(db, startOfMonth, currentDay, referalId, function (mtd) {
                                C.close();
                                ytd.push(ctd);
                                ytd.push(mtd);
                                self.json({
                                    status: true,
                                    data: ytd
                                })
                            });
                        });
                    });
                });
            }
        } catch (err) {
            // err
            console.log("err", err);
            self.throw401("Invalid Token");
        }
    } else {
        self.throw401("Please provide token");
    }

}

async function ordersCount() {
    var self = this;
    var body = self.body
    var token = self.headers['x-auth'];
    if (token != null) {
        try {
            decoded = jwt.verify(token, JWT_SECRET_KEY);
            //console.log("decoded", decoded);
            if (decoded != null) {
                const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });
                client.connect(function (err, C) {
                    assert.equal(null, err);
                    // console.log("body",body);
                    var fromDt = new Date(body.fromDt);
                    fromDt.setUTCHours(0, 0, 0, 0);

                    var toDt = new Date(body.toDt);
                    toDt.setUTCHours(23, 59, 59, 999);
                    console.log("fromDt", fromDt);
                    console.log("toDt", toDt);
                    const db = C.db(DB_NAME);

                    ordersByStatus(db, fromDt, toDt, function (orders) {
                        // C.close();
                        getAllOrders(db, fromDt, toDt, function (totalOrders) {
                            // C.close();
                            codOrders(db, fromDt, toDt, function (iscod) {
                                // C.close();
                                paidOrders(db, fromDt, toDt, function (ispaid) {
                                    // C.close();
                                    UnconfirmedOrders(db, fromDt, toDt, function (unconfirmed) {
                                        C.close();
                                        orders.push(totalOrders);
                                        orders.push(iscod);
                                        orders.push(ispaid);
                                        orders.push(unconfirmed);
                                        self.json({
                                            status: true,
                                            data: orders
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            }
        } catch (err) {
            // err
            console.log("err", err);
            self.throw401("Invalid Token");
        }
    } else {
        self.throw401("Please provide token");
    }
}

async function storeOrdersCount() {
    console.log("STORE_ORDERS_COUNT_TRIGGERED=======");
    var self = this;
    var body = self.body
    var token = self.headers['x-auth'];
    if (token != null) {
        try {
            decoded = jwt.verify(token, JWT_SECRET_KEY);
            //console.log("decoded", decoded);
            if (decoded != null) {
                const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });
                client.connect(function (err, C) {
                    assert.equal(null, err);
                    // console.log("body",body);
                    var fromDt = new Date(body.fromDt);
                    fromDt.setUTCHours(0, 0, 0, 0);

                    var toDt = new Date(body.toDt);
                    toDt.setUTCHours(23, 59, 59, 999);
                    console.log("fromDt", fromDt);
                    console.log("toDt", toDt);
                    var referalId;
                    if (body.referalId) {
                        referalId = body.referalId;
                    } else {
                        referalId = ""
                    }
                    console.log("referalId", referalId)
                    const db = C.db(DB_NAME);

                    StoreOrdersByStatus(db, fromDt, toDt, referalId, function (orders) {
                        C.close();
                        self.json({
                            status: true,
                            data: orders
                        });
                        // getAllStoreOrders(db, fromDt, toDt, referalId, function (totalOrders) {
                        //     // C.close();
                        //     codStoreOrders(db, fromDt, toDt, referalId, function (iscod) {
                        //         // C.close();
                        //         paidStoreOrders(db, fromDt, toDt, referalId, function (ispaid) {
                        //             // C.close();
                        //             UnconfirmedStoreOrders(db, fromDt, toDt, referalId, function (unconfirmed) {
                        //                 C.close();
                        //                 orders.push(totalOrders);
                        //                 orders.push(iscod);
                        //                 orders.push(ispaid);
                        //                 orders.push(unconfirmed);
                        //                 self.json({
                        //                     status: true,
                        //                     data: orders
                        //                 });
                        //             });
                        //         });
                        //     });
                        // });
                    });
                });
            }
        } catch (err) {
            // err
            console.log("err", err);
            self.throw401("Invalid Token");
        }
    } else {
        self.throw401("Please provide token");
    }
}

// orders aggregation functions

function ordersByStatus(db, fromDt, toDt, cb) {
    const collection = db.collection('orders');
    collection.aggregate([
        {
            $match: {
                datecreated: { $gte: fromDt, $lt: toDt }
            }
        },
        {
            $group: {
                "_id": {
                    "status": "$status"
                },
                "price": { "$sum": "$price" },
                "Count": { "$sum": 1 }
            }
        }, {
            $project: {
                "Status": "$_id.status",
                "Value": "$price",
                "Count": "$Count",
                "_id": 0
            }
        }
    ]).toArray(function (err, result) {
        var temparray = [
            {
                "Status": "Accepted",
                "Count": 0,
                "Value": 0
            },
            {
                "Status": "Cancel",
                "Count": 0,
                "Value": 0
            },
            {
                "Status": "Hold",
                "Count": 0,
                "Value": 0
            },
            {
                "Status": "Sent",
                "Count": 0,
                "Value": 0
            },
            {
                "Status": "Finished",
                "Count": 0,
                "Value": 0
            }
        ];
        if (err) {

            cb(temparray);
            return;
        }
        temparray.forEach(element => {
            result.forEach(element2 => {
                //console.log("element", element2)
                if (element.Status == element2.Status) {
                    element.Count = element2.Count;
                    element.Value = element2.Value;
                }
            });
        });
        cb(temparray);
        //console.log("tempArray---------------------------------------------", temparray)
        // return temparray;
    });
}

function getAllOrders(db, fromDt, toDt, cb) {
    const collection = db.collection('orders');

    collection.aggregate([
        {
            $match: {
                datecreated: { $gte: fromDt, $lt: toDt }
            }
        },
        {
            $group: {
                "_id": {
                    "_id": null,
                },
                "price": { "$sum": "$price" },
                "Count": { "$sum": 1 }
            }
        }, {
            $project: {
                "TotalOrdersReceived": "$Count",
                "Value": "$price",
                "_id": 0
            }
        }
    ]).toArray(function (err, result) {
        var tempObj = {
            Status: "Received",
            Count: 0,
            Value: 0
        };
        if (err) {
            cb(tempObj);
            return;
        }
        if(result.length > 0) {
                tempObj.Count = result[0].TotalOrdersReceived || 0,
                tempObj.Value = result[0].Value || 0,
            cb(tempObj);
            return;
        }else {
            cb(tempObj);
            return;
        }
       
    });
}

function codOrders(db, fromDt, toDt, cb) {
    const collection = db.collection('orders');
    collection.aggregate([
        {
            $match: {
                datecreated: { $gte: fromDt, $lt: toDt },
                iscod: true
            }
        },
        {
            $group: {
                "_id": {
                    "_id": null,
                },
                "price": { "$sum": "$price" },
                "Count": { "$sum": 1 }
            }
        }, {
            $project: {
                "codOrders": "$Count",
                "Value": "$price",
                "_id": 0
            }
        }
    ]).toArray(function (err, result) {
        var tempObj = {
            Status: "iscod",
            Count: 0,
            Value: 0
        };
        if (err) {
            cb(tempObj);
            return;
        }
        if(result.length > 0) {
                tempObj.Count = result[0].codOrders || 0,
                tempObj.Value = result[0].Value || 0,
            cb(tempObj);
            return;
        }else {
            cb(tempObj);
            return;
        }
    });
}

function paidOrders(db, fromDt, toDt, cb) {
    const collection = db.collection('orders');
    collection.aggregate([
        {
            $match: {
                datecreated: { $gte: fromDt, $lt: toDt },
                ispaid: true
            }
        },
        {
            $group: {
                "_id": {
                    "_id": null,
                },
                "price": { "$sum": "$price" },
                "Count": { "$sum": 1 }
            }
        }, {
            $project: {
                "paidOrders": "$Count",
                "Value": "$price",
                "_id": 0
            }
        }
    ]).toArray(function (err, result) {
        var tempObj = {
            Status: "ispaid",
            Count: 0,
            Value: 0
        };
        if (err) {
            cb(tempObj);
            return;
        }
        if(result.length > 0) {
                tempObj.Count = result[0].paidOrders || 0,
                tempObj.Value = result[0].Value || 0,
            cb(tempObj);
            return;
        }else {
            cb(tempObj);
            return;
        }
    });
}

function UnconfirmedOrders(db, fromDt, toDt, cb) {
    const collection = db.collection('orders');
    collection.aggregate([
        {
            $match: {
                datecreated: { $gte: fromDt, $lt: toDt },
                tag: "wait"
            }
        },
        {
            $group: {
                "_id": {
                    "_id": null,
                },
                "price": { "$sum": "$price" },
                "Count": { "$sum": 1 }
            }
        }, {
            $project: {
                "UnconfirmedOrders": "$Count",
                "Value": "$price",
                "_id": 0
            }
        }
    ]).toArray(function (err, result) {
        var tempObj = {
            Status: "UnConfirmed",
            Count: 0,
            Value: 0
        };
        if (err) {
            cb(tempObj);
            return;
        }
        if(result.length > 0) {
                tempObj.Count = result[0].UnconfirmedOrders || 0,
                tempObj.Value = result[0].Value || 0,
            cb(tempObj);
            return;
        }else {
            cb(tempObj);
            return;
        }
    });
}

// store orders aggregation functions

function StoreOrdersByStatus(db, fromDt, toDt, referalId, cb) {
    const collection = db.collection('orders');
    collection.aggregate([
        {
            $match: {
                datecreated: { $gte: fromDt, $lt: toDt },
                referalId: {
                    "$in": referalId
                  }
            }
        },
        {
            $group: {
                "_id": {
                    "status": "$status"
                },
                "price": { "$sum": "$price" },
                "Count": { "$sum": 1 }
            }
        }, {
            $project: {
                "Status": "$_id.status",
                "Value": "$price",
                "Count": "$Count",
                "_id": 0
            }
        }
    ]).toArray(function (err, result) {
        var temparray = [
            {
                "Status": "Accepted",
                "Count": 0,
                "Value": 0
            },
            {
                "Status": "Finished",
                "Count": 0,
                "Value": 0
            },
            {
                "Status": "Sent",
                "Count": 0,
                "Value": 0
            }
        ];
        if (err) {

            cb(temparray);
            return;
        }
        temparray.forEach(element => {
            result.forEach(element2 => {
                //console.log("element", element2)
                if (element.Status == element2.Status) {
                    element.Count = element2.Count;
                    element.Value = element2.Value;
                }
            });
        });
        cb(temparray);
        //console.log("tempArray---------------------------------------------", temparray)
        // return temparray;
    });
    
}

function getAllStoreOrders(db, fromDt, toDt, referalId, cb) {
    const collection = db.collection('orders');

    collection.aggregate([
        {
            $match: {
                datecreated: { $gte: fromDt, $lt: toDt },
                referalId: referalId
            }
        },
        {
            $group: {
                "_id": {
                    "_id": null,
                },
                "price": { "$sum": "$price" },
                "Count": { "$sum": 1 }
            }
        }, {
            $project: {
                "TotalOrdersReceived": "$Count",
                "Value": "$price",
                "_id": 0
            }
        }
    ]).toArray(function (err, result) {
        if (err) {
            var tempObj = {
                Status: "Received",
                Count: 0,
                Value: 0
            };
            cb(tempObj);
            return;
        }
        if (result.length > 0) {
            var tempObj = {
                Status: "Received",
                Count: result[0].TotalOrdersReceived || 0,
                Value: result[0].Value || 0,
            };
            cb(tempObj);
        } else {
            var tempObj = {
                Status: "Received",
                Count: 0,
                Value: 0
            };
            cb(tempObj);
        }

    });
}

function codStoreOrders(db, fromDt, toDt, referalId, cb) {
    const collection = db.collection('orders');
    collection.aggregate([
        {
            $match: {
                datecreated: { $gte: fromDt, $lt: toDt },
                iscod: true,
                referalId: referalId
            }
        },
        {
            $group: {
                "_id": {
                    "_id": null,
                },
                "price": { "$sum": "$price" },
                "Count": { "$sum": 1 }
            }
        }, {
            $project: {
                "codOrders": "$Count",
                "Value": "$price",
                "_id": 0
            }
        }
    ]).toArray(function (err, result) {
        console.log("cod store order result", result)
        if (err) {
            var tempObj = {
                Status: "iscod",
                Count: 0,
                Value: 0
            };
            cb(tempObj);
            return;
        }
        if (result.length > 0) {
            var tempObj = {
                Status: "iscod",
                Count: result[0].codOrders || 0,
                Value: result[0].Value || 0
            };
            cb(tempObj);
        } else {
            var tempObj = {
                Status: "iscod",
                Count: 0,
                Value: 0
            };
            cb(tempObj);
        }

    });
}

function paidStoreOrders(db, fromDt, toDt, referalId, cb) {
    const collection = db.collection('orders');
    collection.aggregate([
        {
            $match: {
                datecreated: { $gte: fromDt, $lt: toDt },
                ispaid: true,
                referalId: referalId
            }
        },
        {
            $group: {
                "_id": {
                    "_id": null,
                },
                "price": { "$sum": "$price" },
                "Count": { "$sum": 1 }
            }
        }, {
            $project: {
                "paidOrders": "$Count",
                "Value": "$price",
                "_id": 0
            }
        }
    ]).toArray(function (err, result) {
        if (err) {
            var tempObj = {
                Status: "ispaid",
                Count: 0,
                Value: 0
            };
            cb(tempObj);
            return;
        }
        if (result.length > 0) {
            var tempObj = {
                Status: "ispaid",
                Count: result[0].paidOrders || 0,
                Value: result[0].Value || 0
            };
            cb(tempObj);
        } else {
            var tempObj = {
                Status: "ispaid",
                Count: 0,
                Value: 0
            };
            cb(tempObj);
        }

    });
}

function UnconfirmedStoreOrders(db, fromDt, toDt, referalId, cb) {
    const collection = db.collection('orders');
    collection.aggregate([
        {
            $match: {
                datecreated: { $gte: fromDt, $lt: toDt },
                tag: "wait",
                referalId: referalId
            }
        },
        {
            $group: {
                "_id": {
                    "_id": null,
                },
                "price": { "$sum": "$price" },
                "Count": { "$sum": 1 }
            }
        }, {
            $project: {
                "UnconfirmedOrders": "$Count",
                "Value": "$price",
                "_id": 0
            }
        }
    ]).toArray(function (err, result) {
        if (err) {
            var tempObj = {
                Status: "UnConfirmed",
                Count: 0,
                Value: 0
            };
            cb(tempObj);
            return;
        }
        if (result.length > 0) {
            var tempObj = {
                Status: "UnConfirmed",
                Count: result[0].UnconfirmedOrders || 0,
                Value: result[0].Value || 0
            };
            cb(tempObj);
        } else {
            var tempObj = {
                Status: "UnConfirmed",
                Count: 0,
                Value: 0
            };
            cb(tempObj);
        }

    });
}

// YTD , CTD AND MTD Orders aggregation functions

function yearToDayOrders(db, cb) {
    const collection = db.collection('orders');
    collection.aggregate(
        [
            {
                "$match": {
                    "status": {
                        "$in": [
                            "Accepted",
                            "Sent",
                            "Finished"
                        ]
                    }
                }
            },
            {
                "$group": {
                    "_id": {},
                    "__alias_0": {
                        "$sum": "$price"
                    },
                    "Count": { "$sum": 1 }
                }
            },

            {
                "$project": {
                    "YTDOrders": "$Count",
                    "ytdvalue": "$__alias_0",
                }
            },
            {
                "$limit": 5000
            }
        ], {
        "allowDiskUse": true
    }
    ).toArray(function (err, result) {
        console.log("ytd result", result, err);
        if (result.length > 0) {
            cb(result);
        } else {
            result = [{
                "_id": {},
                "YTDOrders": 0,
                "ytdvalue": 0
            }]
            cb(result);
        }
    });
}

function currentDayOrders(db, fromDt, toDt, cb) {
    const collection = db.collection('orders');
    collection.aggregate(
        [
            {
                "$match": {
                    "datecreated": { $gte: fromDt, $lt: toDt },
                    "status": {
                        "$in": [
                            "Accepted",
                            "Sent",
                            "Finished"
                        ]
                    }
                }
            },
            {
                "$group": {
                    "_id": {},
                    "__alias_0": {
                        "$sum": "$price"
                    },
                    "Count": { "$sum": 1 }
                }
            },

            {
                "$project": {
                    "CTDOrders": "$Count",
                    "ctdvalue": "$__alias_0",
                }
            },
            {
                "$limit": 5000
            }
        ], {
        "allowDiskUse": true
    }
    ).toArray(function (err, result) {
        console.log("ctd result", result)
        if (result.length > 0) {
            cb(result[0]);
        } else {
            result = {
                "_id": {},
                "CTDOrders": 0,
                "ctdvalue": 0
            }
            cb(result);
        }

    });
}

function monthToDayOrders(db, fromDt, toDt, cb) {
    const collection = db.collection('orders');
    collection.aggregate(
        [
            {
                "$match": {
                    "datecreated": { "$gte": fromDt, "$lt": toDt },
                    "status": {
                        "$in": [
                            "Accepted",
                            "Sent",
                            "Finished"
                        ]
                    }
                }
            },
            {
                "$group": {
                    "_id": {},
                    "__alias_0": {
                        "$sum": "$price"
                    },
                    "Count": { "$sum": 1 }
                }
            },

            {
                "$project": {
                    "MTDOrders": "$Count",
                    "mtdvalue": "$__alias_0",
                }
            },
            {
                "$limit": 5000
            }
        ], {
        "allowDiskUse": true
    }
    ).toArray(function (err, result) {
        console.log("mtd result", result[0]);
        if (result.length > 0) {
            cb(result[0]);
        } else {
            result = {
                "_id": {},
                "MTDOrders": 0,
                "mtdvalue": 0
            }
            cb(result);
        }
    });
}

// YTD , CTD AND MTD Store Orders aggregation functions

function yearToDayStoreOrders(db, referalId, cb) {
    const collection = db.collection('orders');
    collection.aggregate(
        [
            {
                "$match": {
                    "status": {
                        "$in": [
                            "Accepted",
                            "Finished",
                            "Sent"
                        ]
                    },
                    referalId: {
                        "$in": referalId
                      }
                }
            },
            {
                "$group": {
                    "_id": {},
                    "__alias_0": {
                        "$sum": "$price",
                    },
                    "Count": { "$sum": 1 }
                }
            },
            {
                "$project": {
                    "YTDOrders": "$Count",
                    "ytdvalue": "$__alias_0",
                }
            }
        ], {
        "allowDiskUse": true
    }
    ).toArray(function (err, result) {
        console.log("ytd result", result);
        if (result.length > 0) {
            cb(result);
        } else {
            result = [{
                "_id": {},
                "YTDOrders": 0,
                "ytdvalue": 0
            }]
            cb(result);
        }
    });
}

function currentDayStoreOrders(db, fromDt, toDt, referalId, cb) {
    const collection = db.collection('orders');
    collection.aggregate(
        [
            {
                "$match": {
                    "datecreated": { $gte: fromDt, $lt: toDt },
                    "status": {
                        "$in": [
                            "Accepted",
                            "Finished",
                            "Sent"
                        ]
                    },
                    referalId: {
                        "$in": referalId
                      }
                }
            },
            {
                "$group": {
                    "_id": {},
                    "__alias_0": {
                        "$sum": "$price",
                    },
                    "Count": { "$sum": 1 }
                }
            },
            {
                "$project": {
                    "CTDOrders": "$Count",
                    "ctdvalue": "$__alias_0",
                }
            }
        ], {
        "allowDiskUse": true
    }
    ).toArray(function (err, result) {
        console.log("ctd result", result)
        if (result.length > 0) {
            cb(result[0]);
        } else {
            result = {
                "_id": {},
                "CTDOrders": 0,
                "ctdvalue": 0
            }
            cb(result);
        }

    });
}

function monthToDayStoreOrders(db, fromDt, toDt, referalId, cb) {
    const collection = db.collection('orders');
    collection.aggregate(
        [
            {
                "$match": {
                    "datecreated": { $gte: fromDt, $lt: toDt },
                    "status": {
                        "$in": [
                            "Accepted",
                            "Finished",
                            "Sent"
                        ]
                    },
                    referalId: {
                        "$in": referalId
                      }
                }
            },
            {
                "$group": {
                    "_id": {},
                    "__alias_0": {
                        "$sum": "$price",
                    },
                    "Count": { "$sum": 1 }
                }
            },
            {
                "$project": {
                    "MTDOrders": "$Count",
                    "mtdvalue": "$__alias_0",
                }
            }
        ], {
        "allowDiskUse": true
    }
    ).toArray(function (err, result) {
        console.log("mtd result", result)
        if (result.length > 0) {
            cb(result[0]);
        } else {
            result = {
                "_id": {},
                "MTDOrders": 0,
                "mtdvalue": 0
            }
            cb(result);
        }
    });
}

