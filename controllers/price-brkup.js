// mongo db connection 
var MONGO_DB_CONNECTION = process.env.MONGO_DB_CONNECTION || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);
// create uuid module import
var generateUuidModule = MODULE('generate-uuid');
exports.install = function () {
    ROUTE('/admin/prices-config', pricesConfigSave, ['post', 'cors']);
    ROUTE('/admin/prices-config', pricesConfigFetch, ['cors']);
}

async function pricesConfigSave() {
    var self = this;
    var data = self.body;
    var nosql = new Agent();
    var JOB_ID = generateUuidModule.createUUID();
    if (!data) {
        self.json({
            status: false,
            message: "no data"
        })
    } else {
        nosql.select('getPrices', 'configuration').make(function (builder) {
            builder.where('configurationName', 'Global_Prices');
            builder.first();
        });
        var getPrices = await nosql.promise('savePrices');
        if (getPrices != null) {
            nosql.update('savePrices', 'configuration').make(function (builder) {
                builder.where('configurationName', 'Global_Prices');
                builder.set('configurationDetails', data);
            });
        } else {
            nosql.insert('savePrices', 'configuration').make(function (builder) {
                builder.set('configurationName', 'Global_Prices');
                builder.set('configurationDetails', data);
            });
        }
        var savePrices = await nosql.promise('savePrices');
        self.json({
            status: true,
            message: "Saved Successfully"
        })

        console.log("PRICES_SAVE_TRIGGERED", new Date().toISOString(), JOB_ID, savePrices);
    }
}

// function to get data
async function pricesConfigFetch() {
	var self = this;
	var nosql = new Agent();
	nosql.select('getPrices', 'configuration').make(function (builder) {
		builder.where('configurationName', 'Global_Prices');
		builder.first();
	});
	var getPrices = await nosql.promise('getPrices');
	var json = getPrices.configurationDetails;
	//console.log("data",json)
	self.json(json);
}