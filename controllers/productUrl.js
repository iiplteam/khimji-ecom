var MONGO_DB_CONNECTION = process.env.MONGO_DB_CONNECTION || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
const assert = require('assert');
var slug = require('slug')

// mongo db long url
const MONGO_URL = process.env.MONGO_URL || 'mongodb://sowmya:iNNrxOhVfEdvsUaI@cluster0-shard-00-00-cnw2n.mongodb.net:27017,cluster0-shard-00-01-cnw2n.mongodb.net:27017,cluster0-shard-00-02-cnw2n.mongodb.net:27017/khimji_dev?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority';

// Database Name
const DB_NAME = process.env.DB_NAME || 'khimji_dev';
exports.install = function () {
    ROUTE('/api/category-product-insert', addCategory, ['post', 'cors']);
    ROUTE('/api/category-fetch', getCategory, ['cors']);
    ROUTE('/api/get-urls', getUrls, ['post', 'cors']);
    
}

async function addCategory() {
    var self = this;
    var body = self.body;
    var nosql = new Agent();
    var str = body.category.slug();
    var obj = {
        title: body.category,
        link: str,
        type: "category"
    }
    nosql.insert('addCat', 'product_category').make(function (builder) {
        builder.set(obj);
    });

    var addCat = await nosql.promise('addCat');
    if(addCat != null) {
        self.json({
            status: true,
            message:"Category created"
        })
    } else {
        self.json({
            status: false
        })
    }
}

async function getCategory() {
    var self = this;
    var nosql = new Agent();
    nosql.select('getCat', 'product_category').make(function (builder) {
        builder.where('type', "category");
    })

    var getCat = await nosql.promise('getCat');
    //console.log("getcat", getCat);
    if (getCat.length > 0) {
        self.json({
            status: true,
            data: getCat
        })
    } else {
        self.json({
            status: false
        })
    }
}

async function getUrls() {
    var self = this;
    var nosql = new Agent();
    var type = self.query.type;
    nosql.select('getCat', 'product_category').make(function (builder) {
        builder.where('type', type);
    })
    var getCat = await nosql.promise('getCat');
    if (getCat.length > 0) {
        self.json({
            status: true,
            data: getCat
        })
    } else {
        self.json({
            status: false
        })
    }
    $GET('Product', options, function (err, response) {
        if (err)
            return self.invalid().push(err);
        if (self.query.json == "1") {
            self.json(response);
        } else {
            self.view('~cms/product', response);
        }
    });
    
} 

async function addLinker() {
    var self = this;
    var nosql = new Agent();
    const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });
    client.connect(function (err, C) {
        assert.equal(null, err);
        const db = C.db(DB_NAME);
        const collection = db.collection('product');
        C.close();
        collection.aggregate([
            {
                $group: {
                    "_id": {
                        "linker": "$linker",
                        "name": "$name"
                    },
                    "Count": { "$sum": 1 }
                }
            }, {
                $project: {
                    "linker": "$_id.linker",
                    "name": "$_id.name",
                    "_id": 0
                }
            }
        ]).toArray(function (err, result) {
            console.log("result", result);
            //return;
            result.forEach(async element => {

                //var str = element.category.slug();

                var obj = {
                    title: element.name,
                    link: element.linker,
                    type: "detail"
                }
                nosql.insert('addCat', 'product_category').make(function (builder) {
                    builder.set(obj);
                });

                var addCat = await nosql.promise('addCat');
                console.log("addCat", addCat);

            });

        });

    });
}

async function addCategoryBulk() {
    var self = this;
    var nosql = new Agent();
    const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });
    client.connect(function (err, C) {
        assert.equal(null, err);
        const db = C.db(DB_NAME);
        const collection = db.collection('product');
        C.close();
        collection.aggregate([
            {
                $group: {
                    "_id": {
                        "category": "$category"
                    },
                    "Count": { "$sum": 1 }
                }
            }, {
                $project: {
                    "category": "$_id.category",
                    "_id": 0
                }
            }
        ]).toArray(function (err, result) {
            //console.log("result", result);
            result.forEach(async element => {

                var str = element.category.slug();

                var obj = {
                    title: element.category,
                    link: str,
                    type: "category"
                }
                nosql.insert('addCat', 'product_category').make(function (builder) {
                    builder.set(obj);
                });

                var addCat = await nosql.promise('addCat');
                console.log("addCat", addCat);

            });

        });

    });
}


