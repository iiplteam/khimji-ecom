var MONGO_DB_CONNECTION = process.env.MONGO_DB_CONNECTION || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);
var jwt = require('jsonwebtoken');


// jwt secret key 
var JWT_SECRET_KEY = process.env.JWT_SECRET_KEY || 'happi_jwt_secret';

const fcm = MODULE('firebaseNotification');
exports.install = function () {
    ROUTE('/api/user-fcm', addUserFcmToken, ['POST', 'cors', 10000]);
    ROUTE('/api/send-notify', sendFcmNotification, ['POST', 'cors', 10000]);
}

async function addUserFcmToken() {
    console.log("FCM TOKEN USER API TRIGGER-------------------")
    var self = this;
    var body = self.body;
    var nosql = new Agent();
    var JOB_ID = createUUID();
    var token = self.headers['x-auth'];
    // console.log("body", body);
    // console.log("token", token);
    if (token == null || token == undefined || token == "null") {
        //console.log("NO x-auth");
        var obj = {
            id: UID(),
            fcmToken: body.fcmToken,
            datecreated: new Date()
        };
        nosql.insert('AddFcm', 'fcmtoken_user').make(function (builder) {
            builder.set(obj);
        });

        var AddFcm = await nosql.promise('AddFcm');

        self.json({
            status: true
        });

    } else {
        //console.log("IF  x-auth");
        try {
            decoded = jwt.verify(token, JWT_SECRET_KEY);

            nosql.select('getFcm', 'fcmtoken_user').make(function (builder) {
                builder.and();
                builder.where('phone', decoded.phone);
            });

            var getFcm = await nosql.promise('getFcm');

            console.log("FCM_TRIGGER", JOB_ID, "FCM-VERIFY", getFcm);

            if (getFcm.length != 0) {
                self.json({
                    status: false,
                    message: "FCM token already added"
                });
                return;
            }

            var obj = {
                id: UID(),
                phone: decoded.phone,
                fcmToken: body.fcmToken,
                datecreated: new Date()
            };

            nosql.insert('AddFcm', 'fcmtoken_user').make(function (builder) {
                builder.set(obj);
            });

            var AddFcm = await nosql.promise('AddFcm');

            self.json({
                status: true
            });

        } catch (err) {
            console.log("err", err)
            self.json({
                state: false,
                message: "Sorry some thing went wrong"
            });
            return;
        }
    }
}


// Generate a UUID
function createUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function sendFcmNotification() {
    var self = this;
    var tokens = ['ekf8Vex6j9w:APA91bElgnMdlctDHni_By7X2uhiSvqX0HNp0OMfw7kbsJazOCtgMz77FQA_4ufVhH5M9NmCUD_qw3gANHF40wBBas25bZRMZpP16z0bbRXJU_scjXHgostizBXz96YgEijrByW2sVQm',
        'eXAfZyNn2MU:APA91bEPWrtTxhUT_MOQifC7wHcUVOoBfCwKuNCxedcUkX320zLzdYjbUXQk30VzPLsX2f42-ZzJ7T8u2PUlknB6UB7OyHbjeV0pT_f6VOBjSc05h-V1QZhLZDGVOenh20OK49UTq2Kx',
        'ejuVGgPxgEO_oA_wq7R8Eq:APA91bH2WNIsUYiwv-d3P3QJghsE0KluBS4kLtZe8p3z6E4cOTmiRsUmmNyZ3Ht9cLN7ZTSrD766ALGPOHFDOeX4YzemUxrSb--954mIb69XYAb_PEFWEptq3IVMbtKDyUSQYrWuX6GP',
         'd5vvcAiac0Apgmu1C3yYBP:APA91bEQyh2g_vxbJp--c9GQ_ygtDbWiSZQIK5lJpgmibhA7srUtXeNasYG8_bOKZSNMy_40p7ruRnCBQHWnW4qihR1ZNJBPnC93qYnn21yMV7riKz6XX5aMKIcmGf9dqAzqObiT4Rym'];
    var message = {
        data: {
        },
        notification: {
            title: self.body.title,
            body:  self.body.message
        }
    };
    fcm.sendNotification(message, tokens)
    self.json({status: true})
}



