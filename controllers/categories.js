var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
const assert = require('assert');

// mongo db long url
const MONGO_URL = process.env.MONGO_URL || 'mongodb://sowmya:iNNrxOhVfEdvsUaI@cluster0-shard-00-00-cnw2n.mongodb.net:27017,cluster0-shard-00-01-cnw2n.mongodb.net:27017,cluster0-shard-00-02-cnw2n.mongodb.net:27017/khimji_dev?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority';

// Database Name
const DB_NAME = process.env.DB_NAME || 'khimji_dev';

// mongo db connection 
var MONGO_DB_CONNECTION = process.env.MONGO_DB_CONNECTION || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);


exports.install = function () {
    // categories crud api's
    ROUTE('#admin/api/product-categories', saveCategories, ['post', 'cors']);
    ROUTE('#admin/api/product-categories', getCategories, ['cors']);
    ROUTE('#admin/api/product-categories/{id}', getCategoryById, ['cors']);
    ROUTE('#admin/api/product-categories/{id}', deleteCategory, ['delete', 'cors']);

    // categories filter admin api's
    ROUTE('#admin/api/product-main-category', getMainCat, ['cors']);
    ROUTE('#admin/api/category', getCat, ['post', 'cors']);
    ROUTE('#admin/api/sub-category', getSubCat, ['post', 'cors']);
    ROUTE('#admin/api/sub-one-category', getSubOneCat, ['post', 'cors']);

    // categories filter user api's
    ROUTE('/api/product-main-category', getMainCat, ['cors']);
    ROUTE('/api/category', getCat, ['post', 'cors']);
    ROUTE('/api/sub-category', getSubCat, ['post', 'cors']);
    ROUTE('/api/sub-one-category', getSubOneCat, ['post', 'cors']);

};


// product category crud functions

// function to create and update product category
function saveCategories() {
    var self = this;
    var nosql = new Agent();
    var body = self.body;

    var isUpdate = !!body.id;
    if (isUpdate) {
        body.dateupdated = new Date();
        nosql.update('updateCat', 'product_categories').make(function (builder) {
            builder.set(body);
            builder.where('id', body.id)
        });
    } else {
        body.datecreated = new Date();
        body.dateupdated = new Date();
        body.id = UID();
        nosql.insert('saveCat', 'product_categories').make(function (builder) {
            builder.set(body);
        });
    }

    nosql.exec(function (err, response) {
        if (err) {
            console.log("mongoerr", err);
        }
       // console.log("response", response.saveCat);
        self.json({
            status: true,
            message: "Success"
        })
    });
}

// function to get all product categories
async function getCategories() {
    var self = this;
    var opt = self.query;
    var nosql = new Agent();
    nosql.listing('getCats', 'product_categories').make(function (builder) {
        builder.page(opt.page || 1, opt.limit || 10);
        builder.sort('datecreated', 'desc');
    })
    var getCats = await nosql.promise('getCats');
    //console.log("getCats", getCats);
    if (getCats != null) {
        self.json({
            status: true,
            data: getCats
        })
    } else {
        self.json({
            status: false
        })
    }
}

// function to get product category by id
async function getCategoryById() {
    var self = this;
    var opt = self.params;
    var nosql = new Agent();
    //console.log("id", opt);
    nosql.select('getCat', 'product_categories').make(function (builder) {
        builder.where('id', opt.id);
        builder.first();
    });

    var getCat = await nosql.promise('getCat');
    if (getCat != null) {
        self.json({
            status: true,
            message: "Success",
            data: getCat
        })
    } else {
        self.json({
            status: false
        })
    }
}

// function to delete product category
async function deleteCategory() {
    var self = this;
    var opt = self.params;
    var nosql = new Agent();

    nosql.remove('deleteCat', 'product_categories').make(function (builder) {
        builder.where('id', opt.id);
    });

    var deleteCat = await nosql.promise('deleteCat');
    console.log("deleteCat", deleteCat);
    if (deleteCat > 0) {
        self.json({
            status: true,
            message: "Category Deleted Successfully"
        })
    } else {
        self.json({
            status: false
        })
    }
}


// category filter functions
async function getMainCat() {
    var self = this;
    const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });
    client.connect(function (err, C) {
        assert.equal(null, err);
        const db = C.db(DB_NAME);

        mainCatAggr(db, function (cat) {
            C.close();
            self.json(cat);
        });
    });
}

async function getCat() {
    var self = this;
    const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });
    client.connect(function (err, C) {
        assert.equal(null, err);
        const db = C.db(DB_NAME);

        catAggr(db, self.body.main_category, function (cat) {
            C.close();
            self.json(cat);
        });
    });
}

async function getSubCat() {
    var self = this;
    const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });
    client.connect(function (err, C) {
        assert.equal(null, err);
        const db = C.db(DB_NAME);

        subCatAggr(db, self.body.category, function (subcat) {
            C.close();
            self.json(subcat);
        });
    });
}

async function getSubOneCat() {
    var self = this;
    var body = self.body;
    console.log("body", body);
    var { result, err } = await subCatOneAggr(body.category, body.sub_category);
    if (err) {
        console.log("err", err)
    }

    self.json(result);
    // const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });
    // client.connect(function (err, C) {
    //     assert.equal(null, err);
    //     const db = C.db(DB_NAME);

    //     subCatOneAggr(db, self.body.category, self.body.sub_category, function (subcat) {
    //         C.close();
    //         self.json(subcat);
    //     });
    // });
}
// aggregation function to get the main categories
function mainCatAggr(db, cb) {
    const collection = db.collection('product_categories');
    collection.aggregate([
        {
            $group: {
                "_id": {
                    "main_category": "$main_category"
                },
                "Count": { "$sum": 1 }
            }
        }, {
            $project: {
                "main_category": "$_id.main_category",
                "_id": 0
            }
        }
    ]).toArray(function (err, result) {
        //console.log("err",err, result);
        var output = {
            status: false,
            message: "No Category"
        }
        if (result[0].main_category == null || result.length == 0) {
            cb(output);
            return;
        }
        cb(result);
    });
}

// aggregation function to get the categories
function catAggr(db, mainCat, cb) {
    const collection = db.collection('product_categories');
    collection.aggregate([
        {
            $match: {
                main_category: mainCat
            }
        },
        {
            $group: {
                "_id": {
                    "category": "$category"
                },
                "Count": { "$sum": 1 }
            }
        }, {
            $project: {
                "Category": "$_id.category",
                "_id": 0
            }
        }
    ]).toArray(function (err, result) {
        //console.log("err", err, result);
        var output = {
            status: false,
            message: "No Category"
        }
        if (result.length == 0 || result[0].Category == null) {
            cb(output);
            return;
        } else {
            result.forEach(element => {
                var display_name = element.Category.split('/ ');
                element.display_name = display_name[1]
            });
        }
        cb(result);
    });
}

// aggr function to get the sub categories
function subCatAggr(db, cat, cb) {
    const collection = db.collection('product_categories');
    collection.aggregate([
        {
            $match: {
                category: cat
            }
        },
        {
            $group: {
                "_id": {
                    "sub_category": "$sub_category"
                },
                "Count": { "$sum": 1 }
            }
        }, {
            $project: {
                "sub_category": "$_id.sub_category",
                "_id": 0
            }
        }
    ]).toArray(function (err, result) {
        //console.log("result", result);
        var output = {
            status: false,
            message: "No Sub Category"
        }
        if (result.length == 0 || result[0].sub_category == null) {
            cb(output);
            return;
        } else {
            result.forEach(element => {
                var display_name = element.sub_category.split(cat + ' / ');
                element.display_name = display_name[1]
            });
        }
        cb(result);
    });
}

//function to get the sub categories
async function subCatOneAggr(cat, subcat) {
    var nosql = new Agent();

    var nosql = new Agent();
    nosql.select('get_subcat_one', 'product_categories').make(function (builder) {
        builder.where('category', cat);
        builder.where('sub_category', subcat);
        builder.fields('sub_category_one');
        builder.first();
    });
    try {
        var output = {
            status: false,
            message: "No Sub One Category"
        }
        var data = await nosql.promise('get_subcat_one');
        //console.log("data", data);
        if (data == null || data == undefined) {
            return { result: output, err: null };
        }
       
        var resultArr = [];
        
        data.sub_category_one = data.sub_category_one.split(',').trim();
        data.sub_category_one.forEach(element => {
            var result = {};
            //console.log("element", element);
            var display_name = element.split(subcat+' / ');
            result.sub_category_one = element;
            result.display_name = display_name[1]
            resultArr.push(result);
        });
        //console.log("resultArr",resultArr);
        return { result: resultArr, err: null };
    } catch (err) {
        return { result: null, err: err };
    }
}


