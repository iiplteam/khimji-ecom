var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
const assert = require('assert');
var jwt = require('jsonwebtoken');
var request = require('request');

// mongo db long url
const MONGO_URL = process.env.MONGO_URL || 'mongodb://sowmya:iNNrxOhVfEdvsUaI@cluster0-shard-00-00-cnw2n.mongodb.net:27017,cluster0-shard-00-01-cnw2n.mongodb.net:27017,cluster0-shard-00-02-cnw2n.mongodb.net:27017/khimji_dev?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority';

// Database Name
const DB_NAME = process.env.DB_NAME || 'khimji_dev';

// mongo db connection 
var MONGO_DB_CONNECTION = process.env.MONGO_DB_CONNECTION || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);

// importing pincode verify module
var pincodeVerify = MODULE('pincodetest');

// create uuid module import
var generateUuidModule = MODULE('generate-uuid');

// send sms import modile
var smsModule = MODULE('sms');


exports.install = function () {
    ROUTE('/api/bot/device', getDevices, ['post', 'cors']);
    ROUTE('/api/bot/brand', getBrands, ['post', 'cors']);
    ROUTE('/api/bot/model', getModels, ['post', 'cors']);
    ROUTE('/api/bot/color', getColors, ['post', 'cors']);
    ROUTE('/api/bot/specification', getSpecifications, ['post', 'cors']);
    ROUTE('/api/bot/create-order', orderCreateBot, ['post', 'cors']);
}



async function orderCreateBot() {
    var self = this;
    var nosql = new Agent();
    var JOB_ID = generateUuidModule.createUUID();
    var no = NOSQL('orders');
    var body = self.body;
    var pincodedata = await pincodeVerify.pincodeVerify(body.billingAddress.zip);
    if (body.quantity == null) {
        self.json({
            status: false,
            message: "Invalid Quantity"
        })
        return;
    }
    var obj = {
        name: body.billingAddress.name,
        firstname: body.billingAddress.name,
        lastname: body.billingAddress.name,
        email: body.email,
        phone: body.billingAddress.phone,
        street: body.billingAddress.street + " " + body.billingAddress.area,
        zip: body.billingAddress.zip,
        city: body.billingAddress.city,
        country: "IN",
        billingstreet: body.billingAddress.street + " " + body.billingAddress.area,
        billingcity: body.billingAddress.city,
        billingcountry: 'IN',
        billingzip: body.billingAddress.zip,
        delivery: "",
        payment: "",
        company: "",
        companyid: "",
        companyvat: "",
        datecreated: new Date(),
        iduser: "",
        iscompany: "",
        isnewsletter: true,
        isterms: true,
        ispaid: false,
        iscod: false,
        referalId: '',
        isnewsletter: undefined,
        isemail: undefined,
        isterms: undefined,
        internal_type: "waiting for update",
        tag: "wait",
        order_from: "chatBot",
        area: pincodedata != "Invalid Pincode" ? `${pincodedata.Districtname},${pincodedata.statename}` : "Invalid Pincode"
    };
    console.log("CREATE_ORDER_V2_GUEST", JOB_ID, "FIRST_FILEDS", JSON.stringify(obj));

    obj.pickupCity = "N/A";
    obj.pickupLocation = "N/A";
    obj.pickupAddress = "N/A";
    obj.pickupMobile = "N/A";
    obj.pickupState = "N/A";
    obj.pickupPincode = "N/A";
    obj.ispickup = false;

    obj.deliverycity = body.billingAddress.city;
    obj.deliverycountry = 'India';
    obj.deliveryfirstname = body.billingAddress.name;
    obj.deliverylastname = "";
    obj.deliveryphone = body.billingAddress.phone;
    obj.deliverystreet = body.billingAddress.street + " " + body.billingAddress.area;
    obj.deliveryzip = body.billingAddress.zip;


    console.log("CREATE_ORDER_V2_GUEST", JOB_ID, "SECOND_FILEDS", JSON.stringify(obj));
    // fetch product details 
    var { product, err } = await FetchProduct(body.productId);
    if (product == "Product Out of Stock") {
        self.json({
            status: false,
            message: "Product Out of Stock"
        })
        return;
    }
    if (err || product == null) {
        self.json({
            status: false,
            message: "Invalid Product"
        })
        return;
    }

    var totalShippingPrice = 0;
    var SubTotal = 0;
    var TotalQuantity = 0;
    if (product.shippingPrice != undefined) {
        totalShippingPrice += product.shippingPrice;
    }
    SubTotal += (product.payPrice * body.quantity);
    TotalQuantity += body.quantity;

    //return;
    obj.version = "V2";
    obj.id = UID();
    obj.items = product;

    obj.totalShippingPrice = totalShippingPrice;
    obj.SubTotal = SubTotal;
    obj.count = TotalQuantity;
    obj.price = totalShippingPrice + SubTotal;
    obj.number = createNumber(no);

    // return;
    console.log("CREATE_ORDER_V2_GUEST", JOB_ID, "CART", JSON.stringify(obj));

    nosql.insert('order', 'orders').make(function (builder) {
        builder.set(obj);
    });

    console.log("CREATE_ORDER_V2_GUEST", JOB_ID, "ORDER_SAVE_PRE", JSON.stringify(obj));

    var cart = await nosql.promise('order');

    console.log("CREATE_ORDER_V2_GUEST", JOB_ID, "ORDER_POST", JSON.stringify(cart));

    self.json({ success: true, data: obj });

    var message = `Thankyou for placing order in happimobiles.com. Your order is waiting for confirmation for more information https://happimobiles.com/checkout/${obj.id}`;

    smsModule.sendSMS(body.billingAddress.phone, message);
}

async function getDevices() {
    var self = this;
    const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });
    client.connect(function (err, C) {
        assert.equal(null, err);
        const db = C.db(DB_NAME);

        devicesAggr(db, function (device) {
            C.close();
            self.json(device);
        });
    });
}

async function getBrands() {
    var self = this;
    const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });
    client.connect(function (err, C) {
        assert.equal(null, err);
        const db = C.db(DB_NAME);

        brandsAggr(db, self.body.device, function (device) {
            C.close();
            self.json(device);
        });
    });
}

async function getModels() {
    var self = this;
    var body = self.body;
    console.log("body", body);
    var { result, err } = await modelsAggr(body.device, body.brand);
    if (err) {
        console.log("err", err)
    }

    self.json(result);
    // const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });
    // client.connect(function (err, C) {
    //     assert.equal(null, err);
    //     const db = C.db(DB_NAME);

    //     modelsAggr(db, self.body.device, self.body.model, function (models) {
    //         C.close();
    //         self.json(models);
    //     });
    // });
}

async function getColors() {
    var self = this;
    var body = self.body;
    console.log("body", body);
    var { result, err } = await colorAggr(body.device, body.brand, body.model);
    if (err) {
        console.log("err", err)
    }

    self.json(result);
}

async function getSpecifications() {
    var self = this;
    var body = self.body;
    console.log("body", body);
    var { result, err } = await specsAggr(body.device, body.brand, body.model);
    if (err) {
        console.log("err", err)
    }

    self.json(result);
}

// aggregation 
function devicesAggr(db, cb) {
    const collection = db.collection('botinfo');
    collection.aggregate([
        {
            $group: {
                "_id": {
                    "Device": "$Device"
                },
                "Count": { "$sum": 1 }
            }
        }, {
            $project: {
                "Device": "$_id.Device",
                "_id": 0
            }
        }
    ]).toArray(function (err, result) {
        cb(result);
    });
}

function brandsAggr(db, device, cb) {
    const collection = db.collection('botinfo');
    collection.aggregate([
        {
            $match: {
                Device: device
            }
        },
        {
            $group: {
                "_id": {
                    "Brand": "$Brand"
                },
                "Count": { "$sum": 1 }
            }
        }, {
            $project: {
                "Brand": "$_id.Brand",
                "_id": 0
            }
        }
    ]).toArray(function (err, result) {
        cb(result);
    });
}

async function modelsAggr(device, brand) {
    var nosql = new Agent();

    var nosql = new Agent();
    nosql.select('getModel', 'botinfo').make(function (builder) {
        builder.where('Device', device);
        builder.where('Brand', brand);
        builder.fields('Model');
    });
    try {
        var data = await nosql.promise('getModel');

        console.log("data", data);
        // data.map(function (x) {
        //     var model = x.Model
        //     var model = model.replace(/\s*$/, "");
        //     x.Model = model;
        // })
        return { result: data, err: null };
    } catch (err) {
        return { result: null, err: err };
    }
}

async function colorAggr(device, brand, model) {
    var nosql = new Agent();

    var nosql = new Agent();
    nosql.select('getColor', 'botinfo').make(function (builder) {
        builder.where('Device', device);
        builder.where('Brand', brand);
        builder.where('Model', model);
        builder.fields('Colors');
        builder.first();
    });
    try {
        var data = await nosql.promise('getColor');
        //console.log("data", data);
        var colors = [];
        data.Colors = data.Colors.split(',').trim();
        data.Colors.map(function (x) {
            var obj = {
                color: ""
            }
            if (x != "") {
                obj.color = x;
                colors.push(obj)
            }
        })
        data.Colors = colors;

        return { result: data, err: null };
    } catch (err) {
        return { result: null, err: err };
    }
}

async function specsAggr(device, brand, model) {
    var nosql = new Agent();
    nosql.select('getSpecs', 'botinfo').make(function (builder) {
        builder.where('Device', device);
        builder.where('Brand', brand);
        builder.where('Model', model);
        builder.fields('Specs');
        builder.first();
    });
    try {
        var data = await nosql.promise('getSpecs');
        //console.log("data", data);
        var specifications = [];
        data.Specs = data.Specs.split(',').trim();
        data.Specs.map(function (x) {
            var obj = {
                specification: ""
            }
            if (x != "") {
                obj.specification = x;
                specifications.push(obj)
            }
        })
        data.Specs = specifications;

        return { result: data, err: null };
    } catch (err) {
        return { result: null, err: err };
    }
}

function createNumber(nosql) {
    var year = F.datetime.getFullYear();
    var key = 'numbering' + year;
    var number = (nosql.get(key) || 0) + 1;
    nosql.set(key, number);
    return (year + '000001').parseInt() + number;
}

// function to fetch product
async function FetchProduct(id) {
    var nosql = new Agent();
    //console.log("id", id)
    nosql.select('getProduct', 'product').make(function (builder) {
        builder.where('id', id);
        builder.fields('name', 'id', 'payPrice', 'shippingPrice', 'pictures', 'stock', 'ftrBrand', 'offerdesc', 'iscod', 'ispickup', 'stock', 'PinelabsproductCode');
        builder.first();
    });
    try {
        var product = await nosql.promise('getProduct');
        if (product.stock == 0) {
            return { product: "Product Out of Stock", err: null }
        }
        //console.log("productsssssssss", product);
        if (product.shippingPrice == null) {
            product.shippingPrice = 0
        }
        if (product.payPrice == null) {
            product.payPrice = 0
        }

        return { product: product, err: null };
    } catch (err) {
        return { product: null, err: err };
    }

}


// db.getCollection("botinfo").find().forEach(function (docs) {
//     var model = docs.Model.replace(/\s*$/, "");
//     docs.Model = model
//     db.getCollection("product_orders").save(docs);
//     print("UPDATED : " + obj.name);
// });