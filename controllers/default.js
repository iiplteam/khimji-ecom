const fs = require('fs');

// mongo db connection 
var MONGO_DB_CONNECTION = process.env.MONGO_DB_CONNECTION || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);

exports.install = function () {
	//ROUTE('/pages/*', view_cms);
	ROUTE('/', view_home);
	// Posts

	ROUTE('/my-account', view_account);
	ROUTE('/our-stores', view_Stores);
	ROUTE('/my-account-update', view_accountUpdate);
	ROUTE('/cart', view_checkout);
	ROUTE('/product', view_product);
	ROUTE('/my-wishlist', view_wishlist);
	ROUTE('/my-orders', view_orderslist);
	ROUTE('/exchange', view_exchange);
	ROUTE('/about', view_aboutus);
	ROUTE('#posts', view_posts, ['*Post']);
	ROUTE('#post', view_posts_detail, ['*Post']);
	ROUTE('#notices', view_notices, ['*Notice']);

	//ROUTE('#menu', view_menu);
};

function view_Stores() {
	var self = this;
	var model = {};
	model.cities = [
		{
			"storeName": "Flagship Store",
			"address": "3rd Floor,621,Janpath,Saheed Nagar,Bhubaneswar",
			"phone": "0674-6697777"
		},
		{
			"storeName": "CS PUR",
			"address": "Plot No:309,District Centre,Chandrasekharpur,BBSR-751016",
			"phone": "0674-2747677"
		}
		,
		{
			"storeName": "Cuttack",
			"address": "5558/834,Cantonment Road,Cuttack-753001",
			"phone": "0671-2970099"
		},
		{
			"storeName": "BERHAMPUR",
			"address": "Venkataraju Arcade,Berhampur-760002",
			"phone": "0680-2250600"
		},
		{
			"storeName": "Rourkela",
			"address": "Kesar Bhawan,Main Road,Rourkela-769001",
			"phone": "0661-2521777"
		}


	];
	model.headerJson = JSON.parse(fs.readFileSync(__dirname + '/../public/header.json'));
	if (self.query.json) {
		self.json(model);
		return;
	}
	self.layout('layout-new');
	self.view('~eshop/our-stores', model);
}
function view_aboutus() {

	var self = this;
	var model = {};
	model.headerJson = JSON.parse(fs.readFileSync(__dirname + '/../public/header.json'));
	if (self.query.json) {
		self.json(model);
		return;
	}
	self.layout('layout-new');
	self.view('~partials/about', model);
}


async function view_home() {
	var self = this;
	var model = {};
	model.headerJson = JSON.parse(fs.readFileSync(__dirname + '/../public/header.json'));
	model.homepageJson = JSON.parse(fs.readFileSync(__dirname + '/../public/homepage.json'));
	// nosql.select('getHomepage', 'configuration').make(function (builder) {
	// 	builder.where('configurationName', 'Homepage_Json');
	// 	builder.first();
	// });
	// var homepageData = await nosql.promise('getHomepage');
	// var homepageJson = homepageData.configurationDetails;
	console.log("DDDDDDDIR NAME", __dirname);
	if (self.query.json) {
		self.json(model);
		return;
	}
	self.layout('layout-new');
	self.view('home', model);
}

function view_account() {
	var self = this;
	var model = {};
	model.headerJson = JSON.parse(fs.readFileSync(__dirname + '/../public/header.json'));
	self.layout('layout-new');
	self.view('~eshop/new-account', model);
}

function view_wishlist() {

	var self = this;
	var model = {};
	model.headerJson = JSON.parse(fs.readFileSync(__dirname + '/../public/header.json'));
	self.layout('layout-new');
	self.view('~eshop/wishlist', model);
}

function view_exchange() {

	var self = this;

	self.layout('layout-new');
	self.view('~cms/exchange');
}

function view_orderslist() {

	var self = this;
	var model = {};
	model.headerJson = JSON.parse(fs.readFileSync(__dirname + '/../public/header.json'));
	self.layout('layout-new');
	self.view('~eshop/orders-list', model);
}

function view_accountUpdate() {

	var self = this;
	var model = {};
	model.headerJson = JSON.parse(fs.readFileSync(__dirname + '/../public/header.json'));
	self.layout('layout-new');
	self.view('~eshop/account-update', model);
}

function view_cms() {
	//console.log("view CMS called------------------");

	var self = this;
	self.CMSpage();
}

function view_checkout() {

	var self = this;
	var model = {};
	model.headerJson = JSON.parse(fs.readFileSync(__dirname + '/../public/header.json'));
	self.layout('layout-new');
	self.view('~eshop/checkout', model);
}

function view_product() {

	var self = this;

	self.layout('layout-new');
	self.view('~cms/product-new');
}

function view_posts() {
	var self = this;
	var options = {};

	options.page = self.query.page;
	options.published = true;
	options.limit = 10;
	// options.category = 'category_linker';

	self.sitemap();
	self.$query(options, self.callback('posts'));
}

function view_posts_detail(linker) {

	var self = this;
	var options = {};

	options.linker = linker;
	// options.category = 'category_linker';

	self.$workflow('render', options, function (err, response) {

		if (err) {
			self.throw404();
			return;
		}

		self.sitemap();
		self.sitemap_replace(self.sitemapid, response.name);
		self.view('cms/' + response.template, response);
	});
}

function view_notices() {
	var self = this;
	var options = {};

	options.published = true;

	self.sitemap();
	self.$query(options, self.callback('notices'));
}


// ON('error404', function(req, res, exception) {
// 	var self = this;
// 	//self.throw404();
// 	res.render('page404');

// });
