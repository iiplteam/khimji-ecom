var axios = require('axios');
var md5 = require("md5");
const crypto = require('crypto');
const request = require('request');
const { callbackPromise } = require('nodemailer/lib/shared');
var MONGO_DB_CONNECTION = process.env.MONGO_DB_CONNECTION || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);
var transaction = MODULE('transactionUpdate');
var generateUuidModule = MODULE('generate-uuid');



exports.install = function () {
    ROUTE('/api/bajaj/emiOptions', getEmiOptions, ['post', 'cors']);
    ROUTE('/api/bajaj/test', test, ['get', 'cors']);
    ROUTE('/api/bajaj-emi-return', bajajreturn, ['post', 'cors']);
}



async function test() {
    var self = this;
    var body = self.query;
    var orderDetails = await getOrder(body.orderId);
    console.log("orderDetails", orderDetails);
    transaction.saveOrderTransaction(orderDetails.id, orderDetails.price);

    var trasactionDetails = await getTrasactionId(body.orderId);
    console.log("trasactionId", trasactionDetails.transactionid);

    // var plaintext = {
    //     "DealerCode": "5756",
    //     "LoanAmount": orderDetails.price,
    //     "Tenure": "12",
    //     "PinCode": "411014",
    //     "OrderNo": orderDetails.id,
    //     "RequestID": trasactionDetails.transactionid,
    //     "ValidationKey": "B9G744OTAL1108200207248TRZYX4NH9",
    //     "Dealer_code_hex": "0010K00001i2LxIQAU",
    //     "Identifier": "",
    //     "InvoiceAmt": orderDetails.price,
    //     "ModelID": "92800",
    //     "ReturnURL": "http://localhost:8000/api/bajaj-emi-return"
    // };

    // static
    var plaintext = {
        "DealerCode": "5756",
        "LoanAmount": "41590",
        "Tenure": "12",
        "PinCode": "411014",
       // "OrderNo": "HAPPY795",
        "OrderNo": body.orderNo,
        //"RequestID": "HAPPY8095",
        "RequestID": body.requestId,
        "ValidationKey": "4499162128466537",
        "Dealer_code_hex": "0011t00000ShtzHAAR",
        "Identifier": "",
        "InvoiceAmt": "41590",
        "ModelID": "92800",
        "ReturnURL": "https://qa-happi.iipl.work/api/bajaj-emi-return"
    };


    const crypto = require('crypto');
    const ENCRYPTION_KEY = 'B9G744OTAL1108200207248TRZYX4NH9'; // Must be 256 bits (32 characters)
    const IV_LENGTH = 16; // For AES, this is always 16
    //console.log("plaintext", plaintext, "ENCRYPTION_KEY", ENCRYPTION_KEY, "IV_LENGTH", IV_LENGTH);
    let iv = '1234567887654321';
    iv = new Buffer(iv, "binary");
    console.log("IV", iv.toString('hex'), iv);
    let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);
    let encrypted = cipher.update(JSON.stringify(plaintext));
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    var enc = encrypted.toString('base64');
    var seal = md5(enc + ENCRYPTION_KEY);
  //  console.log('encrypted', enc);
    //console.log('md5', seal);
    this.view('bajaj', {
        EncReqData: encrypted.toString('base64'),
        iv: seal
    });
}

async function bajajreturn() {
    var self = this;
    console.log("res", self.body);

    const ENCRYPTION_KEY = 'B9G744OTAL1108200207248TRZYX4NH9';
    let iv = '1234567887654321';
    // iv = new Buffer(iv, "binary");
    const decipher = crypto.createDecipheriv('aes-256-cbc', ENCRYPTION_KEY, iv);
    // decipher.setAuthTag(tag);
    //   decipher.setAAD(aad, {
    //     plaintextLength: self.body.EncResData.length
    //   });
    //let buff = new Buffer(self.body.EncResData, 'base64');
    var receivedPlaintext = decipher.update(self.body.EncResData, 'base64', null, 'utf8');
    try {
        receivedPlaintext += decipher.final();
    } catch (err) {
        console.error('Authentication failed!', err);
        return;
    }
    console.log("bajajreturn receivedPlaintext--------", JSON.parse(receivedPlaintext));
    // sample receivedPlaintext
    // {
    //   "REQSEQ": "886",
    //   "ValidDBKey": "B9G744OTAL1108200207248TRZYX4NH9",
    //   "AdvanceEMIAmt": "10398",
    //   "ProcessingFees": "450",
    //   "TotalDownpayment": "10848",
    //   "GrossTenor": "24",
    //   "AdvanceEMITenor": "6",
    //   "ResDateTime": "20201007103235",
    //   "LoanAmt": "41590",
    //   "DealID": "CS028160163720",
    //   "ResponseDesc": "Success",
    //   "ResponseCode": "00",
    //   "RequestID": "HAPPY8061"
    // }
    // if response status is 0 then hit the function call Requery()

    if (JSON.parse(receivedPlaintext).ResponseCode == "00") {
        // var plaintext = {
        //   "DEALERID": receivedPlaintext.DealID, // dynamic DealerCode in test
        //   "VALKEY": "6186878960985226", // static
        //   "REQID": receivedPlaintext.REQSEQ, // dynamic // auto increment
        //   "REQUERYID": receivedPlaintext.RequestID, // RequestID in test 
        //   "ACQCHNLID": "22" // static
        // };
        Requery(JSON.parse(receivedPlaintext),self.body.DealerID, function (response1) {
            console.log('SUCCESS response1', response1);
            //console.log("ouoiuoi", response1);
            return self.json(response1)
        });

    } else {
        console.log("FAIL -----------------------------");
        // Requery(JSON.parse(receivedPlaintext), function (response1) {
        //     console.log('FAIL response1', response1);
        //     //console.log("ouoiuoi", response1);
        //     return self.json(response1)
        // });
        return self.json({
          status: false,
          message: JSON.parse(receivedPlaintext).ResponseDesc
        })
    }

}

function Requery(receivedPlaintext,dealerId ,callback1) {
    console.log("receivedPlaintext in requery ->", receivedPlaintext);
    //dynamic
    var plaintext = {
        "DEALERID": dealerId, // dynamic DealerCode in test
        "VALKEY": "6186878960985226", // static
        "REQID": receivedPlaintext.REQSEQ, // dynamic // auto increment
        "REQUERYID": receivedPlaintext.RequestID, // RequestID in test 
        "ACQCHNLID": "22" // static
    };

    // static
    // var plaintext = {
    //     "DEALERID": "5756",  // dynamic DealerCode in test
    //     "VALKEY": "6186878960985226", // static
    //     "REQID": "CDORD00121", // dynamic // auto increment
    //     "REQUERYID": "HAPPY8095", // RequestID in test 
    //     "ACQCHNLID": "22" // static
    // };
    const crypto = require('crypto');
    const ENCRYPTION_KEY = 'sOnfGB3atf4UYZggYGQQjzCrZ9XeUgNn'; // Must be 256 bits (32 characters)
    const IV_LENGTH = 16; // For AES, this is always 16
    //console.log("plaintext", plaintext, "ENCRYPTION_KEY", ENCRYPTION_KEY, "IV_LENGTH", IV_LENGTH);
    let iv = '1234567887654321';
    iv = new Buffer(iv, "binary");
    console.log("IV", iv.toString('hex'), iv);
    let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);
    let encrypted = cipher.update(JSON.stringify(plaintext));
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    var enc = encrypted.toString('base64');
    var seal = md5(enc + ENCRYPTION_KEY);
    //console.log('encrypted', enc);
    //console.log('md5', seal);

    // hit a third part api. and response will be a encrypt key

    var options = {
        'method': 'POST',
        'url': 'https://bfl2.in.worldline.com/WorldlineInterfaceEnqRequery/WorldlineInterfaceEnhanceRequery.svc/ENQRequest',
        'headers': {
            'SealValue': seal,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(enc)

    };
    request(options, function (error, response) {
        if (error) throw new Error(error);
        //console.log("rrrrrr", response.body);
        var data = response.body.split("|");
        console.log("keyyyyyyyyyyyyyy", data[0]);
        // seperate with pipe send it to the bajajrequeryreturn()
        var out = bajajrequeryreturn(data[0]);
        callback1(out);

    });
}

function bajajrequeryreturn(key) {
    const ENCRYPTION_KEY = 'sOnfGB3atf4UYZggYGQQjzCrZ9XeUgNn';
    let iv = '1234567887654321';
    const decipher = crypto.createDecipheriv('aes-256-cbc', ENCRYPTION_KEY, iv);
    var receivedPlaintext = decipher.update(
        JSON.stringify(key),
        'base64', null, 'utf8');
   // console.log("bajajrequeryreturn data ----------------------------", receivedPlaintext);
    try {
        receivedPlaintext += decipher.final();
    } catch (err) {
        console.error('Authentication failed!', err);
        return obj = {
            status: false,
            data: "Authentication failed!"
        }
    }

    console.log("BAJAJ REQUERY RETURN DATA ", JSON.parse(receivedPlaintext));
    //let buff = new Buffer(receivedPlaintext, 'base64');
    return obj = {
        status: true,
        data: receivedPlaintext
    }
}



async function getOrder(id) {
    var nosql = new Agent();
    //console.log("id", id)
    nosql.select('getOrder', 'orders').make(function (builder) {
        builder.where('id', id);
        builder.fields('id', 'payPrice', 'price', 'bajajcode');
        builder.first();
    });

    var order = await nosql.promise('getOrder');
    //console.log("order",order);
    return order;
}

async function getTrasactionId(id) {
    var nosql = new Agent();
    //console.log("id", id)
    nosql.select('getOrderTransaction', 'transaction_details').make(function (builder) {
        builder.where('orderid', id);
        builder.fields('transactionid');
        builder.sort('datecreated', 'desc')
        builder.first();
    });

    var orderTranscation = await nosql.promise('getOrderTransaction');
    console.log("orderTranscation", orderTranscation);
    return orderTranscation;
}

async function saveOrderTransactionUpdate(orderId, amount) {
    var JOB_ID = generateUuidModule.createUUID();
    var nosql = new Agent();
    var obj = {
        transactionid: JOB_ID,
        orderid: orderId,
        bajajAmount: amount,
        datecreated: new Date()
    };

    nosql.insert('saveTransaction', 'transaction_details').make(function (builder) {
        builder.set(obj);
    });

    var saveTransaction = await nosql.promise('saveTransaction');
    console.log("SAVE TRASACTION TRIGGERED ", JOB_ID, saveTransaction);
}


async function getEmiOptions() {
    var self = this;
    var price = self.body.price;
    var options = {
        method: 'post',
        url: "",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `Token ${DELIVERY_TOKEN}`
        },
        data: qs.stringify({
            'format': 'json',
            'data': JSON.stringify(obj)
        })
    }
    var body = await axios.request(options);
    self.json({ status: true, data: body })
}
