var axios = require('axios');
var MONGO_DB_CONNECTION = process.env.MONGO_DB_CONNECTION || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);
var moment = require('moment');
var qs = require('qs');

// create uuid module import
var generateUuidModule = MODULE('generate-uuid');

// order push to  delivery variables 
const WARE_HOUSE = process.env.WARE_HOUSE || "HAPPI EXPRESS";
const DELIVERY_CLIENT = process.env.DELIVERY_CLIENT || "HAPPIEXPRESS";
const DELIVERY_CREATE_URL = process.env.DELIVERY_CREATE_URL || 'https://track.delhivery.com/api/cmu/create.json';
const DELIVERY_TOKEN = process.env.DELIVERY_TOKEN || 'abba9e0e8aeb2b383728c78568ccf1cfe4d17904';

// deliver status get variables
const DELIVERY_GET_URL = process.env.DELIVERY_GET_URL || 'https://track.delhivery.com/api/v1/packages/json/';
const DELIVERY_GET_TOKEN = process.env.DELIVERY_GET_TOKEN  || 'abba9e0e8aeb2b383728c78568ccf1cfe4d17904';
const Sentry = require('@sentry/node');
// or use es6 import statements
// import * as Sentry from '@sentry/node';

Sentry.init({ dsn: 'https://8a7b8edb176249de940978c69b383c21@o386444.ingest.sentry.io/5353190' });

exports.install = function () {
    ROUTE('/api/delivery/', order_push_to_delhivery, ["post",'cors' ,10000]);
    ROUTE('/api/delivery/{AWB}', getOrder_delivery_details, ['cors']);

};

// https://staging-express.delhivery.com/api/v1/packages//?parameter

async function order_push_to_delhivery() {
    var self = this;
    var orderId = self.body.orderId
    var order = await getOrder(orderId);
    var JOB_ID = generateUuidModule.createUUID();
    if (order != null) {
        if(order.area == "Invalid Pincode") {
            self.json({
                status: false,
                message:"Invalid Pincode"
            })
            return;
        }
        var paymentMode = null;
        var codAmount = 0;
        if (order.ispaid) {
            paymentMode = "Prepaid";
        }
        if (order.iscod) {
            paymentMode = "COD";
            codAmount = order.price;
        }
        if(paymentMode == null) {
            self.json({
                status: false,
                message:"Uncofirmed order"
            });
            return;
        }

        var product_names = "";
        order.items.forEach(function(e){
            product_names +=  e.name + " ";
        });
        var test = {
            shipments: [{
                "add": order.deliverystreet,
                "address_type": "home",
                "phone": order.deliveryphone,
                "payment_mode": paymentMode,
                "name": order.deliveryfirstname,
                "pin": order.deliveryzip,
                "order": order.id,
                "client": DELIVERY_CLIENT,
                "city": order.deliverycity,
                "weight": `${self.body.weight}(gms)`,
                "cod_amount": codAmount,
                "total_amount": order.price,
                "state": order.area.split(",")[1].trim(),
                "dangerous_good": "False",
                "order_date": moment(order.datecreated).format('YYYY-MM-DD hh:mm:ss'),
                "country": "India",
                "return_name": WARE_HOUSE,
                "plastic_packaging": "true",
                "quantity": order.count + ""
            }],
            "pickup_location": { "name": WARE_HOUSE }
        };

        var obj = {
            "pickup_location": {
              "pin": "110034",
              "add": "H.No 2-56/2-33, Plot No 1303, Khanamet, Ayyappa Society, Madhapur, Hyderabad, TELANGANA, 500081, India.",
              "phone": "+919100574444",
              "state": "TELANGANA",
              "city": "Hyderabad",
              "country": "India",
              "name": "HAPPI EXPRESS"
            },
            "shipments": [
              {
                "return_name": "HAPPI EXPRESS",
                "return_pin": "500081",
                "return_city": "Hyderabad",
                "return_phone": "+919100574444",
                "return_add": "H.No 2-56/2-33, Plot No 1303, Khanamet, Ayyappa Society, Madhapur, Hyderabad, TELANGANA, 500081, India.",
                "return_state": "Hyderabad",
                "return_country": "India",
                "order": order.id,
                "phone":  order.deliveryphone,
                "products_desc": product_names,
                "cod_amount": codAmount,
                "name": order.deliveryfirstname,
                "country": "India",
                "weight": `${self.body.weight}(gms)`,
                "seller_inv_date": moment(order.datecreated).format('YYYY-MM-DD hh:mm:ss'),
                "order_date": moment(order.datecreated).format('YYYY-MM-DD hh:mm:ss'),
                "total_amount": order.price,
                "seller_add": "H.No 2-56/2-33, Plot No 1303, Khanamet, Ayyappa Society, Madhapur, Hyderabad, TELANGANA, 500081, India.",
                "seller_cst": "1",
                "add": self.body.deliverystreet,
                "seller_name": "HAPPI EXPRESS",
                "seller_inv": "1",
                "seller_tin": "1",
                "pin": self.body.pin,
                "quantity": order.count + "",
                "payment_mode": "COD",
                "state": order.area.split(",")[1].trim(),
                "city": self.body.deliverycity,
                "client": "HAPPI EXPRESS"
              }
            ]
          };

        
        console.log("ORDER_PUSH_TO_DELIVERY_TRIGGERED", new Date().toISOString(), JOB_ID, obj);


        var options = {
            method: 'post',
            url: DELIVERY_CREATE_URL,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': `Token ${DELIVERY_TOKEN}`
            },
            data:  qs.stringify({
                'format': 'json',
                'data': JSON.stringify(obj)
            })
        }
        var body = await axios.request(options);
        console.log("ORDER_PUSH_TO_DELIVERY_RESULT", new Date().toISOString(), JOB_ID, body.data);
        // Sentry.captureMessage(`ORDER_PUSH_TO_DELIVERY_RESULT ${new Date().toISOString()} ${JOB_ID} ${JSON.stringify(body.data)}`);
        Sentry.captureMessage(`ORDER_PUSH_TO_DELIVERY_TRIGGERED`);

        await updateOrderDeliverStatus(orderId,body.data);
        if (body.data.success) {
            self.json({ status: true })
        } else {
            var remark = null
            if (body.data && body.data.packages[0] && body.data.packages[0].remarks != null) {
                remark = body.data.packages[0].remarks[0]
            } else {
                remark = body.data.rmk;
            }
            //Sentry.captureMessage(`ORDER_PUSH_TO_DELIVERY_RESULT ${new Date().toISOString()} ${JOB_ID} ${JSON.stringify(body.data)}`);
            self.json({ status: false, message: remark })
        }
    }
}

async function getOrder(id) {
    var nosql = new Agent();
    nosql.select('order', 'orders').make(function (builder) {
        builder.where('id', id);
        builder.first()
    });

    var orders = await nosql.promise('order');
    return orders;
}

async function updateOrderDeliverStatus(id, data) {
    var nosql = new Agent();
    if(data.success) {
        var waybill = data.packages[0].waybill;
        nosql.update('order', 'orders').make(function (builder) {
            builder.where('id', id);
            builder.set('delivery_logs', [JSON.stringify(data)]);
            builder.set('AWB', waybill);
        });
    } else {
        nosql.update('order', 'orders').make(function (builder) {
            builder.where('id', id);
            builder.set('delivery_logs', [JSON.stringify(data)]);
        });
    }
    
    var orders = await nosql.promise('order');
}

async function getOrder_delivery_details(AWB) {
    //console.log("AWB",AWB);
    var self = this;
    var options = {
        method: 'get',
        url: `${DELIVERY_GET_URL}?waybill=${AWB}&verbose=2&token=${DELIVERY_GET_TOKEN}`,
        headers: {
        },
    }
    var body = await axios.request(options);
    console.log("body", body);
    if(body.data.Error) {
        self.json({
            status: false,
            message: body.data.Error
        })
    } else {
        self.json({
            status: true,
            data: body.data
        })
    }
}




