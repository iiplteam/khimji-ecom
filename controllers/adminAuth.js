var MONGO_DB_CONNECTION = process.env.MONGO_DB_CONNECTION || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);
var jwt = require('jsonwebtoken');
var md5 = require('md5');

// jwt secret key 
var JWT_SECRET_KEY = process.env.JWT_SECRET_KEY || 'happi_jwt_secret';

exports.install = function () {
  // admin users
  ROUTE('/api/admin-user', createAdminUser, ['POST', 'cors', 10000]);
  ROUTE('/api/admin-user', getAdminUsers, ['cors', 10000]);
  ROUTE('/api/admin-user', deleteAdminUser, ['DELETE', 'cors', 10000]);
  ROUTE('/api/admin-user/{id}', getAdminUserById, ['cors', 10000]);

  // admin user login
  ROUTE('/admin-login', adminLogin, ['POST', 'cors']);
}

//admin user login
function adminLogin() {
  var self = this;
  var mnosql = new Agent();
  console.log("admin login function called ----------------------------------------------------------");
  mnosql.select('getAdmin', 'admin_users').make(function (builder) {
    builder.where('name', self.body.name);
    builder.first();
  });
  mnosql.exec(function (err, response) {
    console.log("rresponse", response.getAdmin)
    if (err) {
      console.log("MONGO ERR", err)
      self.json({ status: false })
      return;
    }
    if (response.getAdmin != null) {
      console.log(md5(self.body.password), response.getAdmin.password);
      var EncryptPassword = md5(self.body.password);


      if (EncryptPassword == response.getAdmin.password) {
        var token = jwt.sign({
          id: response.getAdmin.id,
          email: response.getAdmin.email,
          name: response.getAdmin.name
        }, JWT_SECRET_KEY, { expiresIn: '23h' });

        self.json({
          status: true,
          message: "Login Success",
          token: token,
          data: response.getAdmin
        })
      } else {
        self.json({
          status: false,
          message: "Invalid Password",
        })
      }
    } else {
      self.json({ status: false, message: "User not found" })
    }
  })
}

// add and update admin user
function createAdminUser() {

  var self = this;
  var body = self.body;
  //console.log("body-------", body);

  var token = self.headers['x-auth'];
  //console.log("token", token);
  if (token != null) {
    try {
      decoded = jwt.verify(token, JWT_SECRET_KEY);
      console.log("decoded ------", decoded)
      if (decoded != null) {
        var obj = {
          id: UID(),
          name: body.name,
          email: body.email,
          password: md5(body.password),
          gender: body.gender,
          access: body.access || [],
          menuList: body.menuList || [],
          createdBy: decoded.name,
          datecreated: new Date(),
          dateupdated: new Date()
        };
        var mnosql = new Agent();
        if (body.id) { // update
          //console.log("UPDATE ADMIN USER")
          obj.dateupdated = new Date()
          mnosql.update('updateAdmin', 'admin_users').make(function (builder) {
            builder.where('id', body.id);
            builder.set(obj)
          });
        } else { // add new user
          //console.log("ADD ADMIN USER")
          mnosql.insert('addAdmin', 'admin_users').make(function (builder) {
            builder.set(obj)
          });
        }

        //var addAdmin = await mnosql.promise('addAdmin');
        mnosql.exec(function (err, response) {
          console.log("MONGO err", err)
          if (err) {
            return self.json({
              status: false,
              message: err
            })
          }
          self.json({
            status: true,
            id: obj.id
          })
        })
      } else {
        self.json({
          status: false,
          message: "Invalid token"
        });
      }

    } catch (err) {
      console.log("err", err)
      self.json({
        status: false,
        message: "Sorry some thing went wrong"
      });
      return;
    }
  } else {
    self.throw401("Please provide token");
  }
}

// get admin users
async function getAdminUsers() {
  var self = this;
  var opt = self.query;
  var mnosql = new Agent();
  var token = self.headers['x-auth'];
  if (token != null) {
    try {
      decoded = jwt.verify(token, JWT_SECRET_KEY);
      //console.log("decoded", decoded)
      if (decoded != null) {
        mnosql.listing('getAdmin', 'admin_users').make(function (builder) {
          builder.sort('dateupdated', 'desc');
          //builder.where('createdBy', decoded.name);
          builder.page(opt.page || 1, opt.limit || 10);
        });

        var getAdmin = await mnosql.promise('getAdmin');

        //console.log(getAdmin);
        if (getAdmin != null) {
          self.json({ status: true, data: getAdmin })
        } else {
          self.json({ status: false })
        }
      } else {
        self.json({
          status: false,
          message: "Invalid token"
        });
      }
    } catch (err) {
      console.log("err", err)
      self.json({
        status: false,
        message: "Sorry some thing went wrong"
      });
      return;
    }
  } else {
    self.throw401("Please provide token");
  }
}

// delete admin user
async function deleteAdminUser() {
  var self = this;
  var token = self.headers['x-auth'];
  //console.log("params", self.query)
  if (token != null) {
    try {
      decoded = jwt.verify(token, JWT_SECRET_KEY);
      // console.log("decoded---", decoded)
      if (decoded != null) {
        var mnosql = new Agent();
        mnosql.remove('deleteAdmin', 'admin_users').make(function (builder) {
          builder.where('id', self.query.id);
        });
        var deleteAdmin = await mnosql.promise('deleteAdmin');
        if (deleteAdmin > 0) {
          self.json({ status: true, message: "Admin deleted" })
        } else {
          self.json({ status: false })
        }
      } else {
        self.json({
          status: false,
          message: "Invalid token"
        });
      }
    } catch (err) {
      // console.log("err", err)
      self.json({
        status: false,
        message: "Sorry some thing went wrong"
      });
      return;
    }
  } else {
    self.throw401("Please provide token");
  }
}

// get admin user by id
async function getAdminUserById(id) {
  var self = this;
  var token = self.headers['x-auth'];
  if (token != null) {
    try {
      decoded = jwt.verify(token, JWT_SECRET_KEY);
      //console.log("decoded", decoded)
      if (decoded != null) {
        var mnosql = new Agent();
        console.log("admin", self.controller);
        mnosql.select('getAdmin', 'admin_users').make(function (builder) {
          builder.where('id', id);
          builder.first();
        });

        var getAdmin = await mnosql.promise('getAdmin');

        if (getAdmin != null) {
          self.json({ status: true, data: getAdmin })
        } else {
          self.json({ status: false })
        }
      } else {
        self.json({
          status: false,
          message: "Invalid token"
        });
      }
    } catch (err) {
      console.log("err", err)
      self.json({
        status: false,
        message: "Sorry some thing went wrong"
      });
      return;
    }
  } else {
    self.throw401("Please provide token");
  }
}

