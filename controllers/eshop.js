var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
const assert = require('assert');

// captcha
var svgCaptcha = require('svg-captcha');

var MONGO_DB_CONNECTION = process.env.MONGO_DB_CONNECTION || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);

// imported product stocks module
var stockModule = MODULE('productsStock');

var request = require('request');
var jsonexport = require('jsonexport');
var fs = require('fs');

// create uuid module import
var generateUuidModule = MODULE('generate-uuid');

// send sms import modile
var smsModule = MODULE('sms');

// mongo db long url
const MONGO_URL = process.env.MONGO_URL || 'mongodb://sowmya:iNNrxOhVfEdvsUaI@cluster0-shard-00-00-cnw2n.mongodb.net:27017,cluster0-shard-00-01-cnw2n.mongodb.net:27017,cluster0-shard-00-02-cnw2n.mongodb.net:27017/khimji_dev?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority';

// Database Name
const DB_NAME = process.env.DB_NAME || 'khimji_dev';

// paytm env variables
const MID = process.env.PAYTM_MID || "Happim26189443024001";
const WEBSITE = process.env.PAYTM_WEBSITE || "WEBPROD";
const INDUSTRY_TYPE_ID = process.env.PAYTM_INDUSTRY_TYPE_ID || "Retail100";
const CHANNEL_ID = process.env.PAYTM_CHANNEL_ID || "WEB";
const CALLBACK_URL = process.env.PAYTM_CALLBACK_URL || "https://www.happimobiles.com/order/paytm-return";
const CALLBACK_LINK_URL = process.env.PAYTM_CALLBACK_LINK_URL || "https://happimobiles.com/order/paytm-return-link";
const CHECK_SUM_KEY = process.env.PAYTM_CHECK_SUM_KEY || "pBgjbchezWmjrORm";
const ORDER_SUBMIT_URL = process.env.PAYTM_ORDER_SUBMIT_URL || "https://securegw.paytm.in/order/process";

// pinelabs env variables
const PINELABS_URL = process.env.PINELABS_URL || "https://pinepg.in/PinePGRedirect/index";
const PINELABS_MERCHANT_ID = process.env.PINELABS_MERCHANT_ID || "1550";
const PINELABS_MERCHANT_ACCESS_CODE = process.env.PINELABS_MERCHANT_ACCESS_CODE || "bdd01d1d-52ea-460b-a6a7-625a7384dbc1";
const PINELABS_PAYMODE_ONLANDING_PAGEL = process.env.PINELABS_PAYMODE_ONLANDING_PAGEL || "0,1,3,4,5,10,11,12";
const PINELABS_MERCHANT_RETURN_URL = process.env.PINELABS_MERCHANT_RETURN_URL || "https://www.happimobiles.com/order/pinelabs-return";
const PINELABS_NAVIGATION_MODE = process.env.PINELABS_NAVIGATION_MODE || "2";
const PINELABS_TRANSACTION_TYPE = process.env.PINELABS_TRANSACTION_TYPE || "1";
const PINELABS_LPC_SEQ = process.env.PINELABS_LPC_SEQ || "1"; // strSecretKey
const PINELABS_STR_SECRET_KEY = process.env.PINELABS_STR_SECRET_KEY || "42F2F10AE4AE40CC858096722371EF49";
const PINELABS_STR_HASH_TYPE = process.env.PINELABS_STR_HASH_TYPE || "SHA256";
const PINELABS_UNIQUE_MERCHANT_TXNID = process.env.PINELABS_UNIQUE_MERCHANT_TXNID || 'ppc_UniqueMerchantTxnID';

exports.install = function () {
    ROUTE('#popular', view_popular);
    ROUTE('#top', view_top);
    ROUTE('#new', view_new);
    ROUTE('#category', view_category, [30000]); // new category added
    // ROUTE('#categorynew', view_category,[30000]);
    ROUTE('#detail', view_detail, [10000]);
    ROUTE('#detailnew', view_detail, [10000]);
    ROUTE('#checkout', checkout);

    ROUTE('#order', view_order, [10000]);
    ROUTE('#account', 'account', ['authorize']);
    ROUTE('#settings', 'settings', ['authorize']);
    ROUTE('#account', view_signin, ['unauthorize']);
    ROUTE('#logoff', redirect_logoff, ['authorize']);

    // Payment process
    ROUTE('#order/paypal/', paypal_process, ['*Order', 10000]);
    // ROUTE('/order/paytm/', paytm_process, ['post']);
    // ROUTE('/order/paytm-return', paytm_cb, ['post']);
    // ROUTE('/order/paytm-return/', paytm_cb, ['post', 10000]);
    ROUTE('/order/paytm-return-link', paytm_cb_link, ['post']);

    ROUTE('/order/razorpay-payment', update_payment, ['post']);
    ROUTE('/order/cod/', cod_process, ['post']);
    ROUTE('/order/export/', orders_export, ['cors']);
    ROUTE('/csv-download/{id}', download_csv);
    //ROUTE('/order/verify/', verify_process, ['post']);

    // ROUTE('/order/pinelabs/', pinelabs_process, ['post']);
    // ROUTE('/order/pinelabs-return/', pinelabs_cb, ['post', 10000]);
    ROUTE('/order-delivery/{id}', view_delivery_page);
    ROUTE('/order-payment/{id}', view_payment_page);
    //ROUTE('/product-view/{id}', view_product_view);

    //Insurance 
    ROUTE('/warranty/', view_insurance);
    ROUTE('/warranty/{id}', view_insurance_by_id);
    ROUTE('/order/insurance/paytm/', insurance_process, ['post']);
    ROUTE('/order/insurance/paytm-return', insurance_paytm_cb, ['post']);
    ROUTE('/order-confirmation', orderConfirmation, ['post', 'cors']);

    //recaptcha
    ROUTE('/api/get-recaptcha', generateRecaptcha, ['cors']);
    ROUTE('/api/verify-recaptcha', verifyRecaptcha, ['post', 'cors']);

    ROUTE('/api/order/verification/{id}', paymentVerify, ['get', 10000]);


};

async function paymentVerify() {
    console.log("PAYMENT VERIFY ---------------");
    var self = this;
    var mnosql = new Agent();
    var orderid = self.params.id;
    console.log("orderid", orderid);
    mnosql.select('orders', 'orders').make(function (builder) {
        builder.where('id', orderid);
        builder.first();
    });

    var order = await mnosql.promise('orders');
    self.layout('layout-new');
    self.view('~order-payment-confirmation', order);
}

function generateRecaptcha() {
    var self = this;
    var captcha = svgCaptcha.create();
    //console.log(captcha);
    self.json({
        status: true,
        data: captcha
    })
}

function verifyRecaptcha() {
    var self = this;
    // recaptcha data verify
    if (self.body.captcha != self.body.userCaptcha) {
        var captcha = svgCaptcha.create();
        return self.json({
            status: false,
            message: "Invalid Captcha",
            data: captcha
        })
    } else {
        self.json({
            status: true,
            message: "Captcha Verified Successfully",
        })
    }
}

function orderConfirmation() {
    var self = this;

}

ON("ORDERS_EXPORT", function (opt) {
    var mnosql = new Agent();

    mnosql.select('ordersmayreport', 'orders').make(function (builder) {
        builder.query('datecreated', {
            $gte: new Date(new Date(opt.fromDate).setUTCHours(00, 00, 00, 000)),
            $lte: new Date(new Date(opt.toDate).setUTCHours(23, 59, 59, 999))
        });
        console.log("builder", builder.builder);
    })
    mnosql.exec(function (err, response) {
        if (err) {
            console.log(err);
        } else {
            console.log("resp", response.ordersmayreport.length);
            var records = [];
            for (var i = 0; i < response.ordersmayreport.length; i++) {
                var each = response.ordersmayreport[i];
                var products = "";
                for (var j = 0; j < each.items.length; j++) {
                    var x = each.items[j].name + ' X ' + each.items[j].count + ' - ';
                    products = products + x;
                }
                var dateformat = each.datecreated;
                dateformat = dateformat.getDate() + "-" + (dateformat.getMonth() + 1) + "-" + dateformat.getFullYear();
                var brands = "";
                var temp = products.toLowerCase();
                if (temp.indexOf("iphone") != -1) {
                    brands = brands + ' ' + "Apple"
                }
                if (temp.indexOf("realme") != -1) {
                    brands = brands + ' ' + "Realme"
                }
                if (temp.indexOf("oppo") != -1) {
                    brands = brands + ' ' + "Oppo"
                }
                if (temp.indexOf("nokia") != -1) {
                    brands = brands + ' ' + "Nokia";
                }
                if (temp.indexOf("oneplus") != -1) {
                    brands = brands + ' ' + "Oneplus"
                }
                if (temp.indexOf("vivo") != -1) {
                    brands = brands + ' ' + "Vivo"
                }
                if (temp.indexOf("honor") != -1) {
                    brands = brands + ' ' + "Honor"
                }
                if (temp.indexOf("mi") != -1) {
                    brands = brands + ' ' + "Xiaomi"
                }
                if (temp.indexOf("samsung") != -1) {
                    brands = brands + ' ' + "Samsung"
                }
                if (temp.indexOf("motorola") != -1) {
                    brands = brands + ' ' + "Motorola"
                }
                if (temp.indexOf("lg") != -1) {
                    brands = brands + ' ' + "LG"
                }

                // console.log("brnds", brands)

                var obj = {
                    Orderid: each.id,
                    OrderNo: each.number,
                    Name: each.name,
                    Email: each.email,
                    price: each.price,
                    status: each.status,
                    internalNote: each.internal_type,
                    CityName: each.deliverycity,
                    BillingAddress: each.billingstreet.split(',').join(' ') + ' ' + each.billingcity + ' ' + each.billingzip,
                    DeliveryAddress: each.deliverystreet.split(',').join(' ') + ' ' + each.deliverycity + ' ' + each.deliveryzip,
                    Products: products,
                    Paymentstatus: "none",
                    Ordertype: "none",
                    Date: dateformat,
                    DeliveryPhoneNum: each.deliveryphone,
                    PhoneNum: each.phone,
                    Brand: brands,
                };
                if (each.ispaid) {
                    obj.Paymentstatus = "paid #" + each.taxid;
                }
                if (each.iscod) {
                    obj.Paymentstatus = "cod"
                }
                if (each.ispickup) {
                    obj.Ordertype = "pickup"
                }
                if (each.istwohrs) {
                    obj.Paymentstatus = "2hrs"
                }
                records.push(obj);
                //console.log("records", JSON.stringify(obj));
            }
            //return self.json(response.ordersmayreport);
            jsonexport(records, function (err, csv) {
                if (err) return console.log(err);
                //console.log(csv);
                var timestamp = new Date().getTime();
                fs.writeFile('./public/csv/t-' + timestamp + '.csv', csv, 'utf8', function (err) {
                    if (err) {
                        console.log('Some error occured - file either not saved or corrupted file saved.');
                    }
                    else {
                        console.log('It\'s saved!');
                        // self.file('csv/t-' + timestamp + '.csv', 'orders_t-' + timestamp + '.csv');
                        // self.json({ url: 'http://localhost:9898/csv/t-' + timestamp + '.csv' })
                        var model = opt;
                        model.url = "https://happimobiles.com/csv-download/" + timestamp;

                        MAIL("saycoolsainadh@gmail.com", `Orders Export From ${opt.fromDate} to ${opt.toDate}`, '=?/mails/order-export', model);
                        MAIL("orders@happimobiles.com", `Orders Export From ${opt.fromDate} to ${opt.toDate}`, '=?/mails/order-export', model);
                        MAIL("bhagat@redmattertech.com", `Orders Export From ${opt.fromDate} to ${opt.toDate}`, '=?/mails/order-export', model);
                        MAIL("sharansreeharsh@gmail.com", `Orders Export From ${opt.fromDate} to ${opt.toDate}`, '=?/mails/order-export', model);
                    }
                });
            });
        }
    });
});

function checkout() {
    this.redirect('/cart');
}

function download_csv(id) {
    this.file('csv/t-' + id + '.csv', 'orders_t-' + id + '.csv');
}

async function view_delivery_page(id) {
    var self = this;
    var nosql = new Agent();

    nosql.select('order', 'order-link').make(function (builder) {
        builder.where('id', id);
        builder.first();
    });

    console.log("ORDER_ID", id);
    var order = await nosql.promise('order');

    self.layout('nolayout');
    //
    self.view('eshop/order-delivery', order);


}

async function view_payment_page(id) {
    var self = this;
    var nosql = new Agent();

    nosql.select('order', 'order-link').make(function (builder) {
        builder.where('id', id);
        builder.first();
    });

    var order = await nosql.promise('order');
    self.layout('nolayout');
    self.view('eshop/order-payment', order);
}

function orders_export() {
    var self = this;
    var opt = self.query;
    // orders get mongo
    // console.log("opt",opt);
    EMIT("ORDERS_EXPORT", opt);
    self.plain("Shortly will recive an email to orders@happimobiles.com, sharansreeharsh@gmail.com");
}

function orders_cod() {
    var self = this;

    if (self.body.order_id !== null) {
        var mnosql = new Agent();

        mnosql.select('ordersGet', 'orders').make(function (builder) {
            builder.where('id', self.body.order_id);
            builder.first();
        });

        mnosql.exec(function (err, response) {
            if (err) {
                self.redirect('/checkout/' + self.body.order_id);
                return;
            }

        });


    } else {

    }

}

function cod_process() {
    var self = this;

    if (self.body.order_id !== null) {
        // console.log("order_id", self.body.order_id);
        var mnosql = new Agent();
        // start update the COD tag (mongo)
        mnosql.update('orders', 'orders').make(function (builder) {
            builder.set({ iscod: true, datecod: F.datetime, tag: "cod", internal_type: "Order Placed" });
            builder.where('id', self.body.order_id);
        });
        // Start send SMS to Order (mongo)
        mnosql.select('ordersGet', 'orders').make(function (builder) {
            builder.where('id', self.body.order_id);
            builder.first();
        });
        mnosql.exec(function (err, response) {
            if (err) {
                self.redirect('/checkout/' + self.body.order_id);
            }
            //self.redirect('/checkout/' + self.body.order_id + '/?cod=1');
            self.redirect('/api/order/verification/' + self.body.order_id);
            //console.log("FETCH ORDER",err, response.ordersGet);

            var order = response.ordersGet;
            stockModule.OrderConfirm(order.id);
            if (order == null) {
                return;
            }
            var message = `Thankyou for placing order in happimobiles.com. Your order is underprocess for information https://happimobiles.com/checkout/${order.id}`;
            var subject = '@(Order #) ' + order.id;
            var model = response.ordersGet;

            MAIL(model.email, subject, '=?/mails/order', model, model.language);
            if (order.istwohrs) {
                subject = subject + " - 2HRS";
            }
            if (order.ispickup) {
                subject = subject + " - PICK_UP";
            }
            subject = subject + " - COD ";
            subject = subject + order.id;
            MAIL("happionlineorders@gmail.com", subject, '=?/mails/order-admin', model, model.language);
            smsModule.sendSMS(order.phone, message);
        });
    }
    else {
        self.redirect('/checkout/' + self.body.order_id);
    }
}

function prepare_links(d) {
    // for (var i = 0; i < d.length; i++) {
    //     d[i].linker = '/detail/' + d[i].linker;
    // }
    return d;
}

function searchProducts(db, keywords, callback) {
    // Get the documents collection
    const collection = db.collection('product');
    // Insert some documents
    collection.aggregate(
        // Pipeline
        [
            // Stage 1 
            {
                $match: {
                    '$and': [{
                        searchkeywords: { $in: keywords },
                        ispublished: true,
                        isactive: true
                    }]
                }
            },
            // Stage 2
            {
                $project: {
                    "searchkeywords": 1,
                    "name": 1,
                    "payPrice": 1,
                    "id": 1,
                    'mrp': 1, 'linker': 1, 'linker_category': 1, 'linker_manufacturer': 1, 'category': 1, 'manufacturer': 1,
                    'pricemin': 1, 'priceold': 1, 'isnew': 1, 'istop': 1, 'pictures': 1, 'availability': 1, 'datecreated': 1, 'ispublished': 1,
                    'signals': 1, 'size': 1, 'stock': 1, 'color': 1, 'purchase_type': 1, 'ftrFeatures': 1, 'ftrTransfer': 1, 'product_type': 1, 'prices': 1, 'istvs': 1, 'booking_type': 1, 'weight': 1,
                    "orders": {
                        "$size": {
                            "$setIntersection": [keywords, "$searchkeywords"]
                        }
                    }
                }
            },
            // Stage 3
            {
                $sort: {
                    "orders": -1,
                    "stock": -1,
                    "weight": -1,

                }
            },
        ],
        // Options
        {
            allowDiskUse: true
        }
    ).toArray(function (err, res) {
        if (err) throw err;
        var obj = {
            page: 1,
            items: prepare_links(res),
            limit: res.length,
            count: res.length,
            pages: 2,
        };
        callback(obj);
        // console.log(JSON.stringify(res));
        // db.close();
    });;

}

function view_category() {
    //console.log("view cat called --------------------------------");
    var self = this;
    var url = self.sitemap_url('category');
    var linker = self.url.substring(url.length, self.url.length - 1);
    var category = null;
    var sub_category = null;
    var sub_category_one = null;
    var options = {};
    var metaTags = [{
        linker: "price-0-10000",
        keywords: "Mobile Phones under 10000",
        content: "Get mobile phones under Rs.10000. Buy your new smartphone today, shop with us, and get a great deal on it too Serving only the best for you -Happi Mobiles",
        h1: "Some Content about under 10000 mobile phones",
        title: "mobile phones under 10000 at the most affordable prices available- Happi Mobiles",
        description: "Ever wish to buy a phone but have a strict budget? Do not wait more because we have the perfect solution for you. We have all your favourite latest products that will fit under your budget. And all this comes with a few perks. We have the best offers and deals that are waiting for you. So why wait anymore. Get the <a href='/' style='color:#fb9013;' >best latest phone</a> under Rs. 10000 because we make sure you get the best with Happi Mobiles."
    }, {
        linker: "price-10001-14999",
        keywords: "Mobile Phones under 15000",
        content: "Get your new smartphone with amazing deals and offers under just Rs.15000. Make sure you shop with us to avail offers and deals for yourself or your loved ones.",
        h1: "Some Content about under 15000 mobile phones",
        title: "Mobile phones under 15000 -Happi Mobiles",
        description: "Want to gift your parents or grandparents a phone they could easily access? We have the perfect options for you under your budget. Get the latest smartphones with incredible offers under Rs. 15000 for you or your loved ones. Make sure they get the best out of living as well, surprise them and give them the freedom of digital independence with the best and <a href='/' style='color:#fb9013;' >latest smartphones</a> with Happi Mobiles today!"
    }, {
        linker: "price-15000-19999",
        keywords: "Mobile Phones under 20000",
        content: "Get your favourite smartphone with us and avail some amazing deals and offers on your new purchase. Get your mobile phone under Rs. 20000 with Happi Mobiles.",
        h1: "Some Content about under 20000 mobile phones",
        title: "Smart phones under 20000- Happi Mobiles",
        description: "Have you been trying to save up for that latest smartphone that you've had your eyes on for so long? Why wait for something for so long when you can have the best right now! Get the latest and the <a href='/' style='color:#fb9013;' >best mobile phones</a> in the market with Happi Mobiles. We will make sure that we guide you with the best options including some amazing offers and deals. Visit Happi Mobiles and make your dreams come true today!"
    }, {
        linker: "price-20000-24999",
        keywords: "Mobile Phones under 25000",
        content: "Make sure you get your hands on your new mobile phone with Happi Mobiles and get it just under Rs. 25000. Shop with Happi Mobiles; make sure you don't miss out.",
        h1: "Some Content about under 25000 mobile phones",
        title: "Smart phones just under 25000- Happi Mobiles",
        description: "Best latest smartphone under just rs. 25000? Yes, it's true and even possible with Happi Mobiles. We at Happi Mobiles make sure that every customer gets the best of the deals and products that fit right under their budget. It is our motive to ensure one hundred per cent customer satisfaction. So visit Happi Mobiles today without a worry. Just tell us your budget and get the <a href='/' style='color:#fb9013;' >best phones</a> that will satisfy all your needs."
    }, {
        linker: "price-25000-29999",
        keywords: "Mobile Phones under 30000",
        content: "Get a new phone for yourself or your loved ones today! Get your new mobile phone under Rs. 30000 and make sure you enjoy our offers and deals, with Happi Mobiles!",
        h1: "Some Content about under 30000 mobile phones",
        title: "Get new Mobile Phones under 30000- Happi Mobiles",
        description: "Best latest smarthphones just under Rs. 30000? No, it's not a dream. Yes, it's possible! We at Happi Mobiles are more than happy to assist you with your favourite and <a href='/' style='color:#fb9013;' >latest mobile phones </a> with amazing offers and deals. Why wait anymore? Grab your latest device today because we make sure you leave happy and satisfied after shopping with us. For all your basic mobile phone needs, remember Happi Mobiles!"
    }, {
        linker: "price-30000-200000",
        keywords: "Mobile Phones under 200000",
        content: "Make the most out of our deals and offers at Happi Mobiles. Make your new purchase with Happi Mobiles and get your new mobile phone under Rs. 200000.",
        h1: "Some Content about under 200000 mobile phones",
        title: "New Smart phones under 200000- Happi Mobiles",
        description: "For the best smartphones available in the market, Happi Mobiles is your answer. Get in on the <a href='/' style='color:#fb9013;' >latest smartphones</a> and enjoy various deals and offers that are waiting for you. Now you can get the latest smartphone under just Rs. 20000. With the kind of deals and products we offer, you won't be able to say no. Visit us today and see for yourself. Because at Happi Mobiles, we want each and every customer to come in happi and leave happi!"
    }];
    var metaTag = {};
    //console.log("LINKER____", linker);
    var linkerContext = linker.split('/');
    if (linkerContext.length > 2) {

        //console.log("NEW LINKER",linker);
        if (linkerContext[2].indexOf('price') != -1) {
            linker = `${linkerContext[0]}/${linkerContext[1]}`;
            console.log("linkerContext[2]", linkerContext[2]);
            metaTags.forEach(element => {
                var metaLinker = element.linker;
                if (metaLinker.indexOf(linkerContext[2]) != -1) {
                    // console.log("metaLinker",metaLinker);
                    //console.log(metaLinker.indexOf(linkerContext[2]),"heloooooooooooo");
                    var price = linkerContext[2].split('-');
                    price[1] && (options.minprice = price[1]);
                    price[2] && (options.maxprice = price[2]);
                    metaTag = element;
                    console.log("metatag", element);
                }
            });
            var price = linkerContext[2].split('-');
            price[1] && (options.minprice = price[1]);
            price[2] && (options.maxprice = price[2]);
        }
    }
    console.log("LINKER____", linker);
    //console.log("ctaegories========== Global", F.global.categories);
    if (linker !== '/' && linker != undefined) {
        category = F.global.categories.findItem('linker', linker);
        sub_category = F.global.sub_categories.findItem('linker', linker);
        sub_category_one = F.global.sub_categories_one.findItem('linker', linker);
        // console.log("category", category);
        // console.log("sub_category.name", sub_category);
        // console.log("sub_category_one.name", sub_category_one);
        if (category == null && sub_category == null && sub_category_one == null) {
            self.throw404();
            return;
        }
    }

    // Binds a sitemap
    self.sitemap();

    //return;
    if (category) {
        options.category = category.name;
        console.log(category.name);

        self.title(category.name);
        self.repository.category = category;

        var path = self.sitemap_url('category');
        var tmp = category;
        while (tmp) {
            self.sitemap_add('category', tmp.name, path + tmp.linker + '/');
            tmp = tmp.parent;
        }

    } else
        self.title(self.sitemap_name('category'));

    if (sub_category) {
        options.sub_category = sub_category.name;
        console.log(sub_category.name);
    }

    if (sub_category_one) {
        options.sub_category_one = sub_category_one.name;
        console.log(sub_category_one.name);
    }


    options.published = true;
    options.limit = 15;
    if (self.query.q != null) {

        const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });

        var keywords = self.query.q.toLowerCase().split(" ");


        client.connect(function (err, C) {
            assert.equal(null, err);
            //console.log("Connected successfully to server");
            const db = C.db(DB_NAME);
            searchProducts(db, keywords, function (response) {
                C.close();
                self.repository.linker_category = linker;
                if (self.query.json == "1") {
                    self.json(response);
                    return;
                }
                self.layout('layout-new');
                self.view('category', response);

            });
        });



    } else {
        //var fullUrl = self.protocol + '://' + self.get('host') + self.originalUrl;
        // console.log("req.originalUrl",controller.url);
        self.query.page && (options.page = self.query.page);
        self.query.manufacturer && (options.manufacturer = self.query.manufacturer);
        self.query.size && (options.size = self.query.size);
        self.query.color && (options.color = self.query.color);
        // self.query.q && (options.q = self.query.q);
        self.query.sort && (options.sort = self.query.sort);
        self.query.ftrFeatures && (options.ftrFeatures = self.query.ftrFeatures);
        self.query.ftrTranser && (options.ftrTranser = self.query.ftrTranser);
        self.query.ftrBrand && (options.ftrBrand = self.query.ftrBrand);
        self.query.ftrCam && (options.ftrCam = self.query.ftrCam);
        self.query.ftrScreen && (options.ftrScreen = self.query.ftrScreen);
        self.query.ftrBattery && (options.ftrBattery = self.query.ftrBattery);
        self.query.ftrProcessor && (options.ftrProcessor = self.query.ftrProcessor);
        self.query.ftrMemory && (options.ftrMemory = self.query.ftrMemory);
        self.query.ftrFeatures1 && (options.ftrFeatures1 = self.query.ftrFeatures1);
        self.query.ftrTransfer1 && (options.ftrTransfer1 = self.query.ftrTransfer1);
        self.query.ftrBrand1 && (options.ftrBrand1 = self.query.ftrBrand1);
        self.query.ftrCam1 && (options.ftrCam1 = self.query.ftrCam1);
        self.query.ftrScreen1 && (options.ftrScreen1 = self.query.ftrScreen1);
        self.query.ftrBattery1 && (options.ftrBattery1 = self.query.ftrBattery1);
        self.query.ftrProcessor1 && (options.ftrProcessor1 = self.query.ftrProcessor1);
        self.query.ftrMemory1 && (options.ftrMemory1 = self.query.ftrMemory1);
        self.query.pBrand && (options.pBrand = self.query.pBrand);
        self.query.pBattery && (options.pBattery = self.query.pBattery);
        self.query.sBrand && (options.sBrand = self.query.sBrand);
        self.query.sDrivecapacity && (options.sDrivecapacity = self.query.sDrivecapacity);
        self.query.ccBrand && (options.ccBrand = self.query.ccBrand);
        self.query.cabBrand && (options.cabBrand = self.query.cabBrand);
        self.query.cBrand && (options.cBrand = self.query.cBrand);
        self.query.hBrand && (options.hBrand = self.query.hBrand);
        self.query.hmicrophone && (options.hmicrophone = self.query.hmicrophone);
        self.query.Hconnectivityfeatures && (options.Hconnectivityfeatures = self.query.Hconnectivityfeatures);
        self.query.hfeature && (options.hfeature = self.query.hfeature);
        self.query.hinterface && (options.hinterface = self.query.hinterface);
        self.query.BhDesignedfor && (options.BhDesignedfor = self.query.BhDesignedfor);
        self.query.BhAdditionalfeatures && (options.BhAdditionalfeatures = self.query.BhAdditionalfeatures);
        self.query.BhOtherfeatures && (options.BhOtherfeatures = self.query.BhOtherfeatures);
        self.query.bsBrand && (options.bsBrand = self.query.bsBrand);
        self.query.bsmicro && (options.bsmicro = self.query.bsmicro);
        self.query.bsconnect && (options.bsconnect = self.query.bsconnect);
        self.query.bsfeature && (options.bsfeature = self.query.bsfeature);
        self.query.bshead && (options.bshead = self.query.bshead);
        self.query.tvBrand && (options.tvBrand = self.query.tvBrand);
        self.query.tvsize && (options.tvsize = self.query.tvsize);
        self.query.tvdisplay && (options.tvdisplay = self.query.tvdisplay);
        self.query.minprice && (options.minprice = self.query.minprice);
        self.query.maxprice && (options.maxprice = self.query.maxprice);
        self.query.proBrand && (options.proBrand = self.query.proBrand);
        self.query.smartBrand && (options.smartBrand = self.query.smartBrand);
        self.query.shop_by_style && (options.shop_by_style = self.query.shop_by_style);
        self.query.wearing_type && (options.wearing_type = self.query.wearing_type);
        self.query.shop_by_metal && (options.shop_by_metal = self.query.shop_by_metal);
        self.query.shop_by_occasion && (options.shop_by_occasion = self.query.shop_by_occasion);
        self.query.shop_by_brand && (options.shop_by_brand = self.query.shop_by_brand);
        self.query.shop_by_collection && (options.shop_by_collection = self.query.shop_by_collection);
        if ( self.query.shop_by_collection != "all") {
            self.query.shop_by_collection && (options.shop_by_collection = self.query.shop_by_collection);
        }
        if ( self.query.shop_by_style != "all") {
            self.query.shop_by_style && (options.shop_by_style = self.query.shop_by_style);
        }
        if ( self.query.wearing_type != "all") {
            self.query.wearing_type && (options.wearing_type = self.query.wearing_type);
        }
        if ( self.query.shop_by_metal != "all") {
            self.query.shop_by_metal && (options.shop_by_metal = self.query.shop_by_metal);
        }
        if ( self.query.shop_by_occasion != "all") {
            self.query.shop_by_occasion && (options.shop_by_occasion = self.query.shop_by_occasion);
        }
        if ( self.query.shop_by_brand != "all") {
            self.query.shop_by_brand && (options.shop_by_brand = self.query.shop_by_brand);
        }
        $QUERY('Product', options, function (err, response) {
            self.repository.linker_category = linker;
            response.headerJson = JSON.parse(fs.readFileSync(__dirname + '/../public/header.json'));

            if (metaTag.description && metaTag.keywords) {
                response.metaTag = metaTag;
            }

            if (self.query.json == "1") {
                self.json(response);
                return
            }
            self.layout('layout-new');
            self.view('category', response);
        });
    }


}

function view_popular() {
    var self = this;
    var options = {};
    options.published = true;
    self.query.manufacturer && (options.manufacturer = self.query.manufacturer);
    self.query.size && (options.size = self.query.size);
    self.sitemap();
    $WORKFLOW('Product', 'popular', options, self.callback('special'));
}

function view_new() {
    var self = this;
    var options = {};
    options.isnew = true;
    options.published = true;
    self.query.manufacturer && (options.manufacturer = self.query.manufacturer);
    self.query.size && (options.size = self.query.size);
    self.sitemap();
    $QUERY('Product', options, self.callback('special'));
}

function view_top() {
    var self = this;
    var options = {};
    options.istop = true;
    options.published = true;
    self.query.manufacturer && (options.manufacturer = self.query.manufacturer);
    self.query.size && (options.size = self.query.size);
    self.sitemap();
    $QUERY('Product', options, self.callback('special'));
}

function view_detail(linker) {
    var self = this;
    var options = {};
    options.linker = linker;

    $GET('Product', options, function (err, response) {

        if (err)
            return self.invalid().push(err);

        // Binds a sitemap
        self.sitemap();

        // var path = self.sitemap_url('category');
        // var tmp = response.category;

        // while (tmp) {
        //     self.sitemap_add('category', tmp.name, path + tmp.linker + '/');
        //     tmp = tmp.parent;
        // }

        // // Category menu
        // self.repository.linker_category = response.category.linker;

        self.title(response.name);
        self.description(response.name);
        self.keywords(response.name);
        self.sitemap_change('detail', 'url', linker);
        //console.log('RESPONSEEEEEEEEE', response.prices);
        response.page_type = "product";
        response.headerJson = JSON.parse(fs.readFileSync(__dirname + '/../public/header.json'));

        if (self.query.json == "1") {
            self.json(response);
        } else {
            //self.view('~cms/' + (response.template || 'product'), response);
            self.layout('layout-new');
            self.view('~cms/product', response);
        }
    });
}

function view_order(id) {
    var self = this;
    var options = {};

    self.id = options.id = id;

    console.log("ORDER_PREFETCH", options);

    $GET('Order', options, function (err, response) {
        console.log("ORDER_FETCH", err, response);
        if (err || response == null) {
            self.invalid().push(err);
            return;

        }

        // if (!response.ispaid) {
        //     switch (self.query.payment) {
        //         case 'paypal':
        //             paypal_redirect(response, self);
        //             return;
        //     }
        // }

        if (response.ispaid || response.iscod) {

            if (response.counter == undefined) {
                response.counter = 0;

                var mnosql = new Agent();

                mnosql.update('order_update', 'orders').make(function (builder) {
                    builder.set({
                        counter: 0
                    });
                    builder.where('id', self.id);
                });

                mnosql.exec(function (err, resp) {

                })
            } else {

                var mnosql = new Agent();

                mnosql.update('order_update', 'orders').make(function (builder) {
                    builder.inc('counter', 1);
                    builder.where('id', self.id);
                });

                mnosql.exec(function (err, resp) {

                })
            }



        }

        if (self.query.json) {
            self.json(response);
            return;
        }
        if (self.query.userAgent == "Happi-Mobile-App" || self.headers['user-agent'] == "Happi-Mobile-App") {
            self.layout('nolayout');
            self.view('~eshop/order-new', response);
            return;
        }

        if (response && response.version == 'V2') {
            self.layout('layout-new');
            self.view('~eshop/order-new-desktop', response);
            return;
        }
        self.sitemap('order');
        self.view('order', response);
    });
}

function redirect_logoff() {
    var self = this;
    MODEL('users').logoff(self, self.user);
    self.redirect(self.sitemap_url('account'));
}

function view_signin() {
    var self = this;
    var hash = self.query.hash;

    // Auto-login
    if (hash && hash.length) {
        var user = F.decrypt(hash);
        if (user && user.expire > F.datetime.getTime()) {
            MODEL('users').login(self, user.id);
            self.redirect(self.sitemap_url('settings') + '?password=1');
            return;
        }
    }

    self.sitemap();
    self.view('signin');
}

function paypal_redirect(order, controller) {
    var redirect = F.global.config.url + controller.sitemap_url('order', controller.id) + 'paypal/';
    var paypal = require('paypal-express-checkout').create(F.global.config.paypaluser, F.global.config.paypalpassword, F.global.config.paypalsignature, redirect, redirect, F.global.config.paypaldebug);
    paypal.pay(order.id, order.price, F.config.name, F.global.config.currency, function (err, url) {
        if (err) {
            LOGGER('paypal', order.id, err);
            controller.throw500(err);
        }
        else
            controller.redirect(url);
    });
}

function paypal_process(id) {

    var self = this;
    var redirect = F.global.config.url + self.url;
    var paypal = require('paypal-express-checkout').create(F.global.config.paypaluser,
        F.global.config.paypalpassword, F.global.config.paypalsignature,
        redirect, redirect, F.global.config.paypaldebug);

    self.id = id;

    paypal.detail(self, function (err, data) {

        LOGGER('paypal', self.id, JSON.stringify(data));

        var success = false;

        switch ((data.PAYMENTSTATUS || '').toLowerCase()) {
            case 'pending':
            case 'completed':
            case 'processed':
                success = true;
                break;
        }

        var url = self.sitemap_url('order', self.id);

        if (success)
            self.$workflow('paid', () => self.redirect(url + '?paid=1'));
        else
            self.redirect(url + '?paid=0');
    });
}

function update_payment() {
    var self = this;
    NOSQL('orders').modify({ ispaid: true, datepaid: F.datetime, taxid: self.body.razorpay_payment_id })
        .where('id', self.body.order_id)
        .callback((err, count) => {
            console.log('err', err);
            console.log('count', count);
            self.json({ success: true });
        });

}


function paytm_cb_link() {
    var self = this;
    console.log("PAYTM_CALLBACK_TRIGGERED", JOB_ID, new Date().toISOString(), self.body);
    const checksum_lib = require('../modules/paytm-checksum');


    var paytmChecksum = "";


    var paytmParams = {};
    for (var key in self.body) {
        if (key == "CHECKSUMHASH") {
            paytmChecksum = self.body[key];
        }
        else {
            paytmParams[key] = self.body[key];
        }
    }

    var isValidChecksum = checksum_lib.verifychecksum(paytmParams, CHECK_SUM_KEY, paytmChecksum);
    if (isValidChecksum) {
        console.log("Checksum Matched");
        if (paytmParams["STATUS"] == "TXN_SUCCESS") {

            var order_id = paytmParams["ORDERID"];

            var mnosql = new Agent();
            //NOSQL('orders').one().where('id', $.options.id || $.id).callback($.callback, 'error-orders-404');
            //console.log("id", $.options.id, $.id)

            mnosql.update('order_update', 'order-link').make(function (builder) {
                builder.set({
                    ispaid: true,
                    datepaid: F.datetime,
                    tan_no: "PAYTM-" + paytmParams["TXNID"],
                });
                builder.where('id', order_id);
            });

            mnosql.select('orders', 'order-link').make(function (builder) {
                builder.where('id', order_id);
                builder.first();
            });

            mnosql.exec(function (err, response) {
                if (err) {
                    return self.redirect('/order-payment/' + paytmParams["ORDERID"] + '/#PAYMENT_FAILED');
                }

                if (response.orders != null) {
                    self.redirect('/order-payment/' + order_id + '/?paid=1');
                }

            });


        } else {
            return self.redirect('/order-payment/' + paytmParams["ORDERID"] + '/#PAYMENT_FAILED');
        }
    } else {
        return self.redirect('/order-payment/' + paytmParams["ORDERID"] + '/#PAYMENT_FAILED');
    }
}





function view_insurance() {
    var self = this;
    self.view('eshop/insurance', { status: "Form" });
}


function insurance_process() {
    var self = this;

    const checksum_lib = require('../modules/paytm-checksum');



    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("happi");

        var myobj = self.body;
        myobj.status = "Pending";
        myobj.createdOn = new Date();

        dbo.collection("insurance-orders").insertOne(myobj, function (err, res) {
            if (err) throw err;
            console.log("1 document inserted");
            db.close();
            console.log(res["ops"][0]["_id"]);

            var paytmParams = {

                /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
                "MID": "Happim26189443024001",

                /* Find your WEBSITE in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
                "WEBSITE": "WEBPROD",

                /* Find your INDUSTRY_TYPE_ID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
                "INDUSTRY_TYPE_ID": "Retail100",

                /* WEB for website and WAP for Mobile-websites or App */
                "CHANNEL_ID": "WEB",

                /* Enter your unique order id */
                "ORDER_ID": res["ops"][0]["_id"].toString(),

                /* unique id that belongs to your customer */
                "CUST_ID": self.body.mobile,

                /* customer's mobile number */
                "MOBILE_NO": self.body.mobile,

                /* customer's email */
                "EMAIL": self.body.email,

                /**
                 * Amount in INR that is payble by customer
                 * this should be numeric with optionally having two decimal points
                 */
                "TXN_AMOUNT": "499",

                /* on completion of transaction, we will send you the response on this URL */
                // "CALLBACK_URL" : "http://localhost:8000/order/paytm-return", //local
                //"CALLBACK_URL" : "http://13.232.26.24:8888/order/paytm-return", //dev
                //"CALLBACK_URL": "https://happimobiles.com/order/insurance/paytm-return", //prod
                "CALLBACK_URL": "https://dev-happi.iipl.work/order/insurance/paytm-return"
            };

            var message = `Thank you for initiating your extended warranty order. Your order is in process. Please click on link below for status update https://happimobiles.com/warranty/${res["ops"][0]["_id"].toString()}`;
            var options = {
                'method': 'POST',
                'url': `http://api.smscountry.com/SMSCwebservice_bulk.aspx?User=happi9&passwd=Happi@12345&mobilenumber=91${self.body.mobile}&message=${message}&sid=HappiM&mtype=N&DR=Y`,
                'headers': {
                    'Content-Type': 'application/json'
                }
            };
            request(options, function (error, response) {
                if (error) throw new Error(error);
                console.log(response.body);
            });

            myobj._id = res["ops"][0]["_id"].toString();


            MAIL(self.body.email, '@(Order #) ' + res["ops"][0]["_id"].toString(), '=?/mails/warranty', myobj);

            console.log(paytmParams)

            checksum_lib.genchecksum(paytmParams, CHECK_SUM_KEY, function (err, checksum) {

                /* for Staging */
                // var url = "https://securegw-stage.paytm.in/order/process";

                /* for Production */
                var url = "https://securegw.paytm.in/order/process";
                console.log('checksum', checksum);

                paytmParams.checksum = checksum;
                paytmParams.submit_url = url;
                console.log('paytmParams', paytmParams);
                //self.json(paytmParams);
                self.view('paytm-test', paytmParams);
            });
        });
    });
}


function insurance_paytm_cb() {
    var self = this;

    console.log("paytmcallback", self.body);
    const checksum_lib = require('../modules/paytm-checksum');


    var paytmChecksum = "";


    var paytmParams = {};
    for (var key in self.body) {
        if (key == "CHECKSUMHASH") {
            paytmChecksum = self.body[key];
        }
        else {
            paytmParams[key] = self.body[key];
        }
    }


    var isValidChecksum = checksum_lib.verifychecksum(paytmParams, CHECK_SUM_KEY, paytmChecksum);
    if (isValidChecksum) {
        console.log("Checksum Matched");
        if (paytmParams["STATUS"] == "TXN_SUCCESS") {

            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("happi");
                var myquery = { _id: new ObjectID(paytmParams["ORDERID"]) };
                var newvalues = { $set: { datepaid: new Date(), txnid: paytmParams["TXNID"], 'status': "PAID" } };
                dbo.collection("insurance-orders").updateOne(myquery, newvalues, function (err, res) {
                    if (err) throw err;
                    console.log("1 document updated");
                    //db.close();



                    dbo.collection("insurance-orders").findOne(myquery, function (err, result) {
                        if (err) throw err;
                        // console.log(result.name);
                        db.close();
                        //self.view('eshop/insurance', result);

                        // myobj._id = res["ops"][0]["_id"].toString();


                        MAIL(result.email, '@(Order #) ' + result["_id"].toString(), '=?/mails/warranty', result);


                        var message = `Congratulations your order for 1 year extended warranty is completed.Please click on link below for details https://happimobiles.com/warranty/${paytmParams['ORDERID']}`;
                        var options = {
                            'method': 'POST',
                            'url': `http://api.smscountry.com/SMSCwebservice_bulk.aspx?User=happi9&passwd=Happi@12345&mobilenumber=91${result.phone}&message=${message}&sid=HappiM&mtype=N&DR=Y`,
                            'headers': {
                                'Content-Type': 'application/json'
                            }
                        };
                        request(options, function (error, response) {
                            if (error) throw new Error(error);
                            console.log(response.body);
                        });
                        setTimeout(function () {
                            // body...
                            self.redirect('/warranty/' + paytmParams["ORDERID"] + '#PAID');
                        }, 500);
                    });




                });
            });



        }
        else {
            self.redirect('/warranty/' + paytmParams["ORDERID"] + '#PAYMENT_FAILED');
        }
    }
    else {
        self.redirect('/warranty/' + paytmParams["ORDERID"] + '#PAYMENT_FAILED');
    }

}


function view_insurance_by_id(id) {
    var self = this;
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("happi");
        var myquery = { _id: new ObjectID(id) };
        dbo.collection("insurance-orders").findOne(myquery, function (err, result) {
            if (err) throw err;
            console.log(result);
            db.close();
            self.view('eshop/insurance', result);
        });
    });

}



