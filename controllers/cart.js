var MONGO_DB_CONNECTION = process.env.MONGO_DB_CONNECTION || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);
var jwt = require('jsonwebtoken');
var fs = require('fs');
const { verify } = require('crypto');


// importing pincode verify module
var pincodeVerify = MODULE('pincodetest');

// create uuid module import
var generateUuidModule = MODULE('generate-uuid');

// jwt secret key 
var JWT_SECRET_KEY = process.env.JWT_SECRET_KEY || 'happi_jwt_secret';



exports.install = function () {
    // cart apis
    ROUTE('/api/cart/', addCart, ['post', 'cors']);
    ROUTE('/api/cart/v2', addCart, ['post', 'cors']);

    ROUTE('/api/cart/', getCart, ['get', 'cors']);
    ROUTE('/api/cart/', deleteCart, ['delete', 'cors']);

    ROUTE('/api/wishlist/', addWishlist, ['post', 'cors']);
    ROUTE('/api/wishlist/', getWishlist, ['get', 'cors']);
    ROUTE('/api/wishlist/', deleteWishlist, ['delete', 'cors']);

    ROUTE('/api/order/create-v2', createOrder, ['post', 'cors', 10000]);
    ROUTE('/api/order/v2', fetchOrder, ['post', 'cors', 30000]);

    //coupon
    ROUTE('/api/cart/coupon', verifyCoupon, ['post', 'cors', 30000]);
    ROUTE('/api/cart/remove-coupon', deleteCoupon, ['post', 'cors', 30000]);
    ROUTE('/api/cart/onetime-coupon', verifyOneTimeCoupon, ['post', 'cors', 30000]);

}

async function verifyOneTimeCoupon(phone, coupon) {
    var nosql = new Agent();
    var output = {};
    nosql.select('getCartCoupon', 'cart').make(function (builder) {
        builder.where('id', phone);
        builder.first();
    });

    var getCartCoupon = await nosql.promise('getCartCoupon');
    if (getCartCoupon != null) {
        nosql.select('getCouponCode', 'one_time_coupon').make(function (builder) {
            builder.where('coupon', coupon);
            builder.first();
        });

        var getCouponCode = await nosql.promise('getCouponCode');
        //console.log("getCouponCode", getCouponCode);
        if (getCouponCode != null) {
            // Check coupon is active
            if (getCouponCode.isActive == false) {
                output = {
                    status: false,
                    message: `Coupon ${coupon} is expired`,
                }
                return output;
            }

            // order value
            var cartData = processCart(getCartCoupon);

            //console.log("cartData---------", cartData);
            if (getCouponCode.type == 'P') {
                if (cartData.totalPrice >= getCouponCode.orderMiniAmount) {
                    var offeramount = (cartData.totalPrice * getCouponCode.offerPercentage) / 100;
                    if (offeramount >= getCouponCode.offerMaxAmount) {
                        output = {
                            status: true,
                            data: 'offer value ' + getCouponCode.offerMaxAmount,
                            amount: getCouponCode.offerMaxAmount
                        }
                    }
                    else {
                        output = {
                            status: true,
                            data: 'offer value ' + offeramount,
                            amount: offeramount
                        }
                    }

                }
                else {
                    output = {
                        status: false,
                        data: 'order minimum value ' + getCouponCode.orderMiniAmount
                    }
                }
            }
            else if (getCouponCode.type == 'amount') {
                if (cartData.totalPrice >= getCouponCode.minimum_amount) {
                    var offeramount = getCouponCode.amount;
                    nosql.update('updateDiscount', 'cart').make(function (builder) {
                        builder.set('coupon', coupon);
                        builder.set('discount', offeramount);
                    })
                    var updateDiscount = await nosql.promise('updateDiscount');
                    console.log("updateDiscount", updateDiscount);
                    if (updateDiscount > 0) {
                        output = {
                            status: true,
                            message: 'offer value ' + getCouponCode.amount,
                            amount: offeramount
                        }
                        return output;

                    } else {
                        console.log("Dicsount update fail");
                        // output = ({
                        //     status: false,
                        //     message: "Dicsount update fail",
                        // })
                    }
                    output = {
                        status: true,
                        message: 'offer value ' + getCouponCode.amount,
                        amount: offeramount
                    }
                    return output;
                }
                else {
                    output = {
                        status: false,
                        message: 'order minimum value ' + getCouponCode.minimum_amount
                    }
                    return output;
                }
            }

        } else {
            output = {
                status: false,
                message: "No coupon code found"
            }
            return output;
        }
    } else {
        output = {
            status: false,
            message: "No coupon code found"
        }
        return output;
    }

}

async function deleteCoupon() {
    var self = this;
    var nosql = new Agent();
    var token = self.headers['x-auth'];
    var body = self.body;
    // token verify
    if (token != null) {
        try {
            decoded = jwt.verify(token, JWT_SECRET_KEY);
            console.log("decoded", decoded);
            var phone = decoded.phone;
            if (decoded != null) {
                nosql.select('getCartCoupon', 'cart').make(function (builder) {
                    builder.where('id', phone);
                    builder.first();
                });

                var getCartCoupon = await nosql.promise('getCartCoupon');
                console.log("getCartCoupon", getCartCoupon);
                if (getCartCoupon != null) {
                    var coupon = "";
                    var discount = null;
                    nosql.update('updateCoupon', 'cart').make(function (builder) {
                        builder.set('coupon', coupon);
                        builder.set('discount', discount);
                        builder.where('id', decoded.phone);
                    });

                    var updateCoupon = await nosql.promise('updateCoupon');
                    if (updateCoupon > 0) {
                        console.log("COUPON REMOVED");
                        self.json({
                            status: true,
                            message: "Coupon removed"
                        })
                    }
                } else {
                    self.json({
                        status: false,
                        message: "No coupon code found"
                    })
                }
            } else {
                self.throw401("Invalid Token");
            }
        } catch (err) {
            // err
            console.log("err", err);
            self.throw401("Invalid Token");
        }
    } else {
        self.throw401("Please provide token");
    }
}

async function verifyCoupon() {
    var self = this;
    var nosql = new Agent();
    var token = self.headers['x-auth'];
    var body = self.body;

    if (body.coupon == "" || body.coupon == null) {
        return self.json({
            status: false,
            message: "Coupon Code is invalid"
        })
    }
    // token verify
    if (token != null) {
        try {
            decoded = jwt.verify(token, JWT_SECRET_KEY);
            console.log("decoded", decoded);
            var phone = decoded.phone;
            if (decoded != null) {

                // verify if it is one time coupon 
                var dbCoupons = await getCoupons();
                var couponsArr = [];

                for (let i = 0; i < dbCoupons.length; i++) {
                    var element = dbCoupons[i];
                    couponsArr.push(element.code);
                }
                if (couponsArr.indexOf(body.coupon) == -1) {
                    console.log("COUPON ONE TIME VERIFY-------------");
                    var verifyoneTimeCoupon = await verifyOneTimeCoupon(phone, body.coupon);
                    return self.json(verifyoneTimeCoupon);
                }

                // verify if the regular coupon are reused
                if (body.coupon != "" && couponsArr.indexOf(body.coupon) != -1) {
                    console.log("REGULAR COUPON VERIFY-------------");
                    var regularCouponStatus = await checkRegularCoupon(body.coupon, phone);
                    if (regularCouponStatus != null) {
                        return self.json(regularCouponStatus)
                    }
                }

                nosql.select('getCartCoupon', 'cart').make(function (builder) {
                    builder.where('id', phone);
                    builder.first();
                });

                var getCartCoupon = await nosql.promise('getCartCoupon');
                //console.log("getCartCoupon",getCartCoupon);
                if (getCartCoupon != null) {
                    nosql.select('getCouponCode', 'coupon').make(function (builder) {
                        builder.where('code', body.coupon);
                        builder.first();
                    });

                    var getCouponCode = await nosql.promise('getCouponCode');
                    //console.log("getCouponCode", getCouponCode);
                    if (getCouponCode != null) {
                        if (getCouponCode.isActive) {
                            let st = new Date(getCouponCode.startDate);
                            st.setHours(0, 0, 0, 0)
                            var stnew = new Date(st.getFullYear() + '-' + st.getMonth() + '-' + st.getDate());
                            var ed = new Date(getCouponCode.endDate);
                            var ednew = new Date(ed.getFullYear() + '-' + ed.getMonth() + '-' + ed.getDate());
                            var current = new Date();
                            var currentnew = new Date(current.getFullYear() + '-' + current.getMonth() + '-' + current.getDate());

                            // order value
                            var cartData = processCart(getCartCoupon);

                            //console.log("cartData---------", cartData);
                            if (stnew <= currentnew && currentnew <= ednew) {
                                if (getCouponCode.type == 'P') {
                                    if (cartData.totalPrice >= getCouponCode.orderMiniAmount) {
                                        var offeramount = (cartData.totalPrice * getCouponCode.offerPercentage) / 100;
                                        if (offeramount >= getCouponCode.offerMaxAmount) {
                                            res.send({
                                                status: true,
                                                data: 'offer value ' + getCouponCode.offerMaxAmount,
                                                amount: getCouponCode.offerMaxAmount
                                            })
                                        }
                                        else {
                                            res.send({
                                                status: true,
                                                data: 'offer value ' + offeramount,
                                                amount: offeramount
                                            })
                                        }

                                    }
                                    else {
                                        self.json({
                                            status: false,
                                            data: 'order minimum value ' + getCouponCode.orderMiniAmount
                                        })
                                    }
                                }
                                else if (getCouponCode.type == 'A') {
                                    if (cartData.totalPrice >= getCouponCode.orderMiniAmount) {
                                        var offeramount = getCouponCode.offerMaxAmount;
                                        nosql.update('updateDiscount', 'cart').make(function (builder) {
                                            builder.set('coupon', body.coupon);
                                            builder.set('discount', offeramount);
                                        })
                                        var updateDiscount = await nosql.promise('updateDiscount');
                                        console.log("updateDiscount", updateDiscount);
                                        if (updateDiscount > 0) {
                                            self.json({
                                                status: true,
                                                message: 'offer value ' + getCouponCode.offerMaxAmount,
                                                amount: offeramount
                                            })

                                        } else {
                                            self.json({
                                                status: false,
                                                message: "Dicsount update fail",
                                            })
                                        }
                                        self.json({
                                            status: true,
                                            message: 'offer value ' + getCouponCode.offerMaxAmount,
                                            amount: offeramount
                                        })
                                    }
                                    else {
                                        self.json({
                                            status: false,
                                            message: 'order minimum value ' + getCouponCode.orderMiniAmount
                                        })
                                    }
                                }
                            }
                            else {
                                self.json({
                                    status: false,
                                    message: 'Coupon does not exist'
                                })
                            }
                        } else {
                            self.json({
                                status: false,
                                message: 'Coupon does not exist'
                            })
                        }

                    } else {
                        self.json({
                            status: false,
                            message: "No coupon code found"
                        })
                    }
                } else {
                    self.json({
                        status: false,
                        message: "No coupon code found"
                    })
                }
            } else {
                self.throw401("Invalid Token");
            }
        } catch (err) {
            // err
            console.log("err", err);
            self.throw401("Invalid Token");
        }
    } else {
        self.throw401("Please provide token");
    }
}

async function addWishlist() {
    var self = this;
    var nosql = new Agent();
    var JOB_ID = generateUuidModule.createUUID();

    if (self.body.product_id == null || self.body.product_id == '') {
        self.json({
            state: false,
            message: "invalid product_id"
        });
        return;
    }

    var token = self.headers['x-auth'];
    if (token != null) {
        try {

            decoded = jwt.verify(token, JWT_SECRET_KEY);

            nosql.select('wishlist', 'wishlist').make(function (builder) {
                builder.and();
                builder.where('user_id', decoded.phone);
                builder.where('product_id', self.body.product_id);
            });

            var wishlist = await nosql.promise('wishlist');

            console.log("WISHLIST_TRIGGER", JOB_ID, "WISHLIST-VERIFY", wishlist);

            if (wishlist.length != 0) {
                self.json({
                    state: false,
                    message: "Product already added to your wishlist"
                });
                return;
            }

            var obj = {
                id: UID(),
                user_id: decoded.phone,
                product_id: self.body.product_id,
                datecreated: new Date()
            };

            nosql.insert('AddWishlist', 'wishlist').make(function (builder) {
                builder.set(obj);
            });

            var wishlist = await nosql.promise('AddWishlist');

            self.json({
                state: true
            });



        } catch (err) {
            self.json({
                state: false,
                message: "Sorry some thing went wrong"
            });
            return;
        }
    }
}

async function getWishlist() {
    var self = this;
    var nosql = new Agent();
    var JOB_ID = generateUuidModule.createUUID();

    var token = self.headers['x-auth'];
    if (token != null) {
        try {

            decoded = jwt.verify(token, JWT_SECRET_KEY);
            var phone = decoded.phone;

            nosql.select('wishlist', 'wishlist').make(function (builder) {
                builder.where('user_id', decoded.phone);
                builder.fields('product_id');
            });

            var wishlist = await nosql.promise('wishlist');

            console.log("WISHLIST_TRIGGER", JOB_ID, "WISHLIST-VERIFY", wishlist);


            var products = [];

            for (var i = 0; i < wishlist.length; i++) {
                products.push(wishlist[i].product_id);
            }


            nosql.select('product', 'product').make(function (builder) {
                builder.in('id', products);
                builder.fields('id', 'mrp', 'linker', 'linker_category', 'linker_manufacturer', 'category', 'manufacturer', 'name', 'pricemin', 'priceold', 'isnew', 'istop', 'pictures', 'availability', 'datecreated', 'ispublished', 'signals', 'size', 'stock', 'color', 'purchase_type', 'ftrFeatures', 'ftrTransfer', 'product_type', 'prices', 'istvs', 'booking_type', 'payPrice');
            });

            var product = await nosql.promise('product');

            self.json(product);



        } catch (err) {
            console.log("WISHLIST_TRIGGER", JOB_ID, "WISHLIST-ERROR", err);
        }
    }
}

async function deleteWishlist() {
    var self = this;
    var nosql = new Agent();
    var JOB_ID = generateUuidModule.createUUID();

    var token = self.headers['x-auth'];
    if (token != null) {
        try {

            decoded = jwt.verify(token, JWT_SECRET_KEY);

            nosql.remove('wishlist', 'wishlist').make(function (builder) {
                builder.and();
                builder.where('user_id', decoded.phone);
                builder.where('product_id', self.body.product_id);
            });

            var wishlist = await nosql.promise('wishlist');
            console.log("DELETE_WISHLIST_TRIGGER", new Date().toISOString(), JOB_ID, wishlist);
            self.json({
                state: true
            });

        } catch (err) {
            self.json({
                state: false,
                message: "Something went wrong"
            });
        }
    }
}

function createNumber(nosql) {
    var year = F.datetime.getFullYear();
    var key = 'numbering' + year;
    var number = (nosql.get(key) || 0) + 1;
    nosql.set(key, number);
    return (year + '000001').parseInt() + number;
}

async function fetchOrder() {
    var self = this;
    var nosql = new Agent();
    var JOB_ID = generateUuidModule.createUUID();

    var token = self.headers['x-auth'];
    if (token != null) {
        try {
            decoded = jwt.verify(token, JWT_SECRET_KEY);
            console.log("FETCH_ORDER_V2", JOB_ID, "decoded", decoded);
            var phone = decoded.phone;

            nosql.select('order', 'orders').make(function (builder) {
                builder.where('iduser', phone);
                builder.sort("datecreated", 'desc');
            });

            var orders = await nosql.promise('order');
            self.json(orders);

        } catch (err) {
            console.log("fetchOrder", err);
            self.json(err);
        }
    }
}

async function createOrder() {
    var self = this;
    var nosql = new Agent();
    var JOB_ID = generateUuidModule.createUUID();
    var no = NOSQL('orders');

    var token = self.headers['x-auth'];
    if (token != null) {
        try {
            decoded = jwt.verify(token, JWT_SECRET_KEY);
            console.log("CREATE_ORDER_V2", JOB_ID, "decoded", decoded);
            var phone = decoded.phone;
            var { cart, err } = await FetchExistingCart(phone);
            console.log("CREATE_ORDER_V2", JOB_ID, "FetchExistingCart", cart);
            console.log("CREATE_ORDER_V2", JOB_ID, "BODY", self.body);

            // nosql.select('user', 'Users').make(function(builder) {
            //     builder.where('phone', phone);
            //     builder.first();
            // });

            // var user = await nosql.promise('user');

            // stock check
            // for (let i = 0; i < cart.products.length; i++) {
            //     const element = cart.products[i];
            //     var { product, err } = await FetchProduct(element.id);
            //     //console.log("product", product);
            //     if (product == "Product Out of Stock") {
            //         self.json({
            //             status: false,
            //             message: `Product ${element.name} is Out of Stock`
            //         })
            //         return;
            //     }
            //     if (err || product == null) {
            //         self.json({
            //             status: false,
            //             message: "Invalid Product"
            //         })
            //         return;
            //     }
            // }


            var pincodedata = await pincodeVerify.pincodeVerify(self.body.billingAddress.zip);
            var obj = {
                name: self.body.billingAddress.name,
                firstname: self.body.billingAddress.name,
                lastname: self.body.billingAddress.name,
                email: self.body.email,
                phone: self.body.billingAddress.phone,
                street: self.body.billingAddress.street + " " + self.body.billingAddress.area,
                zip: self.body.billingAddress.zip,
                city: self.body.billingAddress.city,
                country: "IN",
                billingstreet: self.body.billingAddress.street + " " + self.body.billingAddress.area,
                billingcity: self.body.billingAddress.city,
                billingcountry: 'IN',
                billingzip: self.body.billingAddress.zip,
                delivery: "",
                payment: "",
                company: "",
                companyid: "",
                companyvat: "",
                datecreated: new Date(),
                iduser: phone,
                iscompany: "",
                isnewsletter: true,
                isterms: true,
                ispaid: false,
                iscod: false,
                referalId: '',
                isnewsletter: undefined,
                isemail: undefined,
                isterms: undefined,
                internal_type: "waiting for update",
                tag: "wait",
                order_from: self.body.flag || "phone",
                area: pincodedata != "Invalid Pincode" ? `${pincodedata.Districtname},${pincodedata.statename}` : "Invalid Pincode"
            };
            console.log("CREATE_ORDER_V2", JOB_ID, "FIRST_FILEDS", JSON.stringify(obj));

            if (self.body.isPickupAtStore == true) {

                obj.pickupCity = self.body.storeAddress.City;
                obj.pickupLocation = self.body.storeAddress.Area;
                obj.pickupAddress = self.body.storeAddress['Full Postal Address'];
                obj.pickupMobile = self.body.storeAddress['Mobile Number'];
                obj.pickupState = self.body.storeAddress.State;
                obj.pickupPincode = self.body.storeAddress.Pincode;
                obj.ispickup = true;

                obj.deliverycity = "N/A";
                obj.deliverycountry = 'India';
                obj.deliveryfirstname = "N/A";
                obj.deliverylastname = "N/A";
                obj.deliveryphone = "N/A";
                obj.deliverystreet = "N/A";
                obj.deliveryzip = "N/A";
            } else {
                obj.pickupCity = "N/A";
                obj.pickupLocation = "N/A";
                obj.pickupAddress = "N/A";
                obj.pickupMobile = "N/A";
                obj.pickupState = "N/A";
                obj.pickupPincode = "N/A";
                obj.ispickup = false;

                obj.deliverycity = self.body.billingAddress.city;
                obj.deliverycountry = 'India';
                obj.deliveryfirstname = self.body.billingAddress.name;
                obj.deliverylastname = "";
                obj.deliveryphone = self.body.billingAddress.phone;
                obj.deliverystreet = self.body.billingAddress.street + " " + self.body.billingAddress.area;
                obj.deliveryzip = self.body.billingAddress.zip;
            }

            if (cart.products.length == 1) {
                var PinelabsproductCode = cart.products[0].PinelabsproductCode;
                obj.product_code = PinelabsproductCode == undefined || PinelabsproductCode == "" ? "none" : PinelabsproductCode;
            }

            console.log("CREATE_ORDER_V2", JOB_ID, "SECOND_FILEDS", JSON.stringify(obj));

            obj.version = "V2";
            obj.id = UID();
            obj.items = cart.products;

            obj.totalShippingPrice = cart.totalShippingPrice;
            obj.SubTotal = cart.SubTotal;
            obj.count = cart.TotalQuantity;
            obj.price = cart.totalPrice;
            obj.number = createNumber(no);
            if (cart.discount != null) {
                obj.discount = cart.discount;
                obj.coupon = cart.coupon;
            }

            console.log("CREATE_ORDER_V2", JOB_ID, "CART", JSON.stringify(obj));



            nosql.insert('order', 'orders').make(function (builder) {
                builder.set(obj);
            });

            console.log("CREATE_ORDER_V2", JOB_ID, "ORDER_SAVE_PRE", JSON.stringify(obj));

            var cart = await nosql.promise('order');

            console.log("CREATE_ORDER_V2", JOB_ID, "ORDER_POST", JSON.stringify(cart));



            // update coupon status
            // verify if it is one time coupon 
            var dbCoupons = await getCoupons();
            var couponsArr = [];

            for (let i = 0; i < dbCoupons.length; i++) {
                var element = dbCoupons[i];
                couponsArr.push(element.code);
            }

            if (obj.coupon != "" && couponsArr.indexOf(obj.coupon) == -1) {
                var couponStatusUpdate = await updateOneTimeCoupon(obj.coupon, obj.id, phone);
            }

            // verify if the regular coupon are reused
            if (obj.coupon != "" && couponsArr.indexOf(obj.coupon) != -1) {
                var regularCouponStatusUpdate = await updateRegularCoupon(obj.coupon, obj.id, phone);
                if (regularCouponStatusUpdate != null) {
                    return self.json(regularCouponStatusUpdate)
                }
            }

            self.json({ success: true, value: obj.id });

            // update the referalid in orders table
            updateReferalid(phone)

            nosql.remove('cart', 'cart').make(function (builder) {
                builder.where('id', decoded.phone);
            })

            await nosql.promise('cart');


        } catch (err) {
            console.log("CREATE_ORDER_V2", JOB_ID, "ORDER_ERROR", err);
            self.json({ cart: null, err: err });
        }

    }

    /*
    {isPickupAtStore: true,
         storeAddress: {
             City: Khammam, Retailer Locality 
             Area: Khammam, 
             Full Postal Address : Sh No: 8-1-152 & 8-1-153, 
             Mobile Number: 9100783666, 
             State: TG, 
             Pincode: 507001}, 
    billingAddress: {
        name: hari, phone: 9703220974, 
        street: ar, area: ff, 
        city: ag, 
        zip: 523226}}
        */

}

function processCart(cart) {
    var totalShippingPrice = 0;
    var SubTotal = 0;
    var TotalQuantity = 0;
    if (cart.products != undefined) {
        for (var i = 0; i < cart.products.length; i++) {
            var product = cart.products[i];
            if (product.shippingPrice != undefined) {
                totalShippingPrice += product.shippingPrice;
            }
            SubTotal += (product.payPrice * product.quantity);
            TotalQuantity += product.quantity;
        }
    }
    // coupon discount
    if (cart.discount > 0) {
        //console.log("discount greater than 0");
        cart.totalPrice = (totalShippingPrice + SubTotal) - (cart.discount);
        cart.TotalQuantity = TotalQuantity;
        cart.SubTotal = SubTotal;
        cart.totalShippingPrice = totalShippingPrice;

    } else {
        cart.totalPrice = totalShippingPrice + SubTotal;
        cart.TotalQuantity = TotalQuantity;
        cart.SubTotal = SubTotal;
        cart.totalShippingPrice = totalShippingPrice;
    }
    //console.log("cart", cart);

    return cart;
}

async function FetchExistingCart(phone) {
    var nosql = new Agent();

    nosql.select('cart', 'cart').make(function (builder) {
        builder.where('id', phone);
        builder.first()
    });
    try {
        var cart = await nosql.promise('cart');
        // console.log("cart", cart);
        return { cart: processCart(cart), err: null };
    } catch (err) {
        return { cart: null, err: err };
    }

}

async function FetchProduct(id) {
    var nosql = new Agent();
    //console.log("id", id)
    nosql.select('getProduct', 'product').make(function (builder) {
        builder.where('id', id);
        builder.fields('name', 'id', 'payPrice', 'shippingPrice', 'pictures', 'stock', 'ftrBrand', 'offerdesc', 'iscod', 'ispickup', 'stock', 'PinelabsproductCode');
        builder.first();
    });
    try {
        var product = await nosql.promise('getProduct');
        if (product.stock == 0) {
            return { product: "Product Out of Stock", err: null }
        }
        //console.log("productsssssssss", product);
        if (product.shippingPrice == null) {
            product.shippingPrice = 0
        }
        if (product.payPrice == null) {
            product.payPrice = 0
        }

        return { product: product, err: null };
    } catch (err) {
        return { product: null, err: err };
    }

}

async function addCart() {
    var JOB_ID = generateUuidModule.createUUID();
    var self = this;
    var body = self.body;
    var obj = {};
    // obj.id = UID();

    //var phoneNo = obj.phoneNo; 
    // var productId = obj.productId;
    // var productPrice =  obj.productPrice;

    obj.products = [];


    var token = self.headers['x-auth'];
    if (body.quantity == null) {
        self.json({
            status: false,
            message: "Invalid Quantity"
        })
        return;
    }
    // token verify
    if (token != null) {
        try {
            decoded = jwt.verify(token, JWT_SECRET_KEY);
            console.log("decoded", decoded);
            obj.id = decoded.phone;
            if (decoded != null) {
                var { cart, err } = await FetchExistingCart(decoded.phone);
                // if (err) {
                //     self.json({
                //         status: false,
                //       //  tag:"Unable to fetch product with id"
                //         message: "Unable to fetch product with id"
                //     })
                //     return;
                // }
                //console.log("CARTTTTTTTTTTTTTTTTTT", cart)
                var mongoClient = new Agent();
                // add cart
                if (cart == null) {
                    console.log("ADD_CART", new Date().toISOString(), JOB_ID, "ADD NEW CART");
                    // productId
                    // Q
                    // Fetch Product By Id , Name, Shipping Price, Picture, payPrice, stock,  
                    var { product, err } = await FetchProduct(body.productId);
                    //console.log("product", product);
                    if (product == "Product Out of Stock") {
                        self.json({
                            status: false,
                            message: "Product Out of Stock"
                        })
                        return;
                    }
                    if (err || product == null) {
                        self.json({
                            status: false,
                            message: "Invalid Product"
                        })
                        return;
                    }

                    product.quantity = body.quantity;
                    obj.products.push(product);
                    obj.datecreated = new Date();
                    obj.dateupdated = new Date();

                    mongoClient.insert('addCart', 'cart').make(function (builder) {
                        builder.set(obj);
                    });
                } else { // update cart
                    let itemIndex = cart.products.findIndex(p => p.id == body.productId);
                    // console.log("itemIndex",itemIndex);
                    if (itemIndex > -1) {
                        // product exists in cart
                        console.log("UPDATE_CART", new Date().toISOString(), JOB_ID, "UPDATE  EXISTING PRODUCT IN CART");
                        var lineItems = [];
                        for (let i = 0; i < cart.products.length; i += 1) {
                            if (cart.products[i].id === body.productId) {
                                cart.products[i].quantity = Math.max(0, cart.products[i].quantity + body.quantity);
                            }

                            if (cart.products[i].quantity >= 1) {
                                lineItems.push(cart.products[i]);
                            }
                        }
                        // console.log("lineItems",lineItems);

                        cart.products = lineItems;

                    } else {
                        //product does not exists in cart, add new product
                        console.log("UPDATE_CART", new Date().toISOString(), JOB_ID, "ADD NEW PRODUCT IN CART");
                        var { product, err } = await FetchProduct(body.productId);
                        if (product == "Product Out of Stock") {
                            self.json({
                                status: false,
                                message: "Product Out of Stock"
                            })
                            return;
                        }
                        if (err || product == null) {
                            self.json({
                                status: false,
                                message: "Invalid Product"
                            })
                            return;
                        }

                        product.quantity = body.quantity;
                        //   console.log("product", product)
                        cart.products.push(product);
                        //  console.log("cart", cart.products);
                    }
                    cart.dateupdated = new Date();
                    mongoClient.update('updateCart', 'cart').make(function (builder) {
                        builder.set('products', cart.products);
                        builder.where('id', decoded.phone);
                    });
                }

                mongoClient.select('cart', 'cart').make(function (builder) {
                    builder.where('id', decoded.phone);
                    builder.first();
                });



                mongoClient.exec(async function (err, response) {
                    //console.log("res", res.updateCart);
                    if (err) {
                        self.json({
                            status: false,
                            message: "Unable to create cart"
                        })
                    } else {
                        var cartData = processCart(response.cart);
                        // console.log("cart data", cartData);

                        // NEW CODE START --------------------------------------------------------------

                        // if the coupon is from the coupon collection
                        if (cartData.coupon != "") {
                            var output = await cartPriceCheck(cartData, decoded.phone)
                            return self.json(output);
                        }

                        // if the coupon is from the one_time_coupon collection
                        var dbCoupons = await getCoupons();
                        var couponsArr = [];
                        for (let i = 0; i < dbCoupons.length; i++) {
                            var element = dbCoupons[i];
                            couponsArr.push(element.code);
                        }
                        if (cartData.coupon != "" && couponsArr.indexOf(cartData.coupon) == -1) {
                            console.log("COUPON ONE TIME CHECK-------------");
                            var output = await cartPriceCheckForOnetime(cartData, decoded.phone)
                            return self.json(output);
                        }

                        // NEW CODE END --------------------------------------------------------------

                        self.json({
                            status: true,
                            message: "Success",
                            data: processCart(response.cart)
                        })

                    }
                });
            }
        } catch (err) {
            // err
            console.log("err", err);
            self.throw401("Invalid Token");
        }
    } else {
        self.throw401("Please provide token");
    }
}

async function getCart() {
    var self = this;
    var mnosql = new Agent();
    var token = self.headers['x-auth'];
    // token verify
    if (token != null) {
        try {
            decoded = jwt.verify(token, JWT_SECRET_KEY);
            console.log("decoded", decoded);
            if (decoded != null) {
                mnosql.select('cart', 'cart').make(function (builder) {
                    builder.where('id', decoded.phone);
                    builder.first()
                })

                mnosql.exec(async function (err, response) {
                    console.log("MongoErr", err);

                    if (response.cart == null) {
                        self.json({
                            status: false,
                            message: "Your cart is empty"
                        })
                    } else { // update cart
                        // console.log("response.cart",response.cart); 

                        // stock check
                        // for (let i = 0; i < response.cart.products.length; i++) {
                        //     const element = response.cart.products[i];
                        //     var { product, err } = await FetchProduct(element.id);
                        //     //console.log("product", product);
                        //     if (product == "Product Out of Stock") {
                        //         self.json({
                        //             status: false,
                        //             message: `Product ${element.name} is Out of Stock`
                        //         })
                        //         return;
                        //     }
                        //     if (err || product == null) {
                        //         self.json({
                        //             status: false,
                        //             message: "Invalid Product"
                        //         })
                        //         return;
                        //     }
                        // }

                        var cartData = processCart(response.cart);
                        //console.log("cart data", cartData);
                        // NEW CODE START --------------------------------------------------------------

                        // if the coupon is from the coupon collection
                        if (cartData.coupon != "") {
                            var output = await cartPriceCheck(cartData, decoded.phone)
                            return self.json(output);
                        }

                        // if the coupon is from the one_time_coupon collection
                        var dbCoupons = await getCoupons();
                        var couponsArr = [];
                        for (let i = 0; i < dbCoupons.length; i++) {
                            var element = dbCoupons[i];
                            couponsArr.push(element.code);
                        }
                        if (cartData.coupon != "" && couponsArr.indexOf(cartData.coupon) == -1) {
                            console.log("COUPON ONE TIME CHECK-------------");
                            var output = await cartPriceCheckForOnetime(cartData, decoded.phone)
                            return self.json(output);
                        }

                        // NEW CODE END --------------------------------------------------------------

                        self.json({
                            status: true,
                            message: "Success",
                            data: processCart(response.cart)
                        })
                    }
                })
            }
        } catch (err) {
            // err
            console.log("err", err);
            self.throw401("Invalid Token");
        }
    } else {
        self.throw401("Please provide token");
    }
}

function deleteCart() {
    var self = this;
    var mnosql = new Agent();
    var token = self.headers['x-auth'];
    // token verify
    if (token != null) {
        try {
            decoded = jwt.verify(token, JWT_SECRET_KEY);
            console.log("decoded", decoded);
            if (decoded != null) {

                // removes entire cart
                mnosql.remove('cart', 'cart').make(function (builder) {
                    builder.where('id', decoded.phone);
                })

                // // removes specific product
                // mnosql.remove('cart', 'cart').make(function (builder) {
                //     builder.where('id', decoded.phone);
                //     builder.in('productId', self.body.productId);
                //     builder
                // })

                mnosql.exec(function (err, response) {
                    console.log("MongoErr", err);

                    if (response.cart == null) {
                        self.json({
                            status: false,
                            message: "Unable to clear"
                        })
                    } else { // update cart
                        self.json({
                            status: true,
                            message: "Success",
                            data: processCart(response.cart)
                        })
                    }
                })
            }
        } catch (err) {
            // err
            console.log("err", err);
            self.throw401("Invalid Token");
        }
    } else {
        self.throw401("Please provide token");
    }
}

async function updateReferalid(iduser) {
    var nosql = new Agent();
    var res = JSON.parse(fs.readFileSync(__dirname + '/../ExtraFunctions/referal.json'));
    //console.log("res", res);
    // return;
    res.forEach(element => {
        if (element['Mobile Number'] == iduser) {
            console.log("mobile number", element['Mobile Number']);
            nosql.update('updateOrder', 'orders').make(function (builder) {
                builder.set('referalId', element['Retailer Locality Area']);
                builder.where('iduser', element['Mobile Number'])
            });
            nosql.exec(function (err, response) {
                if (err) {
                    console.log("mongoerr", err);
                }
                console.log("response", response.updateOrder);

            });
        }

    });

}

async function updateCouponFunc(phone) {
    var nosql = new Agent();
    var coupon = "";
    var discount = null;
    nosql.update('updateCoupon', 'cart').make(function (builder) {
        builder.set('coupon', coupon);
        builder.set('discount', discount);
        builder.where('id', phone);
    });

    var updateCoupon = await nosql.promise('updateCoupon');
    if (updateCoupon > 0) {
        console.log("COUPON REMOVED");
    }
}


// to update the onetime coupon so that it cant be used again
async function updateOneTimeCoupon(coupon, orderid, phone) {
    var nosql = new Agent();
    nosql.update('updateCoupon', 'one_time_coupon').make(function (builder) {
        builder.set({
            'isActive': false,
            'orderid': orderid,
            'phone': phone,
            'dateused': new Date()
        });
        builder.where('coupon', coupon);
    });

    var updateCoupon = await nosql.promise('updateCoupon');
    if (updateCoupon > 0) {
        console.log("COUPON EXPIRED " + coupon);
    }
}

// function to get the regular coupons from coupon collection
async function getCoupons() {
    var nosql = new Agent();
    nosql.select('getCoupons', 'coupon').make(function (builder) {
        builder.where('isActive', true);
    })
    var getCoupons = await nosql.promise('getCoupons');
    return getCoupons;
}


// function to check the cart price and update the  regular coupon in the cart
async function cartPriceCheck(cartData, phone) {
    var output = {};
    var dbCoupons = await getCoupons();
    var couponMessage;
    for (let i = 0; i < dbCoupons.length; i++) {
        const element = dbCoupons[i];
        if (element.code == cartData.coupon) {
            couponMessage = element.description;
        }
    }
    if (cartData.totalPrice < 10000) {
        console.log("LESS THAN 10000");
        var updateCoupon = await updateCouponFunc(phone);
        return output = {
            status: true,
            message: "Success",
            data: cartData
        }
    } else if (cartData.products.length == 0) {
        console.log("NO PRODUCTS");
        var updateCoupon = await updateCouponFunc(phone);

        return output = {
            status: true,
            message: "Success",
            data: cartData
        }
    } else {
        return output = {
            status: true,
            message: "Success",
            data: cartData,
            couponMessage: couponMessage
        }
    }
}

// function to check the cart price and update the onetime coupon in the cart
async function cartPriceCheckForOnetime(cartData, phone) {
    var output = {};
    if (cartData.totalPrice < 10000) {
        console.log("LESS THAN 10000");
        var updateCoupon = await updateCouponFunc(phone);
        return output = {
            status: true,
            message: "Success",
            data: cartData
        }
    } else if (cartData.products.length == 0) {
        console.log("NO PRODUCTS");
        var updateCoupon = await updateCouponFunc(phone);
        return output = {
            status: true,
            message: "Success",
            data: cartData
        }
    } else {
        return output = {
            status: true,
            message: "Success",
            data: cartData
        }
    }
}

// function to check the regular coupon is reused while creating order
async function updateRegularCoupon(coupon, orderid, phone) {
    //console.log("inside updateRegularCoupon funtion");
    var nosql = new Agent();
    var output;
    nosql.select('getcouponExpiry', 'coupon_expiry_details').make(function (builder) {
        builder.where('id', phone);
        builder.where('coupon', coupon);
        builder.first();
    })

    var getcouponExpiry = await nosql.promise('getcouponExpiry');
    if (getcouponExpiry != null) {
        return output = {
            status: false,
            message: `${coupon} is already used`
        }
    } else {
        nosql.insert('update_RegularCoupon', 'coupon_expiry_details').make(function (builder) {
            builder.set({
                'id': phone,
                'orderid': orderid,
                'coupon': coupon,
                'dateused': new Date()
            });
        });

        var update_RegularCoupon = await nosql.promise('update_RegularCoupon');
        if (update_RegularCoupon != null) {
            console.log(`COUPON :${coupon} EXPIRED FOR USER ${phone}`);
        }
    }
}

// function to check the regular coupon is reused while verifying coupon
async function checkRegularCoupon(coupon, phone) {
    //console.log("inside updateRegularCoupon funtion");
    var nosql = new Agent();
    var output;
    nosql.select('getcouponExpiry', 'coupon_expiry_details').make(function (builder) {
        builder.where('id', phone);
        builder.where('coupon', coupon);
        builder.first();
    })

    var getcouponExpiry = await nosql.promise('getcouponExpiry');
    if (getcouponExpiry != null) {
        return output = {
            status: false,
            message: `${coupon} is already used`
        }
    } 
}



// async function addCart() {
//     var JOB_ID = generateUuidModule.createUUID();
//     var self = this;
//     var body = self.body;
//     var obj = {};
//     // obj.id = UID();

//     //var phoneNo = obj.phoneNo; 
//     // var productId = obj.productId;
//     // var productPrice =  obj.productPrice;

//     obj.products = [];


//     var token = self.headers['x-auth'];
//     if (body.quantity == null) {
//         self.json({
//             status: false,
//             message: "Invalid Quantity"
//         })
//         return;
//     }
//     // token verify
//     if (token != null) {
//         try {
//             decoded = jwt.verify(token, JWT_SECRET_KEY);
//             console.log("decoded", decoded);
//             obj.id = decoded.phone;
//             if (decoded != null) {
//                 var { cart, err } = await FetchExistingCart(decoded.phone);
//                 // if (err) {
//                 //     self.json({
//                 //         status: false,
//                 //       //  tag:"Unable to fetch product with id"
//                 //         message: "Unable to fetch product with id"
//                 //     })
//                 //     return;
//                 // }
//                 //console.log("CARTTTTTTTTTTTTTTTTTT", cart)
//                 var mongoClient = new Agent();
//                 // add cart
//                 if (cart == null) {
//                     console.log("ADD_CART", new Date().toISOString(), JOB_ID, "ADD NEW CART");
//                     // productId
//                     // Q
//                     // Fetch Product By Id , Name, Shipping Price, Picture, payPrice, stock,  
//                     var { product, err } = await FetchProduct(body.productId);
//                     //console.log("product", product);
//                     if (product == "Product Out of Stock") {
//                         self.json({
//                             status: false,
//                             message: "Product Out of Stock"
//                         })
//                         return;
//                     }
//                     if (err || product == null) {
//                         self.json({
//                             status: false,
//                             message: "Invalid Product"
//                         })
//                         return;
//                     }


//                     product.quantity = body.quantity;
//                     obj.products.push(product);
//                     obj.datecreated = new Date();
//                     obj.dateupdated = new Date();

//                     mongoClient.insert('addCart', 'cart').make(function (builder) {
//                         builder.set(obj);
//                     });
//                 } else { // update cart
//                     let itemIndex = cart.products.findIndex(p => p.id == body.productId);
//                     // console.log("itemIndex",itemIndex);
//                     if (itemIndex > -1) {
//                         // product exists in cart
//                         console.log("UPDATE_CART", new Date().toISOString(), JOB_ID, "UPDATE  EXISTING PRODUCT IN CART");
//                         var lineItems = [];
//                         for (let i = 0; i < cart.products.length; i += 1) {
//                             if (cart.products[i].id === body.productId) {
//                                 cart.products[i].quantity = Math.max(0, cart.products[i].quantity + body.quantity);
//                             }

//                             if (cart.products[i].quantity >= 1) {
//                                 lineItems.push(cart.products[i]);
//                             }
//                         }
//                         // console.log("lineItems",lineItems);

//                         cart.products = lineItems;

//                     } else {
//                         //product does not exists in cart, add new product
//                         console.log("UPDATE_CART", new Date().toISOString(), JOB_ID, "ADD NEW PRODUCT IN CART");
//                         var { product, err } = await FetchProduct(body.productId);
//                         if (product == "Product Out of Stock") {
//                             self.json({
//                                 status: false,
//                                 message: "Product Out of Stock"
//                             })
//                             return;
//                         }
//                         if (err || product == null) {
//                             self.json({
//                                 status: false,
//                                 message: "Invalid Product"
//                             })
//                             return;
//                         }

//                         product.quantity = body.quantity;
//                         //   console.log("product", product)
//                         cart.products.push(product);
//                         //  console.log("cart", cart.products);
//                     }
//                     cart.dateupdated = new Date();
//                     mongoClient.update('updateCart', 'cart').make(function (builder) {
//                         builder.set('products', cart.products);
//                         builder.where('id', decoded.phone);
//                     });
//                 }

//                 mongoClient.select('cart', 'cart').make(function (builder) {
//                     builder.where('id', decoded.phone);
//                     builder.first();
//                 });



//                 mongoClient.exec(async function (err, response) {
//                     //console.log("res", res.updateCart);
//                     if (err) {
//                         self.json({
//                             status: false,
//                             message: "Unable to create cart"
//                         })
//                     } else {
//                         var cartData = processCart(response.cart);
//                         // console.log("cart data", cartData);
//                         if (cartData.coupon == "OCTOBAG") {
//                             if (cartData.totalPrice < 10000) {
//                                 console.log("LESS THAN 10000");
//                                 var updateCoupon = await updateCouponFunc(decoded.phone);
//                                 return self.json({
//                                     status: true,
//                                     message: "Success",
//                                     data: processCart(response.cart)
//                                 })
//                             } else if (cartData.products.length == 0) {
//                                 console.log("NO PRODUCTS");
//                                 var updateCoupon = await updateCouponFunc(decoded.phone);

//                                 return self.json({
//                                     status: true,
//                                     message: "Success",
//                                     data: processCart(response.cart)
//                                 })
//                             } else {
//                                 return self.json({
//                                     status: true,
//                                     message: "Success",
//                                     data: processCart(response.cart),
//                                     couponMessage: "Congratulations! You have claimed your FREE bag worth Rs.2000 along with this order."
//                                 })
//                             }
//                         } else if (cartData.coupon == "HAPPISRH") {
//                             if (cartData.totalPrice < 10000) {
//                                 console.log("LESS THAN 10000");
//                                 var updateCoupon = await updateCouponFunc(decoded.phone);
//                                 return self.json({
//                                     status: true,
//                                     message: "Success",
//                                     data: processCart(response.cart)
//                                 })
//                             } else if (cartData.products.length == 0) {
//                                 console.log("NO PRODUCTS");
//                                 var updateCoupon = await updateCouponFunc(decoded.phone);

//                                 return self.json({
//                                     status: true,
//                                     message: "Success",
//                                     data: processCart(response.cart)

//                                 })
//                             } else {
//                                 self.json({
//                                     status: true,
//                                     message: "Success",
//                                     data: processCart(response.cart),
//                                     couponMessage: "Congratulations! You have won a free surprise gift along with your order."
//                                 })
//                             }
//                         } else if (cartData.coupon == "HAPPI") {
//                             if (cartData.totalPrice < 10000) {
//                                 console.log("LESS THAN 10000");
//                                 var updateCoupon = await updateCouponFunc(decoded.phone);
//                                 return self.json({
//                                     status: true,
//                                     message: "Success",
//                                     data: processCart(response.cart)
//                                 })
//                             } else if (cartData.products.length == 0) {
//                                 console.log("NO PRODUCTS");
//                                 var updateCoupon = await updateCouponFunc(decoded.phone);

//                                 return self.json({
//                                     status: true,
//                                     message: "Success",
//                                     data: processCart(response.cart)

//                                 })
//                             } else {
//                                 self.json({
//                                     status: true,
//                                     message: "Success",
//                                     data: processCart(response.cart),
//                                 })
//                             }
//                         }
//                         //console.log("COUPON CODE ----------", cartData.coupon);
//                         if (cartData.coupon != "" && cartData.coupon != "OCTOBAG" && cartData.coupon != "HAPPISRH" && cartData.coupon != "HAPPI") {
//                             console.log("COUPON CODE ", cartData.coupon);
//                             if (cartData.totalPrice < 10000) {
//                                 console.log("LESS THAN 10000");
//                                 var updateCoupon = await updateCouponFunc(decoded.phone);
//                                 return self.json({
//                                     status: true,
//                                     message: "Success",
//                                     data: processCart(response.cart)
//                                 })
//                             } else if (cartData.products.length == 0) {
//                                 console.log("NO PRODUCTS");
//                                 var updateCoupon = await updateCouponFunc(decoded.phone);

//                                 return self.json({
//                                     status: true,
//                                     message: "Success",
//                                     data: processCart(response.cart)
//                                 })
//                             } else {
//                                 return self.json({
//                                     status: true,
//                                     message: "Success",
//                                     data: processCart(response.cart)
//                                 })
//                             }
//                         }
//                         self.json({
//                             status: true,
//                             message: "Success",
//                             data: processCart(response.cart)
//                         })

//                     }
//                 });
//             }
//         } catch (err) {
//             // err
//             console.log("err", err);
//             self.throw401("Invalid Token");
//         }
//     } else {
//         self.throw401("Please provide token");
//     }
// }
