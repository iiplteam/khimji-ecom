var axios = require('axios');
var md5 = require("md5");
var MongoClient = require('mongodb').MongoClient;
var smsModule = MODULE('sms');

exports.install = function () {
    ROUTE('/api/sms/coupon', sendcouponsms, ['post', 'cors']);
}

function sendcouponsms() {
    var self = this;
    console.log("res", self.query);
    var url = "mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net";

    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("happi");

        dbo.collection("sms_coupon").find({ "isActive": null }).limit(1).toArray(function (err, result) {
            if (err) throw err;
            console.log(result[0]);

            smsModule.sendSMS(self.query.mobilenumber, "Dear Customer, Discount Offers OTP is " + result[0].Codes);
            var myquery = { _id: result[0]._id };
            var newvalues = { $set: { "isActive": false, "mobileNumber": self.query.mobilenumber, "VMN": self.query.VMN, "message": self.query.message, "receivedon": self.query.receivedon } };
            dbo.collection("sms_coupon").updateOne(myquery, newvalues, function (err, res) {
                if (err) throw err;
                console.log("1 document updated");
                self.json({ "status": true, data: result[0] });
                db.close();
            });
        });
    });
}