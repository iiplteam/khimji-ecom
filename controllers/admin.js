// import entire SDK
var AWS = require('aws-sdk');
var jwt = require('jsonwebtoken');
var MONGO_DB_CONNECTION = process.env.MONGO_DB_CONNECTION || 'mongodb+srv://sowmya:iNNrxOhVfEdvsUaI@cluster0-cnw2n.mongodb.net/khimji_dev';
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);
const MSG_NOTIFY = { TYPE: 'notify' };
const ALLOW = ['/api/dependencies/', '/api/pages/preview/', '/api/upload/', '/api/nav/', '/api/files/', '/stats/', '/live/', '/api/widgets/'];

var cron = require('node-cron');
const fs = require('fs');
var request = require('request');
var DDOS = {};
var WS = null;
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
const assert = require('assert');

// aws access key id 
var ACCESS_KEY_ID = F.config['ACCESS_KEY_ID'] || process.env.ACCESS_KEY_ID || 'AKIASTAEMZYQTLP6FLB4';

// aws secret access key 
var SECRET_ACCESS_KEY = process.env.SECRET_ACCESS_KEY || 'MaEdglPhsaM0TrJ4c+nBhU80p64VTncaVabMTzNV';

// jwt secret key 
var JWT_SECRET_KEY = process.env.JWT_SECRET_KEY || 'happi_jwt_secret';

// aws bucket name
var BUCKET = process.env.BUCKET || 'happimobiles';

// create uuid module import
var generateUuidModule = MODULE('generate-uuid');

// mongo db long url
const MONGO_URL = process.env.MONGO_URL || 'mongodb://sowmya:iNNrxOhVfEdvsUaI@cluster0-shard-00-00-cnw2n.mongodb.net:27017,cluster0-shard-00-01-cnw2n.mongodb.net:27017,cluster0-shard-00-02-cnw2n.mongodb.net:27017/khimji_dev?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority';

// Database Name
const DB_NAME = process.env.DB_NAME || 'khimji_dev';

// importing stock update module
var stock = MODULE('productStockUpdate');

// send sms import modile
var smsModule = MODULE('sms');

// importing the emitter
var stockSyncEmitter = require('../emitter/product_Stock_emitter');

const s3 = new AWS.S3({
	accessKeyId: ACCESS_KEY_ID,
	secretAccessKey: SECRET_ACCESS_KEY
});

global.ADMIN = {};
global.ADMIN.notify = function (value) {
	if (WS) {
		MSG_NOTIFY.type = value instanceof Object ? value.type : value;
		MSG_NOTIFY.message = value instanceof Object ? value.message : '';
		WS.send(MSG_NOTIFY);
	}
};

F.config['admin-tracking'] && ON('visitor', function (obj) {
	if (WS) {
		MSG_NOTIFY.type = 'visitor';
		MSG_NOTIFY.message = obj;
		WS.send(MSG_NOTIFY);
	}
});

exports.install = function () {

	CORS('/api/*', ['get', 'post', 'put', 'delete'], true);

	// Routes are according to the sitemap
	ROUTE('#admin', '=admin/index');

	ROUTE('#admin/api/upload/', upload, ['post', 'upload', 10000], 3084); // 3 MB
	ROUTE('#admin/api/upload/base64/', upload_base64, ['post', 10000], 2048); // 2 MB

	ROUTE('#admin/api/dashboard/', json_dashboard);
	ROUTE('#admin/api/dashboard/referrers/', json_dashboard_referrers);
	ROUTE('#admin/api/dashboard/online/', json_dashboard_online);

	// Internal
	ROUTE('#admin/api/dependencies/', ['*Settings --> dependencies']);

	// MODEL: /models/widgets.js
	ROUTE('#admin/api/widgets/', ['*Widget --> query']);
	ROUTE('#admin/api/widgets/{id}/', ['*Widget --> read']);
	ROUTE('#admin/api/widgets/', ['*Widget --> save', 'post']);
	ROUTE('#admin/api/widgets/', ['*Widget --> remove', 'delete']);
	ROUTE('#admin/api/widgets/{id}/editor/', ['*Widget --> editor']);
	ROUTE('#admin/api/widgets/dependencies/', ['*Widget --> dependencies']);
	ROUTE('#admin/api/widgets/{id}/settings/', json_widget_settings, ['*Widget']);
	ROUTE('#admin/api/widgets/{id}/backups/', json_backups);

	// MODEL: /models/widgets.js
	ROUTE('#admin/api/widgetsglobals/', ['*WidgetGlobals --> read']);
	ROUTE('#admin/api/widgetsglobals/', ['*WidgetGlobals --> save', 'post'], 30);

	// MODEL: /models/pages.js
	ROUTE('#admin/api/pages/', ['*Page --> query']);
	ROUTE('#admin/api/pages/{id}/', ['*Page --> read']);
	ROUTE('#admin/api/pages/', ['*Page --> save', 'post']);
	ROUTE('#admin/api/pages/', ['*Page --> remove', 'delete']);
	ROUTE('#admin/api/pages/stats/', ['*Page --> stats']);
	ROUTE('#admin/api/pages/{id}/stats/', ['*Page --> stats']);
	ROUTE('#admin/api/pages/{id}/backups/', json_backups);
	ROUTE('#admin/api/pages/preview/', view_pages_preview, ['json'], 512);
	ROUTE('#admin/api/pages/links/', json_pages_links);

	// MODEL: /models/pages.js
	ROUTE('#admin/api/pagesglobals/', ['*PageGlobals --> read']);
	ROUTE('#admin/api/pagesglobals/', ['*PageGlobals --> save', 'post'], 30);

	// MODEL: /models/posts.js
	ROUTE('#admin/api/posts/', ['*Post --> query']);
	ROUTE('#admin/api/posts/{id}/', ['*Post --> read']);
	ROUTE('#admin/api/posts/', ['*Post --> save', 'post']);
	ROUTE('#admin/api/posts/', ['*Post --> remove', 'delete']);
	ROUTE('#admin/api/posts/toggle/', ['*Post --> toggle']);
	ROUTE('#admin/api/posts/stats/', ['*Post --> stats']);
	ROUTE('#admin/api/posts/{id}/stats/', ['*Post --> stats']);
	ROUTE('#admin/api/posts/{id}/backups/', json_backups);

	// MODEL: /models/notices.js
	ROUTE('#admin/api/notices/', ['*Notice --> query']);
	ROUTE('#admin/api/notices/{id}/', ['*Notice --> read']);
	ROUTE('#admin/api/notices/', ['*Notice --> save', 'post']);
	ROUTE('#admin/api/notices/', ['*Notice --> remove', 'delete']);
	ROUTE('#admin/api/notices/toggle/', ['*Notice --> toggle']);
	ROUTE('#admin/api/notices/preview/', view_notices_preview, ['json']);

	// MODEL: /models/subscribers.js
	ROUTE('#admin/api/subscribers/', ['*Subscriber --> query']);
	ROUTE('#admin/api/subscribers/{id}/', ['*Subscriber --> read']);
	ROUTE('#admin/api/subscribers/', ['*Subscriber --> save', 'post']);
	ROUTE('#admin/api/subscribers/', ['*Subscriber --> remove', 'delete']);
	ROUTE('#admin/api/subscribers/stats/', ['*Subscriber --> stats']);
	ROUTE('#admin/api/subscribers/toggle/', ['*Subscriber --> toggle']);

	// MODEL: /models/newsletters.js
	ROUTE('#admin/api/newsletters/', ['*Newsletter --> query']);
	ROUTE('#admin/api/newsletters/{id}/', ['*Newsletter --> read']);
	ROUTE('#admin/api/newsletters/', ['*Newsletter --> save', 'post']);
	ROUTE('#admin/api/newsletters/', ['*Newsletter --> remove', 'delete']);
	ROUTE('#admin/api/newsletters/test/', ['*Newsletter --> test', 'post']);
	ROUTE('#admin/api/newsletters/toggle/', ['*Newsletter --> toggle']);
	ROUTE('#admin/api/newsletters/stats/', ['*Newsletter --> stats']);
	ROUTE('#admin/api/newsletters/{id}/stats/', ['*Newsletter --> stats']);
	ROUTE('#admin/api/newsletters/{id}/backups/', json_backups);
	ROUTE('#admin/api/newsletters/state/', json_newsletter_state);

	// MODEL: /models/navigations.js
	ROUTE('#admin/api/nav/{id}/', ['*Navigation --> read']);
	ROUTE('#admin/api/nav/', ['*Navigation --> save', 'post']);

	// MODEL: /models/settings.js
	ROUTE('#admin/api/settings/', ['*Settings --> read']);
	ROUTE('#admin/api/settings/', ['*Settings --> save', 'post']);

	// ESHOP
	// MODEL: /models/products.js
	ROUTE('#admin/api/products/', ['*Product --> query', 'cors', 30000]);
	ROUTE('#admin/api/products/{id}/', ['*Product --> read', 'cors', 30000]);
	ROUTE('#admin/api/products-save/', ['*Product --> save', 'post', 'cors', 30000]);
	ROUTE('#admin/api/products/', ['*Product --> remove', 'delete', 'cors']);
	ROUTE('#admin/api/products/toggle/', ['*Product --> toggle']);
	ROUTE('#admin/api/products/dependencies/', ['*Product --> dependencies']);
	ROUTE('#admin/api/products/stats/', ['*Product --> stats']);
	ROUTE('#admin/api/products/{id}/stats/', ['*Product --> stats']);
	ROUTE('#admin/api/products/{id}/backups/', json_backups);
	ROUTE('#admin/api/products/category/', json_products_replace, ['*Product']);
	ROUTE('#admin/api/products/manufacturer/', json_products_replace, ['*Product']);
	ROUTE('#admin/api/products/import/', json_products_import, ['post']);
	ROUTE('#admin/api/products/export/', json_products_export, ['*Product']);

	// MODEL: /models/productsgroup.js
	ROUTE('#admin/api/products-group/', ['*ProductsGroup --> save', 'post', 'cors']);
	ROUTE('#admin/api/products-group/', ['*ProductsGroup --> query', 'cors']);
	ROUTE('#admin/api/products-group/{id}/', ['*ProductsGroup --> read', 'cors']);
	ROUTE('#admin/api/products-group/', ['*ProductsGroup --> remove', 'delete', 'cors']);

	// MODEL: /models/orders.js
	ROUTE('#admin/api/orders/', ['*Order --> query', 'cors']);
	ROUTE('#admin/api/orders/{id}/', ['*Order --> read', 'cors']);
	ROUTE('#admin/api/orders/', ['*Order --> save', 'post', 'cors']);
	ROUTE('#admin/api/orders/', ['*Order --> remove', 'delete']);
	ROUTE('#admin/api/orders/stats/', ['*Order --> stats']);
	ROUTE('#admin/api/orders/toggle/', ['*Order --> toggle']);
	ROUTE('#admin/api/orders/dependencies/', ['*Order --> dependencies']);
	ROUTE('#admin/api/orders/export/', json_orders_export, ['*Order']);

	// MODEL: /models/users.js
	ROUTE('#admin/api/users/', ['*User --> query']);
	ROUTE('#admin/api/users/{id}/', ['*User --> read']);
	ROUTE('#admin/api/users/', ['*User --> save', 'post']);
	ROUTE('#admin/api/users/', ['*User --> remove', 'delete']);
	ROUTE('#admin/api/users/stats/', ['*User --> stats']);
	ROUTE('#admin/api/users/{id}/stats/', ['*User --> stats']);

	// Files
	ROUTE('#admin/api/files/', ['*File --> query']);
	ROUTE('#admin/api/files/clear/', ['*File --> clear']);

	//banners
	// ROUTE('#admin/api/banner/', 					['*Banners --> query', 'get']);
	// ROUTE('#admin/api/banner/',  					['*Banners --> save','post']);
	// Other
	ROUTE('#admin/api/contactforms/stats/', ['*Contact --> stats']);

	// Websocket
	WEBSOCKET('#admin/live/', socket, ['json']);

	// Login
	ROUTE('/api/login/admin/', login, ['post']);

	//banners
	ROUTE('#admin/api/banner/', ['*Slider --> query', 'get', 'cors']);
	ROUTE('#admin/api/banner/', ['*Slider --> save', 'post', 'cors']);
	ROUTE('#admin/api/banner/', ['*Slider --> remove', 'delete', 'cors']);
	ROUTE('#admin/api/banner/{id}/', ['*Slider --> read', 'cors']);


	// media
	ROUTE('#admin/api/media/', ['*Media --> save', 'post', 'cors']);
	ROUTE('#admin/api/media/', ['*Media --> query', 'cors']);
	ROUTE('#admin/api/media/', ['*Media --> remove', 'delete', 'cors']);
	ROUTE('#admin/api/media/{id}/', ['*Media --> read', 'cors']);

	//homepage data
	ROUTE('#admin/api/homepage-data', homepageData, ['POST', 'cors', 10000]);
	ROUTE('#admin/api/homepage-data', homepageGetData, ['cors', 10000]);

	ROUTE('#admin/api/payment-link', createpaymentLink, ['POST', 'cors', 10000]);
	ROUTE('#admin/api/payment-link/{id}', fetchPaymentLink, ['cors', 10000]);
	ROUTE('#admin/api/order-notify', order_notify, ['POST', 'cors', 10000]);
	ROUTE('#admin/api/otp-requests', getOtpRequests, ['cors', 10000]);
	ROUTE('#admin/api/cart-details', getCartDetails, ['cors', 10000]);
	ROUTE('#admin/api/product-stock-update', updateStock, ['post', 'cors', 10000]);
	ROUTE('#admin/api/product-stock-fetch', fetchStock, ['post', 'cors', 100000]);

	// update cart status
	ROUTE('#admin/api/update-cart-status', updateCartStatus, ['post', 'cors', 10000]);

	// get notify user details
	ROUTE('#admin/api/notify-user', getNotifyUser, ['cors', 10000]);

	// notify user
	ROUTE('#admin/api/notify-user', sendNotifyUser, ['post', 'cors', 10000]);


	// offer details for category
	ROUTE('#admin/api/offers', saveOffers, ['POST', 'cors', 10000]);
	ROUTE('#admin/api/offers', getOffers, ['cors', 10000]);
	ROUTE('#admin/api/offers/{id}', deleteOffers, ['delete', 'cors', 10000]);
	ROUTE('#admin/api/offers/{id}', getOffer, ['cors']);

	// coupon 
	ROUTE('#admin/api/coupon', addCoupon, ['post', 'cors']);
	ROUTE('#admin/api/coupon', updateCoupon, ['put', 'cors']);
	ROUTE('#admin/api/coupon/{id}', deleteCoupon, ['delete', 'cors']);
	ROUTE('#admin/api/coupon', getCoupons, ['cors']);
	ROUTE('#admin/api/coupon/{id}', getCoupon, ['cors']);

	// cod dynamic
	ROUTE('#admin/api/cod', addCod, ['post', 'cors']);
	ROUTE('#admin/api/cod', getCod, ['cors']);

	// all products stock sync
	ROUTE('#admin/api/products-stock-sync', productsStockSync, ['cors']);

	// api to write menu json data
	ROUTE('#admin/api/menu-json', saveMenuJson, ['POST', 'cors']);
	ROUTE('#admin/api/menu-json', getMenuJson, ['cors']);



};

// function to write menu json data
async function saveMenuJson() {
	var self = this;
	var data = self.body;
	var JOB_ID = generateUuidModule.createUUID();
	var nosql = new Agent();
	if (!data) {
		self.json({
			status: false,
			message: "no data"
		})
	} else {
		nosql.update('saveMenu', 'configuration').make(function (builder) {
			builder.where('configurationName', 'menu_json');
			builder.set('configurationDetails', data);
		});
		var MenuData = await nosql.promise('saveMenu');
		self.json({
			status: true,
			message: "Saved Successfully"
		})
		await menuPageCronJob();
		console.log("MENU_JSON_SAVE_TRIGGERED", new Date().toISOString(), JOB_ID, MenuData);
	}
}

// function to get the menu json
async function getMenuJson() {
	var self = this;
	var nosql = new Agent();
	nosql.select('getMenu', 'configuration').make(function (builder) {
		builder.where('configurationName', 'menu_json');
		builder.first();
	});
	var getMenu = await nosql.promise('getMenu');
	var json = getMenu.configurationDetails;
	//console.log("data",json)
	self.json(json);
}

// function to update all the products stock
async function productsStockSync() {
	var self = this;
	self.json({
		status: true,
		message: "Products Stock Syncing started"
	})

	stockSyncEmitter.emit('stock')

}


// cod dynamic
async function addCod() {
	var self = this;
	var data = self.body;
	var JOB_ID = generateUuidModule.createUUID();
	var nosql = new Agent();
	if (!data) {
		self.json({
			status: false,
			message: "no data"
		})
	} else {
		nosql.update('saveCod', 'configuration').make(function (builder) {
			builder.where('configurationName', 'COD');
			builder.set('configurationDetails', data);
		});
		var saveCod = await nosql.promise('saveCod');
		if (saveCod != null) {
			self.json({
				status: true,
				message: "Saved Successfully"
			})
		} else {
			self.json({
				status: false
			})
		}

		console.log("COD_SAVE_TRIGGERED", new Date().toISOString(), JOB_ID, saveCod);
	}
}

async function getCod() {
	var self = this;
	var nosql = new Agent();
	nosql.select('getCod', 'configuration').make(function (builder) {
		builder.where('configurationName', 'COD');
		builder.first();
	});
	var getCod = await nosql.promise('getCod');
	var json = getCod.configurationDetails;
	//console.log("data",json)
	if (json != null) {
		self.json({
			status: true,
			message: "Success",
			data: json
		})
	} else {
		self.json({
			status: false
		})
	}
}



// coupon 

async function addCoupon() {
	var self = this;
	var couponInfo = self.body;
	var nosql = new Agent();
	couponInfo.id = UID();
	couponInfo.datecreated = new Date();
	couponInfo.startDate = new Date(couponInfo.startDate);
	couponInfo.endDate = new Date(couponInfo.endDate);
	couponInfo.orderMiniAmount = parseInt(couponInfo.orderMiniAmount);
	couponInfo.offerMaxAmount = parseInt(couponInfo.offerMaxAmount);
	nosql.insert('addCoupon', 'coupon').make(function (builder) {
		builder.set(couponInfo);
	});

	var addCoupon = await nosql.promise('addCoupon');
	if (addCoupon != null) {
		self.json({
			status: true,
			message: "Coupon Saved Successfully"
		})
	} else {
		self.json({
			status: false
		})
	}
}

async function updateCoupon() {
	var self = this;
	var couponInfo = self.body;
	var nosql = new Agent();
	couponInfo.dateupdated = new Date();
	couponInfo.startDate = new Date(couponInfo.startDate);
	couponInfo.endDate = new Date(couponInfo.endDate);

	nosql.update('updateCoupon', 'coupon').make(function (builder) {
		builder.set(couponInfo);
		builder.where('id', couponInfo.id)
	});

	var updateCoupon = await nosql.promise('updateCoupon');
	if (updateCoupon > 0) {
		self.json({
			status: true,
			message: "Coupon Updated Successfully"
		})
	} else {
		self.json({
			status: false
		})
	}
}

async function deleteCoupon() {
	var self = this;
	var couponInfo = self.params;
	var nosql = new Agent();

	nosql.remove('deleteCoupon', 'coupon').make(function (builder) {
		builder.where('id', couponInfo.id);
	});

	var deleteCoupon = await nosql.promise('deleteCoupon');
	if (deleteCoupon > 0) {
		self.json({
			status: true,
			message: "Coupon deleted Successfully"
		})
	} else {
		self.json({
			status: false
		})
	}
}

async function getCoupons() {
	var self = this;
	var opt = self.query;
	var nosql = new Agent();

	nosql.listing('getCoupon', 'coupon').make(function (builder) {
		builder.page(opt.page || 1, opt.limit || 10);
	});

	var getCoupon = await nosql.promise('getCoupon');
	if (getCoupon != null) {
		self.json({
			status: true,
			message: "Success",
			data: getCoupon
		})
	} else {
		self.json({
			status: false
		})
	}
}

async function getCoupon() {
	var self = this;
	var opt = self.params;
	var nosql = new Agent();
	//console.log("id", opt);
	nosql.select('getCoupon', 'coupon').make(function (builder) {
		builder.where('id', opt.id);
		builder.first();
	});

	var getCoupon = await nosql.promise('getCoupon');
	if (getCoupon != null) {
		self.json({
			status: true,
			message: "Success",
			data: getCoupon
		})
	} else {
		self.json({
			status: false
		})
	}
}


// offer details for category

async function saveOffers() {
	var self = this;
	var model = self.body;
	model.datecreated = new Date();
	var isUpdate = !!model.id;
	var nosql = new Agent();
	if (isUpdate) {
		model.dateupdated = new Date();
	} else {
		model.id = UID();
		model.datecreated = new Date();
		model.dateupdated = new Date();
	}
	if (isUpdate) {
		nosql.update('updateOffer', 'product_offer').make(function (builder) {
			builder.set(model);
			builder.where('id', model.id)
		})

		var updateOffer = await nosql.promise('updateOffer');
		if (updateOffer > 0) {
			self.json({
				status: true,
				message: "Success"
			})
		} else {
			self.json({
				status: false,
				message: "Fail"
			})
		}
	} else {
		nosql.insert('saveOffer', 'product_offer').make(function (builder) {
			builder.set(model)
		})
		var saveOffer = await nosql.promise('saveOffer');
		if (saveOffer != null) {
			self.json({
				status: true,
				message: "Success"
			})
		} else {
			self.json({
				status: false,
				message: "Fail"
			})
		}
	}

}

async function getOffers() {
	var self = this;
	var opt = self.query;
	var nosql = new Agent();
	nosql.select('getOffer', 'product_offer').make(function (builder) {
		builder.page(opt.page || 1, opt.limit || 10);
		builder.sort('datecreated', 'desc');
	})
	var getOffer = await nosql.promise('getOffer');
	//console.log("getOffer", getOffer);
	if (getOffer != null) {
		self.json({
			status: true,
			data: getOffer
		})
	} else {
		self.json({
			status: false
		})
	}
}

async function deleteOffers() {
	var self = this;
	var offerInfo = self.params;
	var nosql = new Agent();

	nosql.remove('deleteOffer', 'product_offer').make(function (builder) {
		builder.where('id', offerInfo.id);
	});

	var deleteOffer = await nosql.promise('deleteOffer');
	console.log("deleteOffer", deleteOffer);
	if (deleteOffer > 0) {
		self.json({
			status: true,
			message: "Offer deleted Successfully"
		})
	} else {
		self.json({
			status: false
		})
	}
}

async function getOffer() {
	var self = this;
	var opt = self.params;
	var nosql = new Agent();
	//console.log("id", opt);
	nosql.select('getOffer', 'product_offer').make(function (builder) {
		builder.where('id', opt.id);
		builder.first();
	});

	var getOffer = await nosql.promise('getOffer');
	if (getOffer != null) {
		self.json({
			status: true,
			message: "Success",
			data: getOffer
		})
	} else {
		self.json({
			status: false
		})
	}
}

// notify user
function sendNotifyUser() {
	var self = this;
	var productName = self.body.productName;
	var phone = self.body.phone;
	var message = `Dear user, ${productName} is in stock now. Please log on to http://www.happlimobiles.com and place your order.`;

	smsModule.sendSMS(phone, message);
	self.json({
		status: true,
		message: "Notify user success"
	});
}

// function to fetch product details
async function FetchProduct(id) {
	var nosql = new Agent();
	//console.log("id", id)
	nosql.select('getProduct', 'product').make(function (builder) {
		builder.where('id', id);
		builder.first();
	});
	try {
		var product = await nosql.promise('getProduct');
		return { product: product, err: null };
	} catch (err) {
		return { product: null, err: err };
	}

}

async function getNotifyUser() {
	var self = this;
	var nosql = new Agent();

	nosql.listing('getNotifyUser', 'notify_user').make(function (builder) {
		builder.page(self.query.page || 1, self.query.limit || 10);
		builder.sort('timeStamp', 'desc');
	})

	var getNotifyUser = await nosql.promise('getNotifyUser');
	if (getNotifyUser != null) {
		var array = getNotifyUser.items;
		for (let i = 0; i < array.length; i++) {
			const element = array[i];
			var { product, err } = await FetchProduct(element.productId);
			if (err || product == null) {
				console.log("product fetch err", err, product);
			}
			element.productDetails = product;

		}
		self.json({
			status: true,
			data: getNotifyUser
		})
	} else {
		self.json({
			status: false
		})
	}
}

async function updateCartStatus() {
	var self = this;
	var nosql = new Agent();
	nosql.update('updateCart', 'cart').make(function (builder) {
		builder.where('id', self.body.id);
		builder.set('status', self.body.status);
	});
	var updateCart = await nosql.promise('updateCart');
	console.log("updateCart", updateCart);
	if (updateCart > 0) {
		self.json({
			status: true,
			message: "Cart status updated Successfully"
		})
	} else {
		self.json({
			status: false,
			message: "Cart status update Fail"
		})
	}
}

async function fetchStock() {
	var self = this;
	var nosql = new Agent();
	var body = self.body;

	nosql.select('getStock', 'product_stock').make(function (builder) {
		builder.where('productId', body.productId);
	})

	var getStock = await nosql.promise('getStock');
	// console.log("getStock",getStock.length);
	if (getStock.length > 0) {
		self.json({
			status: true,
			data: getStock
		})
	} else {
		self.json({
			status: false,
			message: "Stock Unavailable"
		})
	}
}

async function updateStock() {
	var self = this;
	var body = self.body;
	self.json({
		status: true
	})
	await stock.getAPXitemCode(body.productId);
}

async function getCartDetails() {
	var self = this;
	var nosql = new Agent();
	var opt = self.query;
	nosql.listing('getCart', 'cart').make(function (builder) {
		opt.phoneno && builder.where('id', opt.phoneno);
		builder.page(self.query.page || 1, self.query.limit || 10);
		builder.sort('dateupdated', 'desc');
	})

	var getCart = await nosql.promise('getCart');
	if (getCart != null) {
		self.json({
			status: true,
			data: getCart
		})
	} else {
		self.json({
			status: false
		})
	}
}

async function getOtpRequests() {
	var self = this;
	var nosql = new Agent();

	nosql.listing('getOtp', 'otp_request').make(function (builder) {
		builder.page(self.query.page || 1, self.query.limit || 10);
		builder.sort('timeStamp', 'desc');
	})

	var otpRequests = await nosql.promise('getOtp');
	if (otpRequests != null) {
		self.json({
			status: true,
			data: otpRequests
		})
	} else {
		self.json({
			status: false
		})
	}
}

function login() {

	var self = this;
	var key = (self.body.name + ':' + self.body.password).hash();

	if (F.global.config.users[key]) {
		OPERATION('admin.notify', { type: 'admin.login', message: self.body.name });
		self.cookie(F.config['admin-cookie'], key, '1 month');
		self.success();
	} else
		self.invalid().push('error-users-credentials');
}

function order_notify() {
	var self = this;
	var message = null;

	if (self.body.status == "Cancel") {
		message = `Your order with Happi Mobiles has been canceled for more info, https://happimobiles.com/checkout/${self.body.order_id}`;
	}

	if (self.body.status == "Sent") {
		message = `Your order with Happi Mobiles is out for delivery, https://happimobiles.com/checkout/${self.body.order_id}`;
	}

	if (self.body.status == "Finished") {
		message = `Your order with Happi Mobiles is delivered, https://happimobiles.com/checkout/${self.body.order_id}`;
	}

	if (self.body.status == "Hold") {
		message = `Your order with Happi Mobiles is on Hold, https://happimobiles.com/checkout/${self.body.order_id}`;
	}

	if (self.body.status == "Accepted") {
		message = `Your order with Happi Mobiles is  Accepted, https://happimobiles.com/checkout/${self.body.order_id}`;
	}

	if (message == null) {
		self.json({
			state: false,
			message: "Invalid Status"
		});
		return;
	}

	var options = {
		'method': 'POST',
		'url': `http://api.smscountry.com/SMSCwebservice_bulk.aspx?User=happi9&passwd=Happi@12345&mobilenumber=91${self.body.phone}&message=${encodeURI(message)}&sid=HappiM&mtype=N&DR=Y`,
		'headers': {
			'Content-Type': 'application/json'
		}
	};

	request(options, function (error, response) {
		if (error) throw new Error(error);
		console.log(response.body);
		self.json({
			state: true
		});
	});

}


async function createpaymentLink() {
	var self = this;
	var nosql = new Agent();
	var self = this;
	var obj = self.body;

	obj.data_created = new Date();
	obj.ispaid = false;

	nosql.insert('LinkCreate', 'order-link').make(function (builder) {
		builder.set(obj);
	});

	var message = `Your Payment Link is https://happimobiles.com/order-delivery/${obj.id}`;
	var options = {
		'method': 'POST',
		'url': `http://api.smscountry.com/SMSCwebservice_bulk.aspx?User=happi9&passwd=Happi@12345&mobilenumber=91${obj.delivery_phone}&message=${encodeURI(message)}&sid=HappiM&mtype=N&DR=Y`,
		'headers': {
			'Content-Type': 'application/json'
		}
	};

	request(options, function (error, response) {
		if (error) throw new Error(error);
		console.log(response.body);
	});

	try {
		var link = await nosql.promise('LinkCreate');
		// console.log("cart", cart);
		self.json(
			link
		);
	} catch (err) {
		self.status(400);
		self.json({ err: err })
	}

}

async function fetchPaymentLink(id) {
	var self = this;
	var nosql = new Agent();

	nosql.select('order', 'order-link').make(function (builder) {
		builder.where('id', id);
		builder.first();
	});

	try {
		var link = await nosql.promise('order');
		// console.log("cart", cart);
		self.json(
			link
		);
	} catch (err) {
		self.status(400);
		self.json(err);
	}
}

ON('controller', function (controller, name) {

	if (name !== 'admin' || controller.url === '/api/login/admin/')
		return;

	var ddos = DDOS[controller.ip];

	// 20 failed attempts
	if (ddos > 20) {
		controller.cancel();
		controller.throw401();

		return;
	}

	var cookie = controller.cookie(F.config['admin-cookie']);
	console.log("controller.headers", controller.headers);
	var token = controller.headers['x-auth'];
	var decoded = null;
	var user = null;


	// if ((cookie == null || !cookie.length) && token == null) {
	// 	console.log("COOKIE");
	// 	DDOS[controller.ip] = ddos ? ddos + 1 : 1;
	// 	controller.cancel();
	// 	controller.theme('admin');
	// 	controller.view('~login');
	// 	return;
	// }

	console.log("cookie", cookie, cookie.length);
	console.log("token", token);


	if (cookie != null && cookie.length != 0) {
		console.log("COOKIE");
		user = F.global.config.users[+cookie];
		if (user == null) {
			DDOS[controller.ip] = ddos ? ddos + 1 : 1;
			controller.cancel();
			controller.theme('admin');
			controller.view('~login');
			return;
		}

		// Roles
		if (!user.sa && user.roles.length && controller.url !== controller.sitemap_url('admin')) {

			var cancel = true;

			for (var i = 0, length = user.roles.length; i < length; i++) {
				var role = user.roles[i];
				if (controller.url.indexOf(role.toLowerCase()) !== -1) {
					cancel = false;
					break;
				}
			}

			// Allowed URL
			if (cancel) {
				for (var i = 0, length = ALLOW.length; i < length; i++) {
					if (controller.url.indexOf(ALLOW[i]) !== -1) {
						cancel = false;
						break;
					}
				}

				if (cancel) {
					controller.cancel();
					controller.throw401();
					return;
				}
			}
		}

		// console.log("ADMIN-USER", user);
		controller.user = user;
	} else if (token != null) {
		try {
			decoded = jwt.verify(token, JWT_SECRET_KEY);
			if (decoded != null) {
				user = decoded;
				controller.user = user;
			}
		} catch (err) {
			// err
			console.log("err", err);
			controller.cancel();
			controller.throw401();
		}
	} else {

		controller.cancel();
		controller.theme('admin');
		controller.view('~login');
		return;
	}


});

ON('service', function (counter) {
	if (counter % 15 === 0)
		DDOS = {};
});



function socket() {
	var self = this;
	WS = self;
	self.autodestroy(() => WS = null);
}

function makeid(length) {
	var result = '';
	var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	var charactersLength = characters.length;
	for (var i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

//console.log("ID", makeid(6));

// Upload (multiple) pictures
function upload() {

	var id = [];
	var self = this;

	//console.log(self.query);

	self.files.wait(function (file, next) {
		file.read(function (err, data) {


			if (self.query.type == 'product' || self.query.type == 'sliders' || self.query.type == 'banners') {

				if (self.query.type == 'product') {
					if (file.width == 600 && file.height == 800) {
						file.extension = U.getExtension(file.filename);
						if (file.extension != "jpg") {
							self.throw400('Invalid Image Format');
							return;
						}

						//var ref = NOSQL('files').binary.insert(file.filename, []);
						var ref = makeid(6) + '-' + Date.now();
						console.log("PRODUCT", {
							id: ref, name: file.filename, size: file.size, width: file.width, height: file.height, type: file.type,
							ctime: F.datetime, mtime: F.datetime, extension: file.extension, download: '/download/' + ref + '.' + file.extension,
							s3Key: 'happi/' + ref + "." + file.extension
						});

						id.push({
							id: ref, name: file.filename, size: file.size, width: file.width, height: file.height, type: file.type,
							ctime: F.datetime, mtime: F.datetime, extension: file.extension, download: '/download/' + ref + '.' + file.extension,
							s3Key: 'happi/' + ref + "." + file.extension
						});


						const params = {
							Bucket: BUCKET, // pass your bucket name
							Key: 'happi/' + ref + "." + file.extension, // file will be saved as testBucket/contacts.csv
							Body: data,
							ACL: "public-read"
						};



						s3.upload(params, function (s3Err, data1) {
							if (s3Err) {
								// fs.unlinkSync(element.destination + "/" + element.filename);
								// res.send({ "status": false, "message": "Image upload Fail" });
							} else {
								console.log("video file", data1.Key);
							}
						});

					} else {
						self.throw400('Invalid Image size please upload the 350px X 450px and .JPG');
						return;
					}
				}

				if (self.query.type == 'banners' || self.query.type == 'sliders') {
					if (file.width == 1900 && file.height == 500) {
						file.extension = U.getExtension(file.filename);
						if (file.extension != "jpg") {
							self.throw400('Invalid Image Format');
							return;
						}
						//var ref = NOSQL('files').binary.insert(file.filename, data);
						var ref = makeid(6) + '-' + Date.now();
						id.push({
							id: ref, name: file.filename, size: file.size, width: file.width, height: file.height, type: file.type,
							ctime: F.datetime, mtime: F.datetime, extension: file.extension, download: '/download/' + ref + '.' + file.extension,
							s3Key: 'happi/' + ref + "." + file.extension
						});


						const params = {
							Bucket: BUCKET, // pass your bucket name
							Key: 'happi/' + ref + "." + file.extension, // file will be saved as testBucket/contacts.csv
							Body: data,
							ACL: "public-read"
						};

						s3.upload(params, function (s3Err, data1) {
							if (s3Err) {
								// fs.unlinkSync(element.destination + "/" + element.filename);
								// res.send({ "status": false, "message": "Image upload Fail" });
							} else {
								console.log("video file", data1.Key);
							}
						});
					} else {
						self.throw400('Invalid Image size please upload the 350px X 450px and .JPG');
						return;
					}
				}

			} else {
				// Store current file into the HDD
				file.extension = U.getExtension(file.filename);
				///var ref = NOSQL('files').binary.insert(file.filename, data);
				var ref = makeid(6) + '-' + Date.now();
				id.push({
					id: ref, name: file.filename, size: file.size, width: file.width, height: file.height, type: file.type,
					ctime: F.datetime, mtime: F.datetime, extension: file.extension, download: '/download/' + ref + '.' + file.extension,
					s3Key: 'happi/' + ref + "." + file.extension
				});


				const params = {
					Bucket: BUCKET, // pass your bucket name
					Key: 'happi/' + ref + "." + file.extension, // file will be saved as testBucket/contacts.csv
					Body: data,
					ACL: "public-read"
				};


				s3.upload(params, function (s3Err, data1) {
					console.error(s3Err);
					console.log(data1);
					if (s3Err) {
						// fs.unlinkSync(element.destination + "/" + element.filename);
						// res.send({ "status": false, "message": "Image upload Fail" });

					} else {
						console.log("video file", data1.Key);
					}
				});
			}


			// Next file
			setTimeout(next, 100);
		});

	}, () => {

		setTimeout(function () {
			self.json(id);
		}, 6000);

	});


}

// Upload base64
function upload_base64() {
	var self = this;

	if (!self.body.file) {
		self.json(null);
		return;
	}

	var type = self.body.file.base64ContentType();
	var ext;

	switch (type) {
		case 'image/png':
			ext = '.png';
			break;
		case 'image/jpeg':
			ext = '.jpg';
			break;
		case 'image/gif':
			ext = '.gif';
			break;
		default:
			self.json(null);
			return;
	}

	var data = self.body.file.base64ToBuffer();
	var id = NOSQL('files').binary.insert((self.body.name || 'base64').replace(/\.[0-9a-z]+$/i, '').max(40) + ext, data);
	self.json('/download/' + id + ext);
}

// Creates a preview
function view_pages_preview() {
	var self = this;
	self.layout('layout-preview');
	self.repository.preview = true;
	self.repository.page = self.body;
	self.view('~cms/' + self.body.template);
}

function json_widget_settings(id) {
	var self = this;
	var item = F.global.widgets[id];
	self.json(item ? item.editor : null);
}

function json_backups(id) {
	var self = this;
	NOSQL(self.req.split[self.req.split.length - 3]).backups(n => n.data.id === id, self.callback());
}

function json_newsletter_state() {
	this.json(F.global.newsletter);
}

function json_products_replace() {
	var self = this;
	self.$workflow('replace-' + self.req.split[self.req.split.length - 1], self.callback());
}

function json_products_export() {
	var self = this;
	self.$workflow('export', (err, response) => self.binary(Buffer.from(response), 'applications/json', 'binary', 'products.json'));
}

function json_orders_export() {
	var self = this;
	self.$workflow('export', (err, response) => self.binary(Buffer.from(response), 'applications/json', 'binary', 'orders.json'));
}

function json_products_import() {
	$WORKFLOW('Product', 'import', this.body, this.callback(), this);
}

function json_dashboard_online() {
	var self = this;
	self.json(MODULE('visitors').today());
}

function json_pages_links() {
	var self = this;
	var arr = [];
	for (var i = 0, length = F.global.pages.length; i < length; i++) {
		var item = F.global.pages[i];
		arr.push({ url: item.url, name: item.name, parent: item.parent });
	}
	self.json(arr);
}

function json_dashboard() {
	MODULE('visitors').monthly(this.callback());
}

function json_dashboard_referrers() {
	NOSQL('visitors').counter.stats_sum(24, F.datetime.getFullYear(), this.callback());
}

function view_notices_preview() {
	var self = this;
	$WORKFLOW('Notice', 'preview', self.body.body || '', function (err, response) {
		self.content(response, 'text/html');
	});
}

// function to write data
async function homepageData() {
	console.log("homeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
	var self = this;
	var data = self.body;
	var nosql = new Agent();
	var JOB_ID = generateUuidModule.createUUID();
	if (!data) {
		self.json({
			status: false,
			message: "no data"
		})
	} else {
		nosql.update('saveHomepageRaw', 'configuration').make(function (builder) {
			builder.where('configurationName', 'Homepage_Json');
			builder.set('configurationDetails', data);
		});
		var homepageRawData = await nosql.promise('saveHomepageRaw');
		self.json({
			status: true,
			message: "Saved Successfully"
		})

		console.log("HOMEPAGE_SAVE_TRIGGERED", new Date().toISOString(), JOB_ID, homepageRawData);
	}
}

// function to get data
async function homepageGetData() {
	var self = this;
	var nosql = new Agent();
	nosql.select('getHomepageRaw', 'configuration').make(function (builder) {
		builder.where('configurationName', 'Homepage_Json');
		builder.first();
	});
	var homepageRawData = await nosql.promise('getHomepageRaw');
	var json = homepageRawData.configurationDetails;
	//console.log("data",json)
	self.json(json);
}





// // Cron for every 15 mins 
// cron.schedule('*/15 * * * *', () => {
// 	homeCronPageJob()
// });

setInterval(homeCronPageJob, 10000);

// home page job 
async function homeCronPageJob() {
	var JOB_ID = generateUuidModule.createUUID();
	//console.log("HOMEPAGE_CRON_JOB", new Date().toISOString(), JOB_ID, "TRIGGERED");
	var mnosql = new Agent();
	var self = this;
	//console.log("NO- CACHE");

	mnosql.select('getHomepageRaw', 'configuration').make(function (builder) {
		builder.where('configurationName', 'Homepage_Json');
		builder.first();
	});
	var homepageRawData = await mnosql.promise('getHomepageRaw');
	var res = homepageRawData.configurationDetails;
	//var res = JSON.parse(fs.readFileSync('./homepageRaw.json'));
	if (!res) {
		console.log("HOMEPAGE_CRON_JOB", new Date().toISOString(), JOB_ID, "DATA GET FAILED");
	}
	else {
		//console.log("HOMEPAGE_CRON_JOB", new Date().toISOString(), JOB_ID, "FILE SAVE SUCCESS");
		mnosql.select('bannersDesk', 'slider').make(function (builder) {
			builder.sort('weight', 'desc');
			builder.where('active', true);
			builder.where('devicetype', 'desktop');
		})

		mnosql.select('bannersMobile', 'slider').make(function (builder) {
			builder.sort('weight', 'desc');
			builder.where('active', true);
			builder.where('devicetype', 'mobile');
		})
		
		mnosql.exec(async function (err, response) {
			if (err) {
				console.log("HOMEPAGE_CRON_JOB", new Date().toISOString(), JOB_ID, "MONGO ERROR");
			} else {
				
				res.bannersMobile = response.bannersMobile;
				res.bannersDesk = response.bannersDesk;

				fs.writeFile(__dirname + '/../public/homepage.json', JSON.stringify(res), function (err) {
					if (err) {
		
						console.log("HOMEPAGE_CRON_JOB", new Date().toISOString(), JOB_ID, "FILE SAVE ERROR");
					}
					else {
		
						//console.log("HOMEPAGE_CRON_JOB", new Date().toISOString(), JOB_ID, "FILE SAVE SUCCESS");
					}
				});
			}
		});
	

	}

}

async function menuPageCronJob() {
	var nosql = new Agent();
	var JOB_ID = generateUuidModule.createUUID();
	nosql.select('getMenu', 'configuration').make(function (builder) {
		builder.where('configurationName', 'menu_json');
		builder.first();
	});
	var getMenu = await nosql.promise('getMenu');
	var res = getMenu.configurationDetails;
	//var res = JSON.parse(fs.readFileSync('./homepageRaw.json'));
	if (!res) {
		console.log("MENU_PAGE_CRON_JOB", new Date().toISOString(), JOB_ID, "DATA GET FAILED");
	} else {
		fs.writeFile(__dirname + '/../public/header.json', JSON.stringify(res), function (err) {
			if (err) {

				console.log("MENU_PAGE_CRON_JOB", new Date().toISOString(), JOB_ID, "FILE SAVE ERROR");
			}
			else {

				console.log("MENU_PAGE_CRON_JOB", new Date().toISOString(), JOB_ID, "FILE SAVE SUCCESS");
			}
		});
	}
}

//will run the menu page jon are 5 sec + start or restart time; 
setTimeout(function () {
	menuPageCronJob();
}, 5000);

// will run the home page jon are 5 sec + start or restart time; 
setTimeout(function () {
	homeCronPageJob();
}, 5000);


// 20181130T120049Z
// 20181130
//20201231T091131Z
// var dateToParse = new Date().toUTCString();
// var test = Date.parse(dateToParse.replace(/(....)(..)(..T..)(..)/, "$1-$2-$3:$4:")); 
// var t = new Date(test).toJSON();
// var dt = t.replace(/:/g,"");
// var mt = dt.replace(/-/g,"");
// var ndl = mt.slice(0, 15);
// var newdateLong = ndl + "Z";
// var n = newdateLong.split("T");
// var newdatesmall = n[0];
// console.log(newdateLong,"=====",newdatesmall);