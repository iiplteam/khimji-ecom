
// var pszie = 4;
// var goldwt = 1.409;
// var rose_gold_wt = 0.582;

var goldGlobalPrice = {
    "22k": 5100,
    "18k": 4000,
    "rose_gold": 4500,
    "yellow_gold": 4300
}

var goldObj = [{
    size: 4,
    gold_wt_22: 1.409,
    gold_wt_18: 0,
    rose_gold_wt: 0.582
}, {
    size: 5,
    gold_wt_22: 1.489,
    gold_wt_18: 0,
    rose_gold_wt: 0.57
}, {
    size: 6,
    gold_wt_22: 1.589,
    gold_wt_18: 0,
    rose_gold_wt: 0.672
}, {
    size: 7,
    gold_wt_22: 1.679,
    gold_wt_18: 0,
    rose_gold_wt: 0.782
}, {
    size: 8,
    gold_wt_22: 1.989,
    gold_wt_18: 0,
    rose_gold_wt: 0.882
}]

var goldRingMakingChargePercent = 15;
var goldRingGst = 3;

function priceCalculations(size) {
    // calculate the price for gold ,making charge ,tax and total
    // calculation for 22k
    var result = [];
    for (let i = 0; i < goldObj.length; i++) {
        const element = goldObj[i];
        if (size == element.size) {
            console.log("size",size);
            var glodPrice = element.gold_wt_22 * goldGlobalPrice['22k'];
            var roseGoldPrice = element.rose_gold_wt * goldGlobalPrice['rose_gold'];
            var makingCharge = (glodPrice + roseGoldPrice) * (goldRingMakingChargePercent / 100);
            var tax = (glodPrice + roseGoldPrice + makingCharge) * (goldRingGst / 100);
            var total = (glodPrice + roseGoldPrice + makingCharge + tax);
            var obj = {
                glodPrice: glodPrice.toFixed(3),
                roseGoldPrice:roseGoldPrice.toFixed(3),
                makingCharge:makingCharge.toFixed(3),
                tax:tax.toFixed(3),
                total:total.toFixed(3)
            }
            result.push(obj);
        }
      

    }

    console.table(result);

    //console.table(["glodPrice",glodPrice,"roseGoldPrice",roseGoldPrice,"makingCharge",makingCharge,"tax",tax,"total", total])
    console.log("glodPrice", glodPrice, "roseGoldPrice", roseGoldPrice);
    console.log("makingCharge", makingCharge, "tax", tax);
    console.log("total", total);
}

priceCalculations(5)



// "price_breakup": {
//     "gold": 4437.44,
//     "making": 2450,
//     "tax": 430.9
// },